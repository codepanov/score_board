function _(x){
	return document.getElementById(x);
}
function restrict(elem){
	var tf = _(elem);
	var rx = new RegExp;
	if(elem == "email"){
		rx = /[' "]/gi;
	} else if(elem == "fname" || elem == "lname"){
		rx = /[^a-z]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "pgman" || elem == "pgmod" || /*elem == "team" ||*/ elem == "insco"){
		rx = /[^a-z0-9 ]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "adr"){
		rx = /[^a-z0-9, ]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "pnum" || elem == "insnum"){
		rx = /[^a-z0-9-]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "celno" || elem == "wano"){
		rx = /[^0-9+]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "pgcol"){
		rx = /[^a-z, ]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "faiid"){
		rx = /[^a-z0-9-]/gi;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "civl"){
		rx = /[^0-9]/g;
		tf.value = tf.value.replace(rx, "");
	} else if(elem == "natid"){
		rx = /[^a-z0-9./-]/gi;
		tf.value = tf.value.replace(rx, "");
	}
}
function emptyElement(x){
	_(x).innerHTML = "";
}
function checkmail(){
    var em = _("email").value;
    let emel = _("email");
	if(em != ""){
        _("emailstatus").innerHTML = 'checking ...';
        emel.style.marginBottom = 0;
		var ajax = ajaxObj("POST", "signup_pilot.php"); // Mozda da modulisem php
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            _("emailstatus").innerHTML = ajax.responseText;
	        }
        }
        ajax.send("emailcheck="+em);	// Ne zaboravi da promenis $_POST u php fajlu u emailcheck!
	}
}
function checkmailtl(){
    var em = _("email").value;
    let emel = _("email");
	if(em != ""){
        _("emailstatus").innerHTML = 'checking ...';
        emel.style.marginBottom = 0;
		var ajax = ajaxObj("POST", "signup_tl.php"); // Mozda da modulisem php
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            _("emailstatus").innerHTML = ajax.responseText;
	        }
        }
        ajax.send("emailcheck="+em);	// Ne zaboravi da promenis $_POST u php fajlu u emailcheck!
	}
}
function checknum(){
	var nu = _("pnum").value;
	let nuel = _("pnum");
	let numstatus = _("numstatus");
	if(nu != ""){
		numstatus.innerHTML = 'checking ...';
		nuel.style.marginBottom = 0;
		var ajax = ajaxObj("POST", "signup_pilot.php"); // Mozda da modulisem php
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            _("numstatus").innerHTML = ajax.responseText;
	        }
        }
        ajax.send("passportcheck="+nu);	// Ne zaboravi da promenis $_POST u php fajlu u passportcheck!
	}
}
function getRadioVal(form, name) {
    var val;
    // get list of radio buttons with specified name
    var radios = form.elements[name];
    
    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}
function signup(){
	var i = _("file").files[0].name;
	var fn = _("fname").value; 
	var ln = _("lname").value;
	var e = _("email").value; 
	var tl = getRadioVal( _('signupform'), 'leader' );
	var c = _("country").value;
	var a = _("adr").value;
	var p = _("pnum").value;
	var cn = _("celno").value;
	var w = _("wano").value;
	var pma = _("pgman").value;
	var pmo = _("pgmod").value;
	var pco = _("pgcol").value;
	var f = _("faiid").value;
	var cvl = _("civl").value;
	var nf = _("natid").value;
	var ico = _("insco").value;
	var inu = _("insnum").value;
	var g = getRadioVal( _('signupform'), 'gender' );
	var bd = _("bdate").value;
	var ts = getRadioVal( _('signupform'), 'T-shirt' );
	var v = getRadioVal( _('signupform'), 'vegan' );
	var b = getRadioVal( _('signupform'), 'blood' );
	
	/*var flag = "";
	
	switch(c) {
        case "Albania":
            flag = "https://www.crwflags.com/fotw/images/a/al.gif";
        break;
        case "Serbia":
            flag = "https://www.crwflags.com/fotw/images/r/rs.gif";
        break;
    }*/
	
	var formdata = new FormData();
	formdata.append("file_img", i);
	
	var status = _("status");
	if(i == "" || fn == "" || ln == "" || e == "" || p == "" || cn == "" || /*w == "" ||*/ /*f == "" ||*/ cvl == "" || bd == ""){
		status.innerHTML = "Fill out required form data";
	} else {
		//status.innerHTML = 'please wait...';
		var ajax = ajaxObj("POST", "signup_pilot.php");
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            if(ajax.responseText != "signup_success"){
					status.innerHTML = ajax.responseText;
				} else {
					window.scrollTo(0,0);
					_("signupform").innerHTML = "Your request is received and is pending approval";
					//_("note").innerHTML = "Your request is received and is pending approval";
					_("reg_form").style.textAlign = "center";
					//_("title1").style.textAlign = "center";
					var bodyFlex = document.querySelector("body");
					bodyFlex.style.display = "flex";
                    bodyFlex.style.justifyContent = "center";
                    bodyFlex.style.flexDirection = "column";
                    bodyFlex.style.height = "100vh";
                    setTimeout(function(){
                        window.location.assign("https://pgaworlds.com");
                    }, 3000);
				}
	        }
        }
        ajax.send("i="+i+"&fn="+fn+"&ln="+ln+"&e="+e+"&tl="+tl+"&c="+c+"&a="+a+"&p="+p+"&cn="+cn+"&w="+w+"&pma="+pma+"&pmo="+pmo+"&pco="+pco+"&f="+f+"&cvl="+cvl+"&nf="+nf+"&ln="+ln+"&ico="+ico+"&inu="+inu+"&g="+g+"&bd="+bd+"&ts="+ts+"&v="+v+"&b="+b/*+"&flag="+flag*/);
	}
}
function signup_tl(){
	var i = _("file").files[0].name;
	var fn = _("fname").value; 
	var ln = _("lname").value;
	var e = _("email").value; 
	//var tl = getRadioVal( _('signupform'), 'leader' );
	var c = _("country").value;
	var a = _("adr").value;
	var p = _("pnum").value;
	var cn = _("celno").value;
	var w = _("wano").value;
	//var pma = _("pgman").value;
	//var pmo = _("pgmod").value;
	//var pco = _("pgcol").value;
	//var f = _("faiid").value;
	//var cvl = _("civl").value;
	//var nf = _("natid").value;
	//var ico = _("insco").value;
	//var inu = _("insnum").value;
	var g = getRadioVal( _('signupform'), 'gender' );
	var bd = _("bdate").value;
	var ts = getRadioVal( _('signupform'), 'T-shirt' );
	var v = getRadioVal( _('signupform'), 'vegan' );
	
	var formdata = new FormData();
	formdata.append("file_img", i);
	
	var status = _("status");
	if(i == "" || fn == "" || ln == "" || e == "" || p == "" || cn == "" || /*w == "" ||*/ /*f == "" || cvl == "" ||*/ bd == ""){
		status.innerHTML = "Fill out required form data";
	} else {
		//status.innerHTML = 'please wait...';
		var ajax = ajaxObj("POST", "signup_tl.php");
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            if(ajax.responseText != "signup_success") {
					status.innerHTML = ajax.responseText;
				} else {
					window.scrollTo(0,0);
					_("signupform").innerHTML = "Your request is received and is pending approval";
					//_("note").innerHTML = "Your request is received and is pending approval";
					_("reg_form").style.textAlign = "center";
					//_("title1").style.textAlign = "center";
					var bodyFlex = document.querySelector("body");
					bodyFlex.style.display = "flex";
                    bodyFlex.style.justifyContent = "center";
                    bodyFlex.style.flexDirection = "column";
                    bodyFlex.style.height = "100vh";
                    setTimeout(function(){
                        window.location.assign("https://pgaworlds.com");
                    }, 3000);
				}
	        }
        }
        ajax.send("i="+i+"&fn="+fn+"&ln="+ln+"&e="+e+"&c="+c+"&a="+a+"&p="+p+"&cn="+cn+"&w="+w+"&g="+g+"&bd="+bd+"&ts="+ts+"&v="+v);
	}
}
// Funkcija koja imenuje BROWSE button po odabranoj fotografiji, takodje uploaduje file u pozadini
function fileselected() {
	var fileName = document.querySelector("#file").files[0].name;
	var label = document.querySelector('label[for=file]');
	label.innerHTML = fileName;
	//---auto file uploader starts here---
	var file = document.querySelector("#file").files[0];
	var formdata = new FormData();
	formdata.append("file_img", file);
	var ajax = new XMLHttpRequest();
	ajax.open("POST", "/modules/imgupload.php");
	ajax.send(formdata);
	//---END auto uploader---

	//label.style = 'background: #1ABC9C';
}
/* function openTerms(){
	_("terms").style.display = "block";
	emptyElement("status");
} */
/* function addEvents(){
	_("elemID").addEventListener("click", func, false);
}
window.onload = addEvents; */
