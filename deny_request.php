<?php
	if (isset($_GET['id']) && isset($_GET['e'])) {
		// Connect to database and sanitize incoming $_GET variables
		include_once("php_includes/db_con.php");
		$id = preg_replace('#[^0-9]#i', '', $_GET['id']); 
		$e = mysqli_real_escape_string($db_con, $_GET['e']);
		// Evaluate the lengths of the incoming $_GET variable
		if($id == "" || strlen($e) < 5){
			// Log this issue into a text file and email details to yourself
			header("location: message.php?msg=activation_string_length_issues");
			exit();
		}
		
		// Check their credentials against the applied_pilots table in database
		$sql = "SELECT * FROM applied_pilots WHERE id='$id' AND email='$e' AND activated='0' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
		$numrows = mysqli_num_rows($query);
		// Evaluate for a match in the system (0 = no match, 1 = match)
		if($numrows == 0){
			// Check their credentials against the applied_tls table in database
			$sql = "SELECT * FROM applied_tls WHERE id='$id' AND email='$e' AND activated='0' LIMIT 1";
			$query = mysqli_query($db_con, $sql);
			$numrows = mysqli_num_rows($query);
			// Log this potential hack attempt to text file and email details to yourself
			if($numrows == 0) {
				header("location: message.php?msg=Your credentials are not matching anything in our system");
				exit();
			}
			// Match was found in TEAM LEADERS
			// Send email to deleted person
			$to = $e;							 
			$from = "accounts@pgawordls.com";
			$subject = "Request for PGAWC 2019, denied!";
			$message = "Your request for PGAWC 2019 was denied.";
			$headers = "From: $from\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\n";
			mail($to, $subject, $message, $headers);
			$sql = "DELETE FROM applied_tls WHERE id='$id' LIMIT 1";
			$query = mysqli_query($db_con, $sql);
			
			// Double check to see if deletition happened
			$sql = "SELECT * FROM applied_tls WHERE id='$id' LIMIT 1";
			$query = mysqli_query($db_con, $sql);
			$numrows = mysqli_num_rows($query);
			
			// Evaluate the double check
			if($numrows == 0){
				header("location: message.php?msg=deletition_success");
				exit();
			} else if($numrows == 1) {
				header("location: message.php?msg=deletition_failure");
				exit();
			}
		}
		// Match was found in PILOTS
		// Send email to deleted person
		$to = $e;							 
		$from = "accounts@pgawordls.com";
		$subject = "Request for PGAWC 2019, denied!";
		$message = "Your request for PGAWC 2019 was denied.";
		$headers = "From: $from\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\n";
		mail($to, $subject, $message, $headers);
		$sql = "DELETE FROM applied_pilots WHERE id='$id' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
			
		// Double check to see if deletition happened
		$sql = "SELECT * FROM applied_pilots WHERE id='$id' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
		$numrows = mysqli_num_rows($query);
			
		// Evaluate the double check
		if($numrows == 0){
			header("location: message.php?msg=deletition_success");
			exit();
		} else if($numrows == 1) {
			header("location: message.php?msg=deletition_failure");
			exit();
		}
	} else {
		// Log this issue of missing initial $_GET variables
		header("location: message.php?msg=missing_GET_variables");
		exit(); 
		
	}