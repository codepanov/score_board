<?php
$servername = "localhost";
$username = "pgaworld_root";
$password = "Te0d0raD0ra";
$dbname = "pgaworld_acc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, pilot_image, f_name, l_name FROM applied_pilots";
$result = $conn->query($sql);
//$db_img_names = array();  // ovo ostavljam ukoliko zelim da izbegnem redundantnost. morao bih da petljom prolazim kroz zajednicki niz
                            // ($db_img_names) za obe baze i da onda radim rename() na 36-oj liniji koda

if ($result->num_rows > 0) {

    // create array of file names from dir 'uploads'
    $dir = "uploads/";
    $files = opendir($dir);
    $uploads_file_names = array();
    while (($file = readdir($files)) !== false) {
        if(filetype($dir . $file) != "dir") {
            array_push($uploads_file_names, $file); // napakujem sve nazive fotki iz foldera uploads u niz $uploads_file_names
        }
    }
    // output data of each row from db
    while($row = $result->fetch_assoc()) {
        $img = $row["pilot_image"]; // uzmem naziv fotke iz baze
        $id = $row['id'];
        $name_for_image = strtolower($row['f_name']) . "_" . strtolower($row['l_name']) . ".jpg"; // formatiram buduci naziv za fotke u folderu uploads

        foreach($uploads_file_names as $old_name) { // prodjem kroz niz $uploads_file_names (imena fotki iz foldera uploads)
            if($old_name == $img) { // uporedim sa nazivima fotki u bazi i samo ako je isto...
                //rename("uploads/$old_name", "uploads/$name_for_image"); // ...promenim ime u folderu u formatirani naziv
                echo $name_for_image . "<br>"; // skloni ovo da bi radilo i skloni komentar sa rename()
            }
        }

        //$update = "UPDATE applied_pilots SET pilot_image='$name_for_image' WHERE id='$id'"; // odmah promenim ime fotke po sablonu $name_for_image i u bazi, tako da se zovu isto i jednostavno {name_surname.jpg}
        //$conn->query($update);
    }
    closedir($files);
} else {
    echo "0 results";
}

//----------------------------------------------------------------------------//
// ovde cu ponoviti kod jer me mrzi da ga kucam drugacije nanovo. na kraju bi bilo jednostavnije, lepse i razumljivije,
// ali me mrzi da idem u local i testiram
$sql = "SELECT id, tl_image, f_name, l_name FROM applied_tls";
$result = $conn->query($sql);

if ($result->num_rows > 0) {

    // create array of file names from dir 'uploads'
    $dir = "uploads/";
    $files = opendir($dir);
    $uploads_file_names = array(); // ovom linijom brisem prethodni niz i pravim novi prazan
    while (($file = readdir($files)) !== false) {
        if(filetype($dir . $file) != "dir") {
            array_push($uploads_file_names, $file); // napakujem sve nazive fotki iz foldera uploads u niz $uploads_file_names
        }
    }
    // output data of each row from db
    while($row = $result->fetch_assoc()) {
        $img = $row["tl_image"]; // uzmem naziv fotke iz baze
        $id = $row['id'];
        $name_for_image = strtolower($row['f_name']) . "_" . strtolower($row['l_name']) . ".jpg"; // formatiram buduci naziv za fotke u folderu uploads

        foreach($uploads_file_names as $old_name) { // prodjem kroz niz $uploads_file_names (imena fotki iz foldera uploads)
            if($old_name == $img) { // uporedim sa nazivima fotki u bazi i samo ako je isto...
                //rename("uploads/$old_name", "uploads/$name_for_image"); // ...promenim ime u folderu u formatirani naziv
                echo $name_for_image . "<br>";  // skloni ovo da bi radilo i skloni komentar sa rename()
            }
        }

        //$update = "UPDATE applied_tls SET tl_image='$name_for_image' WHERE id='$id'"; // odmah promenim ime fotke po sablonu $name_for_image i u bazi, tako da se zovu isto i jednostavno {name_surname.jpg}
        //$conn->query($update);
    }
    closedir($files);
} else {
    echo "0 results";
}
//----------------------------------------------------------------------------//

$conn->close();
echo "Images are renamed now! :)";