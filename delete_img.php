<?php
$servername = "localhost";
$username = "pgaworld_root";
$password = "Te0d0raD0ra";
$dbname = "pgaworld_acc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT pilot_image, f_name, l_name FROM applied_pilots";
$result = $conn->query($sql);
$db_file_names = array();

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $img = $row["pilot_image"];
        array_push($db_file_names, $img); // napakujem sve nazive fotki iz baze u niz $db_file_names
    }
} else {
    echo "0 results";
}

$sql = "SELECT tl_image, f_name, l_name FROM applied_tls";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $img = $row["tl_image"];
        array_push($db_file_names, $img); // napakujem sve nazive fotki iz baze u niz $db_file_names
    }
} else {
    echo "0 results";
}

$conn->close();
print_r($db_file_names);    // skloni ovo da bi radilo
exit();                     // skloni ovo da bi radilo

// read file names from folder
$dir = "uploads/";
$files = opendir($dir);
$uploads_file_names = array();
while (($file = readdir($files)) !== false) {
    if(filetype($dir . $file) != "dir") {
        array_push($uploads_file_names, $file); // napakujem sve nazive fotki iz foldera uploads u niz $uploads_file_names
    }
}
closedir($files);

// compare arrays and print ones to delete
$files_to_delete = array_diff($uploads_file_names, $db_file_names); // uporedim veci niz sa manjim
foreach($files_to_delete as $file) {
    //unlink("uploads/$file"); // obrisem iz veceg niza fajlove koji nisu u manjm takodje
}
echo "All extra images are now deleted! :)";