<?php
$message = "";
$msg = preg_replace('#[^a-z 0-9.:_()]#i', '', $_GET['msg']);
if($msg == "activation_failure"){
	$message = '<h2>Activation Error</h2> Sorry there seems to have been an issue activating account at this time. Contact your webmaster to identify the issue.';
} else if($msg == "activation_success"){
	$message = '<h2>Activation Success</h2> Account is now activated.';
} else if($msg == "deletition_failure"){
	$message = '<h2>Delete Error</h2> Sorry there seems to have been an issue deleting account at this time. Contact your webmaster to identify the issue.';
} else if($msg == "deletition_success"){
	$message = '<h2>Delete Success</h2> Account is now deleted.';
} else {
	$message = $msg;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
    <title>Pga Worlds message</title>
	<style>
		body {
			font-family: 'Nunito', sans-serif;
			font-weight: bold;
			font-size: 1.2em;
			text-align: center;
		}
		.message {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
		}
	</style>
</head>
<body>
<div class="message"><?php echo $message; ?></div>