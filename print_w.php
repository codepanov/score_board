<style>
    .image {
        width: 30%;
        margin: 0 auto;
    }
</style>
<div class="image"><img src="uploads/pgaw_logo.jpg" alt="pgaw_logo" width="100%"></div>
<link href="https://bootswatch.com/4/flatly/bootstrap.min.css" rel="stylesheet">
<h3>Women individual results</h3>
<br>	
<?php
	$db_con = mysqli_connect("localhost", "pgaworld_root", "Te0d0raD0ra", "pgaworld_acc");
    if(mysqli_connect_errno()) {
        echo mysqli_connect_error();
        exit();
    }
	$sql = "SELECT activated, pilot_image, f_name, l_name, gender, order_number, country, Team, total, round_1, round_2, round_3, round_4, round_5, round_6, round_7, round_8, round_9, round_10, round_11, round_12 FROM applied_pilots ORDER BY -total DESC";
	//$sql = "SELECT activated, pilot_image, f_name, l_name, gender, country, order_number, total, round_1, round_2, round_3, round_4, round_5, round_6, round_7, round_8, round_9, round_10, round_11, round_12 FROM applied_pilots ORDER BY order_number";
	$result = $db_con->query($sql);

	if ($result->num_rows > 0) {
		echo "
		<table class='table table-hover'>
			<tr class='table-active'>
				<th>Position</th>
				<th>Pilot</th>
				<th>Image</th>
				<th>Country</th>
				<th>Team</th>
				<th>Start number</th>
				<th>round 1</th>
				<th>round 2</th>
				<th>round 3</th>
				<th>round 4</th>
				<th>round 5</th>
				<th>round 6</th>
				<th>round 7</th>
				<th>round 8</th>
				<th>round 9</th>
				<th>round 10</th>
				<th>round 11</th>
				<th>round 12</th>
				<th>Score</th>
			</tr>
		";
		//$pos = 0;
		$score_array = array();
		
		// output data of each row
		while($row = $result->fetch_assoc()) {
			if($row['activated'] != 0 && $row['gender'] == 'female' && !empty($row['order_number'])) {
				//$pos++;
				/*
				foreach ($countries as $code => $countryname) {
					if($countryname == $row['country']) {
						$flag = strtolower($code);
					}
				}
				*/
			$cntcode = $row['country'];
			switch ($cntcode) {
				case 'United States':
					$flag = 'us';
					break;
				case 'Colombia':
					$flag = 'co';
					break;
				case 'Turkey':
					$flag = 'tr';
					break;
				case 'Canada':
					$flag = 'ca';
					break;
				case 'Serbia':
					$flag = 'rs';
					break;
				case 'Russian Federation':
					$flag = 'ru';
					break;
				case 'Iran':
					$flag = 'ir';
					break;
				case 'Mongolia':
					$flag = 'mn';
					break;
				case 'China':
					$flag = 'cn';
					break;
				case 'Slovenia':
					$flag = 'si';
					break;
				case 'Thailand':
					$flag = 'th';
					break;
				case 'Japan':
					$flag = 'jp';
					break;
				case 'France':
					$flag = 'fr';
					break;
				case 'Korea':
					$flag = 'kr';
					break;
				case 'Ecuador':
					$flag = 'ec';
					break;
				case 'Romania':
					$flag = 'ro';
					break;
				case 'Argentina':
					$flag = 'ar';
					break;
				case 'Poland':
					$flag = 'pl';
					break;
				case 'Nepal':
					$flag = 'np';
					break;
				case 'Hungary':
					$flag = 'hu';
					break;
				case 'Czech Republic':
					$flag = 'cz';
					break;
				case 'Kazakhstan':
					$flag = 'kz';
					break;
				case 'Netherlands':
					$flag = 'nl';
					break;
				case 'Latvia':
					$flag = 'lv';
					break;
				case 'Macedonia':
					$flag = 'mk';
					break;
				case 'Spain':
					$flag = 'es';
					break;
				case 'United Kingdom':
					$flag = 'gb';
					break;
				case 'Lithuania':
					$flag = 'lt';
					break;
				case 'Indonesia':
					$flag = 'id';
					break;
				case 'Albania':
					$flag = 'al';
					break;
				case 'Bulgaria':
					$flag = 'bg';
					break;
				case 'Germany':
					$flag = 'de';
					break;
				case 'Kosovo':
					$flag = 'xk';
					break;
				case 'Saudi Arabia':
					$flag = 'sa';
					break;
				default:
					$flag = '';
			}
			
			if($row['country'] == "Kosovo") {
				$country = "Kosovo*";
			} else {
				$country = $row['country'];
			}
            
			
			$score = $row['total'];
			array_push($score_array, $score);
			if(count($score_array) == 1) {
				$pos = count($score_array);
			} elseif(count($score_array) > 1) {
				if($score_array[count($score_array)-1] == $score_array[count($score_array)-2]) {
					$pos = array_search($score_array[count($score_array)-1], $score_array)+1;
				} else {
					$pos = count($score_array);
				}
			}
			
			$rounds = array($row['round_1'], $row['round_2'], $row['round_3'], $row['round_4'], $row['round_5'], $row['round_6'], $row['round_7'], $row['round_8'], $row['round_9'], $row['round_10'], $row['round_11'], $row['round_12']);
			$strike = array_keys($rounds, max($rounds))[0];
			
			switch ($strike) {
                case "0":
                    $row['round_1'] = "<strike>" . $row['round_1'] . "</strike>";
                    break;
                case "1":
                    $row['round_2'] = "<strike>" . $row['round_2'] . "</strike>";
                    break;
                case "2":
                    $row['round_3'] = "<strike>" . $row['round_3'] . "</strike>";
                    break;
                case "3":
                    $row['round_4'] = "<strike>" . $row['round_4'] . "</strike>";
                    break;
                case "4":
                    $row['round_5'] = "<strike>" . $row['round_5'] . "</strike>";
                    break;
                case "5":
                    $row['round_6'] = "<strike>" . $row['round_6'] . "</strike>";
                    break;
                case "6":
                    $row['round_7'] = "<strike>" . $row['round_7'] . "</strike>";
                    break;
                case "7":
                    $row['round_8'] = "<strike>" . $row['round_8'] . "</strike>";
                    break;
                case "8":
                    $row['round_9'] = "<strike>" . $row['round_9'] . "</strike>";
                    break;
                case "9":
                    $row['round_10'] = "<strike>" . $row['round_10'] . "</strike>";
                    break;
                case "10":
                    $row['round_11'] = "<strike>" . $row['round_11'] . "</strike>";
                    break;
                case "11":
                    $row['round_12'] = "<strike>" . $row['round_12'] . "</strike>";
                    break;
            }
			
			echo "
			<tr>
				<td>".$pos."</td>
				<td>" . ucwords(strtolower($row['f_name'])) . " " . ucwords(strtolower($row['l_name'])) . "</td>
				<td><a href='/uploads/" . $row['pilot_image'] . "'><img src='/uploads/" . $row['pilot_image'] . "' width=40></a></td>
				<td>" . $country . "</td>
				<td>" . $row['Team'] . "</td>
				<td>" . $row['order_number'] . "</td>
                <!--<td style='text-align: center;'><img class='flag' src='https://lipis.github.io/flag-icon-css/flags/1x1/{$flag}.svg' alt='Flag' style='width: 30px; border-radius: 15px;'></td>-->
				<td>" . $row['round_1'] . "</td>
				<td>" . $row['round_2'] . "</td>
				<td>" . $row['round_3'] . "</td>
				<td>" . $row['round_4'] . "</td>
				<td>" . $row['round_5'] . "</td>
				<td>" . $row['round_6'] . "</td>
				<td>" . $row['round_7'] . "</td>
				<td>" . $row['round_8'] . "</td>
				<td>" . $row['round_9'] . "</td>
				<td>" . $row['round_10'] . "</td>
				<td>" . $row['round_11'] . "</td>
				<td>" . $row['round_12'] . "</td>
				<td>" . $score . "</td>
			</tr>
			";
			}
		}
		echo "
		</table>
		";
	} else {
		echo "0 results";
	}
	$db_con->close();
?>