<?php
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    // Ajax calls this to check if mail is already registered
    if(isset($_POST["emailcheck"])){
        include_once("php_includes/db_con.php");
        $email = test_input($_POST["emailcheck"]);
        $sql = "SELECT id FROM applied_pilots WHERE email='$email' LIMIT 1";
        $query = mysqli_query($db_con, $sql); 
        $present = mysqli_num_rows($query);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format"; 
            echo '<strong style="color:red;">' . $emailErr . '</strong>';
            exit();
        }
        if ($present < 1) {
            //echo '<strong style="color:green;">' . $email . ' is not yet registered. <span style="color:#fea000;">Please, double check your email before registering!</span></strong>'; //Ovo sam po zelji iskljucio. Sada ne ispisuje da li je mail pronadjen u bazi.
            exit();
        } else {
            echo '<strong style="color:red;">' . $email . ' is already registered</strong>';
            exit();
        }
    }

    // Ajax calls this to check if passport number is already registered
    if(isset($_POST["passportcheck"])){
        include_once("php_includes/db_con.php");
        $pnum = test_input($_POST["passportcheck"]);
        $sql = "SELECT id FROM applied_pilots WHERE passport='$pnum' LIMIT 1";
        $query = mysqli_query($db_con, $sql); 
        $present = mysqli_num_rows($query);
        if ($present < 1) {
            echo '<strong style="color:green;">' . $pnum . ' is OK. <span style="color:#fea000;">Please, double check the number before registering!</span></strong>';
            exit();
        } else {
            echo '<strong style="color:red;">' . $pnum . ' is already registered</strong>';
            exit();
        }
    }

    // Ajax calls this for REGISTRATION to execute
    if(isset($_POST["e"])){
        // CONNECT TO THE DATABASE
        include_once("php_includes/db_con.php");
        // GATHER THE POSTED DATA INTO LOCAL VARIABLES
        //$i = $_FILES["file_img"]["name"];
        //if(isset($_COOKIE['imgnm'])) {
            //$i = $_COOKIE['imgnm'];
        //} else {
            $i = "thumb_" . $_POST['i'];
        //}
        $fn = preg_replace('#[^a-z0-9]#i', '', $_POST['fn']);
        $ln = preg_replace('#[^a-z0-9]#i', '', $_POST['ln']);
        $e = test_input($_POST["e"]);
        $e = filter_var($e, FILTER_VALIDATE_EMAIL);
        $tl = $_POST['tl'];
        $c = preg_replace('#[^a-z]#i', '', $_POST['c']);
        $a = preg_replace('#[^a-z0-9, ]#i', '', $_POST['a']);
        $p = preg_replace('#[^a-z0-9-]#i', '', $_POST['p']);
        $cn = preg_replace('#[^0-9+]#i', '', $_POST['cn']);
        $w = preg_replace('#[^0-9+]#i', '', $_POST['w']);
        $pma = preg_replace('#[^a-z0-9]#i', '', $_POST['pma']);
        $pmo = preg_replace('#[^a-z0-9]#i', '', $_POST['pmo']);
        $pco = preg_replace('#[^a-z, ]#i', '', $_POST['pco']);
        $f = preg_replace('#[^a-z0-9-]#i', '', $_POST['f']);
        $cvl = preg_replace('#[^0-9]#i', '', $_POST['cvl']);
        $nf = preg_replace('#[^a-z0-9./-]#i', '', $_POST['nf']);
        $ico = preg_replace('#[^a-z0-9]#i', '', $_POST['ico']);
        $inu = preg_replace('#[^a-z0-9-]#i', '', $_POST['inu']);
        $g = $_POST['g'];
        $bd = $_POST['bd'];
        $ts = $_POST['ts'];
        $v = $_POST['v'];
        $b = $_POST['b'];

        // DUPLICATE DATA CHECKS FOR EMAIL AND PASSPORT
        $sql = "SELECT id FROM applied_pilots WHERE email='$e' LIMIT 1";
        $query = mysqli_query($db_con, $sql); 
        $e_check = mysqli_num_rows($query);
        // -------------------------------------------
        // $sql = "SELECT id FROM applied_pilots WHERE passport='$p' LIMIT 1";
        // $query = mysqli_query($db_con, $sql); 
        // $p_check = mysqli_num_rows($query);

        // FORM DATA ERROR HANDLING
        if($i == "" || $fn == "" || $ln == "" || $e == "" || /*$a == ""||*/ $p == "" || $cn == "" || /*$w == "" ||*/ /*$f == "" ||*/ $cvl == ""){
            echo "The form submission is missing values.";
            exit();
        } else if ($e_check > 0){ 
            echo "That email address is already in use in the system";
            exit();
        }
        // else if ($p_check > 0){ 
        //     echo "That passport number is already in use in the system";
        //     exit();
        // }
        else if (is_numeric($fn[0]) && is_numeric($ln[0])) {
            echo 'User name and last name cannot begin with a number';
            exit();
        } else {
            // Add pilot into database
            $sql = "INSERT INTO applied_pilots (id, signedup, activated, pilot_image, f_name, l_name, email, team_leader, country, address, passport, cell, wapp, pg_man, pg_mod, pg_col, faiid, civlid, natflid, insuco, insuno, gender, date_birth, t_shirt, vegan, blood)
                    VALUES(null, now(), '0', '$i', '$fn', '$ln', '$e', '$tl', '$c', '$a', '$p', '$cn', '$w', '$pma', '$pmo', '$pco', '$f', '$cvl', '$nf', '$ico', '$inu', '$g', '$bd', '$ts', '$v', '$b')";
            $query = mysqli_query($db_con, $sql); 
            $uid = mysqli_insert_id($db_con); //pilots id from db, for use in activation
            //echo "signup_success";
            //exit();
            if($query == true) {
                // Email to admin activation link
                $to = "ovukazeljko@gmail.com";
                $to2 = "abpanov@gmail.com, paragliding.vrsac@gmail.com";
                $from = "accounts@pgawordls.com";
                $subject = "New Account Approval, ID= " . $uid;

                $message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>PGAW Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#fff; font-size:24px; color:#000; box-shadow: 0 3px 10px grey;"><a href="https://pgaworlds.com"><img src="https://pgaworlds.com/img/logo.jpg" width=50 alt="pgaworlds" style="display: block; border: none; margin-right: 10px;"></a>PGA Worlds Account Activation</div><div style="padding: 24px; font-size: 15px; font-weight: bold;">Hi Željko,<br><br>'.$fn.' '.$ln.' from '.$c.' with CIVL ID '.$cvl.' is pending for aproval to register as competitor for PG ACC 2019,<br><br>'/*.$i*/.'<br><br>Click the link below to activate his account:<br><br><a href="https://pgaworlds.com/activate.php?id='.$uid.'&e='.$e.'" style="text-decoration: none;">Click here to approve request</a><br><br>To deny registration, click the link below:<br><br><a href="https://pgaworlds.com/deny_request.php?id='.$uid.'&e='.$e.'" style="text-decoration: none;">Click here to deny request</a>';
                $message2 = 'New request,<br>'.$fn.' '.$ln.' from '.$c.' is pending for aproval to register as a competitor for PG ACC 2019.';

                /* http://www.pgaworlds.com/activation.php?id='.$uid.'&u='.$u.'&e='.$e.'&p='.$p_hash.' */

                $headers = "From: $from\n";
                $headers .= "MIME-Version: 1.0\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                mail($to, $subject, $message, $headers);
                mail($to2, $subject, $message2, $headers);
                echo "signup_success";
                exit();
            }
        }
        exit();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" href="img/favicon.png" type="image/x-icon">
    <script src="js/ajax.js"></script>
    <script src="js/main.js"></script>
    <title>Pga Worlds</title>
</head>
<body>
    <div class="wrapp">
        <!--<header>
            <nav></nav>
            <div id="reg">
            <h1>PG ACCURACY WORLDS 2019</h1>
                <div class="spacer"></div>
                <a href="#registration"><button class="b_reg">Scroll Down</button></a>
            </div>
            <p class="note" id="note">Note: Scroll down for registration form.</p>
        </header>
        <a name="registration"><div class="spacer"></div></a>
        <div id="title1">Registration Form FAI PG ACC 2019 for Pilots</div>-->
        <section id="reg_form">
            <h1 style="padding: 20px 0;">Pilot Registartion form</h1>
            <span id="status"></span>
            <form enctype="multipart/form-data" name="signupform" id="signupform" onsubmit="return false;">
            <div class="row">
                <label>First name <span style="color: red;">*</span></label><br>
                <input class="half_input" id="fname" type="text" onfocus="emptyElement('status')" onkeyup="restrict('fname')" maxlength="16" placeholder="Name" required="">
            </div>
            <div class="row">
                <label style="margin-left: 28px;">Last name <span style="color: red;">*</span></label><br>
                <input class="half_input" id="lname" type="text" onfocus="emptyElement('status')" onkeyup="restrict('lname')" maxlength="16" placeholder="Surname" required="" style="margin-left: 28px;">
            </div>
            <div class="row_full">
                <label>E-mail <span style="color: red;">*</span></label><br>
                <input class="full_input" id="email" type="text" onfocus="emptyElement('status')" onblur="checkmail()" onkeyup="restrict('email')" maxlength="88" placeholder="e-mail" required=""><!--onblur="checkmail()" izbaceno-->
                <span id="emailstatus"></span>
            </div>
            <div class="row_full">
                <label>Are you also a team leader? <span style="color: red;">*</span></label><br>
                <input type="radio" name="leader" value="ytl"> Yes &emsp;
                <input type="radio" name="leader" value="ntl" required=""> No
            </div>
            <div class="row_full">
                <label>Country <span style="color: red;">*</span></label><br>
                <!-- <input class="full_input" type="text" data-type="regular" style="" placeholder="Nationality"> -->
                <select id="country" style="margin: 20px;"required="">
                    <?php
                        include("php_includes/country_options.php");
                    ?>
                </select>
            </div>
            <div class="row_full">
                <label>Adress</label><br>
                <input class="full_input" id="adr" type="text" onkeyup="restrict('adr')" placeholder="Street name and number, postal code, city">
            </div>
            <div class="row_full">
                <label>Passport number: <span style="color: red;">*</span></label><br>
                <input class="full_input" id="pnum" type="text" onfocus="emptyElement('status')" onkeyup="restrict('pnum')" maxlength="25" required=""><!--onblur="checknum()" izbaceno-->
                <span id="numstatus"></span>
            </div>
            <div class="row">
                <label>Cellphone number <span style="color: red;">*</span></label><br>
                <input class="half_input" id="celno" type="text" onfocus="emptyElement('status')" onkeyup="restrict('celno')" maxlength="15" required="" placeholder="+xxx...">
            </div>
            <div class="row">
                <label style="margin-left: 28px;">WhatsApp number</label><br>
                <input class="half_input" id="wano" type="text" onfocus="emptyElement('status')" onkeyup="restrict('wano')" maxlength="15" style="margin-left: 28px;" placeholder="+xxx...">
            </div>
            <div class="row_full">
                <label>Paraglider manufacturer</label><br>
                <input class="full_input" id="pgman" type="text" onkeyup="restrict('pgman')" placeholder="Ozone, Advance, U-turn... etc">
            </div>
            <div class="row">
                <label>Paraglider model</label><br>
                <input class="half_input" id="pgmod" type="text" onkeyup="restrict('pgmod')" placeholder="PG model" name="10">
            </div>
            <div class="row">
                <label style="margin-left: 28px;">Paraglider color</label><br>
                <input class="half_input" id="pgcol" onkeyup="restrict('pgcol')" type="text" placeholder="Color 1, color 2... etc" style="margin-left: 28px;">
            </div>
            <div class="row_full">
                <label>FAI ID <!--<span style="color: red;">*</span>--></label><br>
                <input class="full_input" id="faiid" onkeyup="restrict('faiid')" type="text">
            </div>
            <div class="row_full">
                <label>CIVL ID <span style="color: red;">*</span></label>&emsp;<a href="http://civlrankings.fai.org/?a=306&" target="_blank" style="font-size: 12px;">Check your CIVL ID</a><br>
                <input class="full_input" id="civl" onfocus="emptyElement('status')" onkeyup="restrict('civl')" type="text" required="">
            </div>
            <div class="row_full">
                <label>National federation licence Id</label><br>
                <input class="full_input" id="natid" onkeyup="restrict('natid')" type="text">
            </div>
            <!-- <div class="row_full">
                <label>Team name</label><br>
                <input class="full_input" id="team" onkeyup="restrict('team')" type="text">
            </div> -->
            <div class="row">
                <label>Insurance company</label><br>
                <input class="half_input" id="insco" onkeyup="restrict('insco')" type="text">
            </div>
            <div class="row">
                <label style="margin-left: 28px;">Insurance number</label><br>
                <input class="half_input" id="insnum" onkeyup="restrict('insnum')" type="text" style="margin-left: 28px;">
            </div>
            <div class="row_full">
                <label>Gender <span style="color: red;">*</span></label><br>
                <input type="radio" name="gender" value="male"> Male &emsp;
                <input type="radio" name="gender" value="female" required=""> Female
            </div>
            <div class="row_full">
            <label>Date of birth <span style="color: red;">*</span></label><br>
                <input type="date" id="bdate" name="bdate" value="yyyy-mm-dd" min="1940-01-01" max="2005-12-31" required="">
            </div>
            <div class="row_full">
                <label>T-shirt <span style="color: red;">*</span></label><br>
                <input type="radio" name="T-shirt" value="xxs"> XXS &emsp;
                <input type="radio" name="T-shirt" value="xs"> XS &emsp;
                <input type="radio" name="T-shirt" value="s"> S &emsp;
                <input type="radio" name="T-shirt" value="m"> M &emsp;
                <input type="radio" name="T-shirt" value="l"> L &emsp;
                <input type="radio" name="T-shirt" value="xl"> XL &emsp;
                <input type="radio" name="T-shirt" value="xxl" required=""> XXL
            </div>
            <div class="row_full">
                <label>Vegan? <span style="color: red;">*</span></label><br>
                <input type="radio" name="vegan" value="yv"> Yes &emsp;
                <input type="radio" name="vegan" value="nv" required=""> No
            </div>
            <div class="row_full">
                <label>Blood type <span style="color: red;">*</span></label><br>
                <input type="radio" name="blood" value="0m"> O- &ensp;
                <input type="radio" name="blood" value="0p"> O+ &ensp;
                <input type="radio" name="blood" value="am"> A- &ensp;
                <input type="radio" name="blood" value="ap"> A+ &ensp;
                <input type="radio" name="blood" value="bm"> B- &ensp;
                <input type="radio" name="blood" value="bp"> B+ &ensp;
                <input type="radio" name="blood" value="abm"> AB- &ensp;
                <input type="radio" name="blood" value="abp"> AB+ &ensp;
                <input type="radio" name="blood" value="dn" required=""> I don't know
            </div>
            <div class="row_full" style="margin-bottom: 25px;">
                <span style="display: block;">Upload image of your face only (JPG or PNG allowed, 2MB max) Not smaller than 400 by 400 pixels<br>Your face should be in the center of the image</span><br>
                <!-- Ovde se povezuje modul za upload slike pilota -->
                <input name="uploaded_file" type=file id="file" class="inputfile" required onchange="fileselected()">
                <label for="file">Browse image: <span style="color: red;">*</span></label><br>
            </div>
            <div class="spacer"></div>
            <button class="b_reg" id="signupbtn" onclick="signup()" style="margin: 0 auto 25px; position: absolute;
            left: 50%; transform: translateX(-50%);">Sign Up</button>
            <div class="spacer"></div>
            </form>
        </section>
        <!--<div class="spacer"></div>
        <footer>
            <div class="spacer"></div>
            <p class="copyright"></p>
        </footer>-->
    </div>
</body>
</html>