<?php
	$db_con = mysqli_connect("localhost", "root", "", "accpg");
    if(mysqli_connect_errno()) {
        echo mysqli_connect_error();
        exit();
    }
	$sql = "SELECT activated, pilot_image, f_name, l_name, country, pg_mod, civlid FROM applied_pilots";
	$result = $db_con->query($sql);

	if ($result->num_rows > 0) {
		echo "
		<table>
			<tr>
			<th>Image</th>
			<th>Pilot</th>
			<th>Country</th>
			<th>Wing</th>
			<th>CIVL ID</th>
		</tr>
		";
		
		// output data of each row
		while($row = $result->fetch_assoc()) {
			if($row['activated'] != 0) {
			echo "
			<tr>
				<td><img src='https://tst.pgaworlds.com/uploads/" . $row['pilot_image'] . "' width=40></td>
				<td>" . $row['f_name'] . " " . $row['l_name'] . "</td>
				<td>" . $row['country'] . "</td>
				<td>" . $row['pg_mod'] . "</td>
				<td>" . $row['civlid'] . "</td>
			</tr>
			";
			}
		}
		echo "
		</table>
		";
	} else {
		echo "0 results";
	}
	$db_con->close();
	/*
	echo "
	img: <img src='https://tst.pgaworlds.com/uploads/". 
	$row['pilot_image'] ."' width=40>" . " - Name: " . 
	$row['f_name']. " " . 
	$row['l_name']. " | Country: " . 
	$row['country']. " | Wing: " . 
	$row['pg_mod']. " | CIVL ID: ". 
	$row['civlid'] . "<br>";
	*/
	?>