<!-- <form enctype="multipart/form-data" method="post" action="">
Choose your file here:
<input name="file_img" type="file"><br><br>
<input type="submit" value="Upload">
</form> -->


<!-- <form enctype="multipart/form-data" method="post">
<input name="file_img" type=file id="file" class="inputfile" onchange="fileselected()">
<label for="file">Browse image: <span style="color: red;">*</span></label><br>
<input type="submit" value="Upload">
</form> -->


<?php
if(isset($_FILES['file_img'])) { 
    $base = $_FILES['file_img']['name'];
    $temp = $_FILES['file_img']['tmp_name'];
    $fileSize = $_FILES['file_img']['size'];
    fileUpload();
}
function rsg($len){
	$result = "";
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789$$$$$$$1111111";
    $charArray = str_split($chars);
    for($i = 0; $i < $len; $i++){
	    $randItem = array_rand($charArray);
	    $result .= "".$charArray[$randItem];
    }
    return $result;
}
function fileUpload() {
    $dir = "../uploads/";
    $target = $dir . basename($GLOBALS['base']);
    $uploadOk = 1;
    $fileExt = strtolower(pathinfo($target, PATHINFO_EXTENSION));

    // Check if file already exists
    if (file_exists("../uploads/resized_" . $GLOBALS['base']) && $target != "../uploads/") {
        echo "Sorry, image with that name already exists.";
        $uploadOk = 0;
    }

    // Check file size
    if ($GLOBALS['fileSize'] > 2000000 || $GLOBALS['fileSize'] == 0) { // Ovde je 0 zato sto php.ini automatski odseca fajl veci od 2MB
        echo "Sorry, your image is larger than 2 Megabytes.";
        $uploadOk = 0;
    }

    // Allowed file formats
    
    if($fileExt != "jpg" && $fileExt != "png" && $fileExt != "jpeg" && $fileExt != "" && $fileExt != "JPG" && $fileExt != "JPEG" && $fileExt != "PNG") {
        echo "Sorry, only JPG & PNG file types are allowed.";
        $uploadOk = 0;
    }
    /*
    if($fileExt != "jpg" || $fileExt != "png" || $fileExt != "jpeg" || $fileExt != "" || $fileExt != "JPG" || $fileExt != "JPEG" || $fileExt != "PNG") {
        echo "Sorry, only JPG & PNG file types are allowed.";
        $uploadOk = 0;
    }
    */
    
    // Error handling
    if($uploadOk == 0) {
        echo "Your file was not uploaded.";
        header("Refresh: 2.25; imgupload.php");
        exit();
    // if everything is ok, try to upload file
    } else {
        if(move_uploaded_file($GLOBALS['temp'], $target)) {

            // ---------- Include Image Resizing Function --------
            include_once("resizer.php");
            $target_file = "../uploads/" . $GLOBALS['base'];
            //$resized_file = "../uploads/resized_" . $GLOBALS['base'];
            $random_name = rsg(5) . $GLOBALS['base']; // dodato
            $resized_file = "../uploads/resized_" . $random_name; // dodato
            $wmax = 400;
            $hmax = 400;
            ak_img_resize($target_file, $resized_file, $wmax, $hmax, $fileExt);
            unlink("../uploads/" . $GLOBALS['base']);
            // ----------- End Image Resizing Function -----------
            // ------ Start Image Thumbnail(Crop) Function ------
            //$target_file = "../uploads/resized_" . $GLOBALS['base']; //"uploads/resized_$fileName";
            //$thumbnail = "../uploads/thumb_" . $GLOBALS['base'];
            $target_file = "../uploads/resized_" . $random_name; // dodato
            $thumbnail = "../uploads/thumb_" . $random_name; // dodato
            //$GLOBALS['imgnm'] = $thumbnail;
            //$cookie_name = "imgnm";
            //$cookie_value = $thumbnail;
            //setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
            list($w_orig, $h_orig) = getimagesize($target_file);
            if($w_orig < $h_orig){
                $wthumb = $w_orig;
                $hthumb = $w_orig;
            } else {
                $wthumb = $h_orig;
                $hthumb = $h_orig;
            }
            ak_img_thumb($target_file, $thumbnail, $wthumb, $hthumb, $fileExt);
            // ------- End Image Thumbnail(Crop) Function -------
            echo basename($GLOBALS['base']) . " has been uploaded. ";
            header("Refresh: 2.25; imgupload.php");
            exit();
        } else { 
            echo "Sorry, there was a problem uploading your image. Try again!";
            header("Refresh: 2.25; imgupload.php");
            exit();
        }
    }
}
