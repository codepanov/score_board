<?php
include_once("php_includes/check_login_status.php");
// If user is already logged in, header him away
if($user_ok == true) {
	header("location: cms/index.php");
    exit();
}
// AJAX CALLS THIS LOGIN CODE TO EXECUTE
if(isset($_POST["u"])){
	// CONNECT TO THE DATABASE
	include_once("php_includes/db_con.php");
	// GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
	$u = mysqli_real_escape_string($db_con, $_POST['u']);
    $p = md5($_POST['p']);
    // GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
	// FORM DATA ERROR HANDLING
	if($u == "" || $p == ""){
		echo "login_failed";
        exit();
	} else { // END FORM DATA ERROR HANDLING
		$sql = "SELECT id, username, password FROM admins WHERE username='$u' AND activated='1' LIMIT 1";
        $query = mysqli_query($db_con, $sql);
        $row = mysqli_fetch_row($query);
		$db_id = $row[0];
		$db_username = $row[1];
        $db_pass_str = $row[2];
		if($p != $db_pass_str){
			echo "login_failed";
            exit();
		} else {
			// CREATE THEIR SESSIONS AND COOKIES
			$_SESSION['userid'] = $db_id;
			$_SESSION['username'] = $db_username;
            $_SESSION['password'] = $db_pass_str;
			setcookie("id", $db_id, strtotime( '+30 days' ), "/", "", "", TRUE);
			setcookie("user", $db_username, strtotime( '+30 days' ), "/", "", "", TRUE);
    		setcookie("pass", $db_pass_str, strtotime( '+30 days' ), "/", "", "", TRUE); 
			// UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
			$sql = "UPDATE admins SET ip='$ip', lastlogin=now() WHERE username='$db_username' LIMIT 1";
            $query = mysqli_query($db_con, $sql);
			echo $db_username;
		    exit();
		}
	}
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="js/ajax.js"></script>
    <script src="js/redirect.js"></script>
    <script src="js/main.js"></script>
    <title>Document</title>
</head>
<body>
    <form id="loginform" onsubmit="return false;">
        <div class="container fit">
                <h1>Log in</h1>
                <p>Please log in to enter admin CMS.</p>
                <hr>
            <label><b>Username</b></label>
            <input placeholder="Enter Username" type="text" id="uname" onfocus="emptyElement('status')" maxlength="40" required>
        
            <label><b>Password</b></label>
            <input placeholder="Enter Password" type="password" id="password" onfocus="emptyElement('status')" maxlength="100" required>

            <label for="empty-element" class="hidden"><b>Empty element</b></label>
            <input class="hidden" type="text" placeholder="Just an empty element" name="empty-element" disabled>
            <hr>
        
            <button class="registerbtn" id="loginbtn" onclick="login()">Log in</button>
            <button type="button" class="registerbtn" onclick="home();">Cancel</button>
            <span id="status"></span>
        </div>
    </form>
</body>
</html>