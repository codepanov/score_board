<?php
include_once("php_includes/check_login_status.php");
// Initialize any variables that the page might echo
$u = "";
$joindate = "";
$lastsession = "";
// Make sure the _GET username is set, and sanitize it
if(isset($_GET["u"])){
	$u = preg_replace('#[^a-z0-9]#i', '', $_GET['u']);
} else {
    header("location: index.html");
    exit();	
}
// Select the member from the users table
$sql = "SELECT * FROM admins WHERE username='$u' AND activated='1' LIMIT 1";
$user_query = mysqli_query($db_con, $sql);
// Now make sure that user exists in the table
$numrows = mysqli_num_rows($user_query);
if($numrows < 1){
	echo "That user does not exist or is not yet activated, press back";
    exit();	
}
// Check to see if the viewer is the account owner
$isOwner = "no";
echo "Alocation page<br>";
if($u == $log_username && $user_ok == true) {
    $isOwner = "yes";
    echo "Ovo vidis ako si ulogovan";
    echo '
        <form action="logout.php">
            <button type="submit" value="submit">Logout</button>
        </form>
    ';
}
// Fetch the user row from the query above
while ($row = mysqli_fetch_array($user_query, MYSQLI_ASSOC)) {
	$profile_id = $row["id"];
	$signup = $row["signup"];
	$lastlogin = $row["lastlogin"];
	$joindate = strftime("%b %d, %Y", strtotime($signup));
	$lastsession = strftime("%b %d, %Y | %H : %M", strtotime($lastlogin));
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $u; ?></title>
<link rel="stylesheet" href="style/style.css">
<script src="js/main.js"></script>
<script src="js/ajax.js"></script>
</head>
<body>
<div id="pageMiddle">
  <h3><?php echo "Welcome " . ucfirst($u); ?></h3>
  <p>Is <?php echo ' ' . $u . ' ';?> logged in and verified? <b><?php echo $isOwner; ?></b></p>
  <p>Join Date: <?php echo $joindate; ?></p>
  <p>Logged in: <?php echo $lastsession; ?></p>
</div>
</body>
</html>