function _(x){
    return document.getElementById(x);
}
function emptyElement(x){
    _(x).innerHTML = "";
}
function restrict(elem){
    var tf = _(elem);
    var rx = new RegExp;
    if(elem == "username"){
        rx = /[^a-z0-9]/gi;
    }
    tf.value = tf.value.replace(rx, "");
}
function checkusername(){
    var u = _("username").value;
    if(u != ""){
        _("unamestatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "register.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) == true) {
                _("unamestatus").innerHTML = ajax.responseText;
            }
        }
        ajax.send("usernamecheck="+u);
    }
}
function signup(){
    var u = _("username").value;
    var p1 = _("pass1").value;
    var p2 = _("pass2").value;
    var status = _("status");
    if(u == "" || p1 == "" || p2 == ""){
        status.innerHTML = "Fill out all of the form data";
    } else if(p1 != p2){
        status.innerHTML = "Your password fields do not match";
    } else {
        _("signupbtn").style.display = "none";
        status.innerHTML = 'please wait ...';
        var ajax = ajaxObj("POST", "register.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) == true) {
                if(ajax.responseText != "signup_success"){
                    status.innerHTML = ajax.responseText;
                    _("signupbtn").style.display = "block";
                } else {
                    window.scrollTo(0,0);
                    _("signupform").innerHTML = "OK "+u+", please wait for account activation";
                    _("signupform").style.textAlign = "center";
                    var bodyFlex = document.querySelector("body");
                    bodyFlex.style.display = "flex";
                    bodyFlex.style.justifyContent = "center";
                    bodyFlex.style.flexDirection = "column";
                    bodyFlex.style.height = "100vh";
                    setTimeout(function(){
                    window.location.assign("index.html");
                    //window.location.assign("https://admin.pgaworlds.com/");   
                    }, 5000);
                }
            }
        }
        ajax.send("u="+u+"&p="+p1);
    }
}
function login(){
	var u = _("uname").value;
	var p = _("password").value;
	if(u == "" || p == ""){
		_("status").innerHTML = "Fill out all of the form data";
	} else {
		//_("loginbtn").style.display = "none";
		_("status").innerHTML = 'please wait ...';
		var ajax = ajaxObj("POST", "login.php");
        ajax.onreadystatechange = function() {
	        if(ajaxReturn(ajax) == true) {
	            if(ajax.responseText == "login_failed"){
					_("status").innerHTML = "Login unsuccessful, please try again.";
					//_("loginbtn").style.display = "block";
				} else {
					window.location = "cms/index.php";
				}
	        }
        }
        ajax.send("u="+u+"&p="+p);
	}
}