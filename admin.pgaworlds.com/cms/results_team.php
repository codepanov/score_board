<?php

// calculate result for every team per each series
     
// get db data and cast to signed for php to use valid integers
// umesto total, uzimace se rezultati iz svake runde

if(isset($_POST['round'])) {
	
	switch ($_POST['round']) {
		case "round_1":
			$round = "round_1";
			break;
		case "round_2":
			$round = "round_2";
			break;
		case "round_3":
			$round = "round_3";
			break;
		case "round_4":
			$round = "round_4";
			break;
		case "round_5":
			$round = "round_5";
			break;
		case "round_6":
			$round = "round_6";
			break;
		case "round_7":
			$round = "round_7";
			break;
		case "round_8":
			$round = "round_8";
			break;
		case "round_9":
			$round = "round_9";
			break;
		case "round_10":
			$round = "round_10";
			break;
		case "round_11":
			$round = "round_11";
			break;
		case "round_12":
			$round = "round_12";
			break;
	}
}

//$round = 'round_1';

// declared array
    $total_team = array();
    
    // db connection
    include_once("../../public_html/php_includes/db_con.php");

$sql = "SELECT DISTINCT country, GROUP_CONCAT(CAST($round AS SIGNED)) FROM applied_pilots WHERE Team = 'Team pilot' GROUP BY country";

$result = $db_con->query($sql);

if($result->num_rows > 0) {
	while($row = $result->fetch_assoc()) {
		
		// echo $row['country'] . "<br>";
		// echo $row['GROUP_CONCAT(CAST(total AS SIGNED))']  . "<br>";
		
		$country = $row['country'];
		
		// make an array of results in every pass
		$total_team = explode("," ,$row['GROUP_CONCAT(CAST('.$round.' AS SIGNED))']); // array does not have null values
		
		sort($total_team); // sort from lowest value
		
		if(count($total_team) == 1) { // for 1 team member, add 3 x 500, for 2 add 2 x 500, for 3 add 500
			$res = array_sum($total_team) + 1500;
			// update db total field
			$sql = "INSERT INTO team_results_per_series (country, round, result) VALUES ('$country', '$round', '$res')";
			
			if (mysqli_query($db_con, $sql)) {
				//echo "Database updated successfully!";
			} else {
				echo "Error updating record: " . mysqli_error($db_con);
			}
		} elseif(count($total_team) == 2) {
			$res = array_sum($total_team) + 1000;
			// update db total field
			$sql = "INSERT INTO team_results_per_series (country, round, result) VALUES ('$country', '$round', '$res')";

			if (mysqli_query($db_con, $sql)) {
				//echo "Database updated successfully!";
			} else {
				echo "Error updating record: " . mysqli_error($db_con);
			}
		} elseif(count($total_team) == 3) {
			$res = array_sum($total_team) + 500;
			// update db total field
			$sql = "INSERT INTO team_results_per_series (country, round, result) VALUES ('$country', '$round', '$res')";
			
			if (mysqli_query($db_con, $sql)) {
				//echo "Database updated successfully!";
			} else {
				echo "Error updating record: " . mysqli_error($db_con);
			}
		} else {
			$res = array_map('array_sum', array_chunk($total_team, 4)) [0];
			// update db total field
			$sql = "INSERT INTO team_results_per_series (country, round, result) VALUES ('$country', '$round', '$res')";
			
			if (mysqli_query($db_con, $sql)) {
				//echo "Database updated successfully!";
			} else {
				echo "Error updating record: " . mysqli_error($db_con);
			}
		}
	}
	echo "Database updated successfully!";
}