function _(x){
	return document.getElementById(x);
}
function list() {
    let country = _("cnt").value;
    
    // AJAX call for table.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "table.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
		    _("table").innerHTML = xhttp.responseText;
            _("nav_all").style.display="none";
            _("message").style.display="none";
        }
    }
    xhttp.send("country="+country);
}
function listTL() {
    let country = _("cnt").value;
    
    // AJAX call for table.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "tableTL.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
		    _("table").innerHTML = xhttp.responseText;
            _("nav_all").style.display="none";
            _("message").style.display="none";
        }
    }
    xhttp.send("country="+country);
}
function update() {
    var tbl = _("table");
    var tRows = tbl.rows.length; // ovde dobijem table rows collection, pa sa 'lenght' koliko zapravo imam <th> i <tr> tojest horizontalnih redova
    for (i=1; i < tRows; i++) { // Preskacem <th> text, time sto loop pocinjem od 1
    var output = []; // inicijalizujem niz gde cu sve da pakujem
        for(j=0; j < 10; j++) { // loop ovde pocinje od 0, da bih ugrabio id takmicara iz baze i ovaj loop pici kroz polja u jednom redu. Brojac se zavrsava na 9 da ne uzima u obzir nista iza kolone "Team". Ako ide dalje, ima problem nextSibling sa iscitavanjem nekih polja i prijavljuje gresku.
            var select = tbl.rows[i].cells[j].firstChild.nextSibling; //Stavljam ovo u varijablu jer je predugacko, a to je ono sto pise u select polju
            if(!isNaN(tbl.rows[i].cells[j].innerHTML)) { //proverava da li je u pitanju broj i preskace ime pilota
                var pilot_id = parseInt(tbl.rows[i].cells[j].innerHTML); // parseInt, iz stringa u int
                output.push(pilot_id);
            }
            if(tbl.rows[i].cells[j].innerHTML.search("select") != -1) { //metoda .search() na izlazu daje -1 ako ne nadje string
                var aloc_data = select.options[select.selectedIndex].text;
                output.push(aloc_data);
            }
        }
        //console.log(output.toString());
        
        // AJAX call for crud.php to UPDATE database
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "crud.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.onreadystatechange = function() {
	        if(xhttp.readyState == 4 && xhttp.status == 200) {
	            _("message").style.display = "block";
			    _("message").innerHTML = xhttp.responseText;
			    setTimeout(function(){_("message").style.display = "none"}, 3000);
			    //console.log('Proslo! :)');
	        }
        }
        var enc = JSON.stringify(output); //Da bih poslao niz kroz POST metodu, moram da ga prethodno pretvorim u string
        xhttp.send("enc="+enc);
        
    }
}
function updateTL() {
    var tbl = _("tableTL");
    var tRows = tbl.rows.length; // ovde dobijem table rows collection, pa sa 'lenght' koliko zapravo imam <th> i <tr> tojest horizontalnih redova
    for (i=1; i < tRows; i++) { // Preskacem <th> text, time sto loop pocinjem od 1
    var output = []; // inicijalizujem niz gde cu sve da pakujem
        for(j=0; j < 6; j++) { // loop ovde pocinje od 0, da bih ugrabio id takmicara iz baze i ovaj loop pici kroz polja u jednom redu.
            var select = tbl.rows[i].cells[j].firstChild.nextSibling; //Stavljam ovo u varijablu jer je predugacko, a to je ono sto pise u select polju
            if(!isNaN(tbl.rows[i].cells[j].innerHTML)) { //proverava da li je u pitanju broj i preskace ime pilota
                var tl_id = parseInt(tbl.rows[i].cells[j].innerHTML); // parseInt, iz stringa u int
                output.push(tl_id);
            }
            if(tbl.rows[i].cells[j].innerHTML.search("select") != -1) { //metoda .search() na izlazu daje -1 ako ne nadje string
                var aloc_data = select.options[select.selectedIndex].text;
                output.push(aloc_data);
            }
        }
        console.log(output.toString());
        
        // AJAX call for crud.php to UPDATE database
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "crud.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.onreadystatechange = function() {
	        if(xhttp.readyState == 4 && xhttp.status == 200) {
	            _("messageTL").style.display = "block";
			    _("messageTL").innerHTML = xhttp.responseText;
			    setTimeout(function(){_("messageTL").style.display = "none"}, 3000);
			    //console.log('Proslo! :)');
	        }
        }
        var tl_update = JSON.stringify(output); //Da bih poslao niz kroz POST metodu, moram da ga prethodno pretvorim u string
        xhttp.send("tl_update="+tl_update);
    }
}
function showResult(res) {
    if(res.length == 0) {
        window.location = 'index.php';
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            _("table").innerHTML = xhttp.responseText;
            _("nav_all").style.display="none";
            _("message").style.display="none";
        }
    }
    xhttp.open("GET","search.php?q="+res,true);
    xhttp.send();
}
function showResultTL(res) {
    if(res.length == 0) {
        window.location = 'index.php';
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            _("tableTL").innerHTML = xhttp.responseText;
            _("nav_allTL").style.display="none";
            _("messageTL").style.display="none";
        }
    }
    xhttp.open("GET","search.php?w="+res,true);
    xhttp.send();
}
function listAllPL(pl) {
    pl = "pl";
    // AJAX call for redraw.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "listAllPL.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
		    _("table").innerHTML = xhttp.responseText;
            //_("nav_all").style.display="none";
            _("message").style.display="none";
        }
    }
    xhttp.send("pl="+pl);
}
function listAllTL(tl) {
    tl = "tl";
    // AJAX call for table.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "listAllTL.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
		    _("tableTL").innerHTML = xhttp.responseText;
            //_("nav_all").style.display="none";
            _("messageTL").style.display="none";
        }
    }
    xhttp.send("tl="+tl);
}
//toggle switcher (expander)
var tog = 1;
function hideElement() {
    var h = document.getElementsByClassName("hideElement");
    var arrow = _("arrow");
    if(tog == 1) {
        var i = 0;
        while(i<h.length){
            h[i].style.display = "none";
            arrow.style.color = "red";
            arrow.style.transform = "rotate(90deg)";
            tog = 0;
            i++;
        }
    } else {
        var j = 0;
        while(j<h.length){
            h[j].style.display = "table-cell";
            arrow.style.color = "green";
            arrow.style.transform = "rotate(-90deg)";
            tog = 1;
            j++;
        }
    }
}
//Updates Database FAI ID by pressing ENTER (keyCode 13)
function fid(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            //var tbl = _("table");
            //var tRows = tbl.rows.length;
            //for (i=1; i < tRows; i++) {
                //var int = e.value;
                //if(!isNaN(tbl.rows[i].cells[0].innerHTML)) { 
                    //var pilot_id = parseInt(tbl.rows[i].cells[0].innerHTML);
                //}
            //}
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var new_value = e.value;
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "crud.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			    //console.log('Proslo! :)');
    	        }
            }
            xhttp.send("new_value="+new_value+"&pilot_id="+pilot_id+"&ident="+"fid");
            e.blur();
        } else {
            e.blur();
        }
    }
}
//Updates Database CIVL ID by pressing ENTER (keyCode 13)
function cid(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var new_value = e.value;
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "crud.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			    //console.log('Proslo! :)');
    	        }
            }
            xhttp.send("new_value="+new_value+"&pilot_id="+pilot_id+"&ident="+"cid");
            e.blur();
        } else {
            e.blur();
        }
    }
}
//Updates Database NATIONAL LICENCE by pressing ENTER (keyCode 13)
function nfid(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var new_value = e.value;
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "crud.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			    //console.log('Proslo! :)');
    	        }
            }
            xhttp.send("new_value="+new_value+"&pilot_id="+pilot_id+"&ident="+"nfid");
            e.blur();
        } else {
            e.blur();
        }
    }
}
//Updates Database NATION_RANK by pressing ENTER (keyCode 13)
function nr(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var new_value = e.value;
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "crud.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			    //console.log('Proslo! :)');
    	        }
            }
            xhttp.send("new_value="+new_value+"&pilot_id="+pilot_id+"&ident="+"nr");
            e.blur();
        } else {
            e.blur();
        }
    }
}
//Updates Database ALOCATION_NUMBER by pressing ENTER (keyCode 13)
function an(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var new_value = e.value;
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "crud.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			    //console.log('Proslo! :)');
    	        }
            }
            xhttp.send("new_value="+new_value+"&pilot_id="+pilot_id+"&ident="+"an");
            e.blur();
        } else {
            e.blur();
        }
    }
}
