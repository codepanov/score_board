<?php
    include_once("../php_includes/check_login_status.php");
    if($user_ok == true) {
        echo "Welcome ".ucfirst($_SESSION['username']);
        echo '
        <form action="../logout.php">
            <button type="submit" value="submit">Logout</button>
        </form>
    ';
    } else {
        header("location: ../index.html");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alocaton</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="main.js"></script>
</head>
<body>
<h1>Alocation page</h1>
<hr>
<h2>Pilots table</h2>
<div class="wrapper">
    <div class="menu"> <!-- START MENU -->
        <div class="search_div">
            <input class="mdb-select search" type="text" size="30" placeholder="Search by pilot name" onkeyup="showResult(this.value)">
        </div>
        <div class="counry_div">
            <!--<label for="country">Choose country:</label>-->
            <select name="country" id="cnt" class="mdb-select md-form">
                <?php
                    include_once("../../public_html/php_includes/country_options.php");
                ?>
            </select>
        </div>
        <div class="buttons_div">
            <button class="btn btn-default" type="button" onclick="list()">List selected</button>
            <button class="btn btn-default" type="button" onclick="window.location = 'index.php'">Refresh table</button>
            <!--<button class="btn btn-default" type="button" onclick="listAllPL(this.value)" value="pl">List all pilots</button>-->
            <button class="btn btn-danger" type="button" onclick="update()">Update changes</button>
            <button class="btn btn-primary" type="button" onclick="window.location = 'score.php'">Go to SCORE table</button>
        </div>
    </div> <!-- END MENU -->
    <br><br>
    <div class="wrapp">
    <table id="table" class="table table-bordered table-striped table-responsive">
        <th>ID</th>
        <th>Pilot Name</th>
        <th>G</th>
        <th>Country</th>
        <th class="thExpand">Nation rank<!--<span id="arrow" style="color: green; cursor: pointer;" onclick="hideElement()">&nbsp;&#9654;</span>--></th>
        <th class="thExpand">Aloc number</th>
        <th class="hideElement">NAC approval</th>
        <th class="hideElement">Local Rules Criteria</th>
        <th class="hideElement">Status</th>
        <th class="hideElement">Team</th>
        <th>Email</th>
        <th>Team Leader</th>
        <th>Phone Number</th>
        <th>WhatsApp</th>
        <th>FAI ID</th>
        <th>Civl ID</th>
        <th>National Licence</th>
        <th>T-shirt</th>
        <th>Vegan</th>
        <th>Blood</th>
    <?php
        include_once("../../public_html/php_includes/db_con.php");
        
        /////////////////////////////////////////////////////////// START PAGINATION
        $rpp = 10;
        if(isset($_GET['page']) && !empty($_GET['page'])) {
            $currentPage = $_GET['page'];
        } else {
            $currentPage = 1;
        }
        $startFrom = ($currentPage * $rpp) - $rpp;
        $total = "SELECT * FROM applied_pilots";
        $result = mysqli_query($db_con, $total);
        $rows = mysqli_num_rows($result);
        $lastPage = ceil($rows/$rpp);
        $firstPage = 1;
        $nextPage = $currentPage + 1;
        $previousPage = $currentPage - 1;
        ///////////////////////////////////////////////////////////// END PAGINATION
        
        $country = $_POST['country'];
        //$sql = "SELECT id, activated, nation_rank, gender, Nac_order_number, LR_Criteria, Status, Team, f_name, l_name, country FROM applied_pilots LIMIT $startFrom, $rpp"; // LIMIT $startFrom, $rpp Dodato zbog PAGINATION
        $sql = "SELECT * FROM applied_pilots LIMIT $startFrom, $rpp";
    	$result = $db_con->query($sql);
    	$i = 0;
        while($row = $result->fetch_assoc()) {
            if ($result->num_rows > 0 && $row['activated'] != 0) {
                if($row['gender'] == "male") {
                    $gender = "M";
                } else $gender = "F";
                $i++;
                $row['team_leader'] == "ytl" ? $tl = "YES" : $tl = "NO";
                $row['vegan'] == "yv" ? $vg = "Vegan" : $vg = "NO";
                $row['nation_rank'] == 0 ? $nr = "" : $nr = $row['nation_rank'];
                $row['aloc_number'] == 0 ? $an = "" : $an = $row['aloc_number'];
            		echo '
                        <tr>
                            <td id="n'.$i.'">'.$row['id'].'</td>
                            <td>' . $row['f_name'] . ' ' . $row['l_name'] . '</td>
                            <td>' . $gender . '</td>
							<td style="width: 1%; white-space: nowrap;">' . $row['country'] . '</td>
							<!--<td><input type="text" value="0" style="text-align: center; width: 100%; color: lightgrey;" disabled></td>-->
							<td class="dbChange"><input type="number" class="inp" value="' . $nr . '" onkeyup="nr(this)"></input>
							<td class="dbChange"><input type="number" class="inp" value="' . $an . '" onkeyup="an(this)"></input>
                            <td>
                                <select name="nac" id="nod">
                                    <option value="NAC Aproval"'; if($row['Nac_order_number'] == "NAC Approval needed") echo "selected"; echo'>NAC Approval needed</option>
                                    <option value="1"'; if($row['Nac_order_number'] == "1") echo "selected"; echo'>1</option>
                                    <option value="2"'; if($row['Nac_order_number'] == "2") echo "selected"; echo'>2</option>
                                    <option value="3"'; if($row['Nac_order_number'] == "3") echo "selected"; echo'>3</option>
                                    <option value="4"'; if($row['Nac_order_number'] == "4") echo "selected"; echo'>4</option>
                                    <option value="5"'; if($row['Nac_order_number'] == "5") echo "selected"; echo'>5</option>
                                    <option value="6"'; if($row['Nac_order_number'] == "6") echo "selected"; echo'>6</option>
                                    <option value="7"'; if($row['Nac_order_number'] == "7") echo "selected"; echo'>7</option>
                                    <option value="8"'; if($row['Nac_order_number'] == "8") echo "selected"; echo'>8</option>
                                    <option value="9"'; if($row['Nac_order_number'] == "9") echo "selected"; echo'>9</option>
                                    <option value="10"'; if($row['Nac_order_number'] == "10") echo "selected"; echo'>10</option>
                                    <option value="11"'; if($row['Nac_order_number'] == "11") echo "selected"; echo'>11</option>
                                    <option value="12"'; if($row['Nac_order_number'] == "12") echo "selected"; echo'>12</option>
                                </select>
                            </td>
                            <td>
                                <select name="criteria" id="lrc">
                                    <option value="null"'; if($row['LR_Criteria'] == null) echo "selected"; echo'>Default</option>
                                    <option value="qual"'; if($row['LR_Criteria'] == "Qualified") echo "selected"; echo'>Qualified</option>
                                    <option value="miss"'; if($row['LR_Criteria'] == "WPRS Missing criteria") echo "selected"; echo'>WPRS Missing criteria</option>
                                </select>
                            </td>
                            <td>
                                <select name="status" id="stat">
                                    <option value="list"'; if($row['Status'] == "Waiting list") echo "selected"; echo'>Waiting list</option>
                                    <option value="pay"'; if($row['Status'] == "Waiting payment") echo "selected"; echo'>Waiting payment</option>
                                    <option value="cnfrm"'; if($row['Status'] == "Confirmed") echo "selected"; echo'>Confirmed</option>
                                    <option value="cnsl"'; if($row['Status'] == "Canceled") echo "selected"; echo'>Canceled</option>
                                </select>
                            </td>
                            <td>
                                <select name="team" id="team">
                                    <option value="null"'; if($row['Team'] == null) echo "selected"; echo'>Default</option>
                                    <option value="team"'; if($row['Team'] == "Team pilot") echo "selected"; echo'>Team pilot</option>
                                </select>
                            </td>
                            <td>' . $row['email'] . '</td>
                            <td>' . $tl . '</td>
                            <td>' . $row['cell'] . '</td>
                            <td>' . $row['wapp'] . '</td>
                            <td class="dbChange"><input class="inp" value="'.$row['faiid'].'" onkeyup="fid(this)"></input></td>
                            <td class="dbChange"><input class="inp" value="'.$row['civlid'].'" onkeyup="cid(this)"></input></td>
                            <td class="dbChange"><input class="inp" value="'.$row['natflid'].'" onkeyup="nfid(this)"></input></td>
                            <td>' . strtoupper($row['t_shirt']) . '</td>
                            <td>' . $vg . '</td>
                            <td>' . $row['blood'] . '</td>
                        </tr>
        		    ';
            }
        }
    ?>
    </table>

    <!-- Modal -->
    <div id="message"></div>
    
    <!-- //////////////////////////////////////////////////////////////// START PAGINATION NAVIGATION -->
    <nav aria-label="Page navigation" class="nav_all" id="nav_all">
    <ul class="pagination">
    <?php if($currentPage != $firstPage) { ?>
    <li class="page-item">
    <a class="page-link" href="?page=<?php echo $firstPage ?>" tabindex="-1" aria-label="Previous">
    <span aria-hidden="true">First</span>
    </a>
    </li>
    <?php } ?>
    <?php if($currentPage >= 2) { ?>
    <li class="page-item"><a class="page-link" href="?page=<?php echo $previousPage ?>"><?php echo $previousPage ?></a></li>
    <?php } ?>
    <li class="page-item active"><a class="page-link" href="?page=<?php echo $currentPage ?>"><?php echo $currentPage ?></a></li>
    <?php if($currentPage != $lastPage) { ?>
    <li class="page-item"><a class="page-link" href="?page=<?php echo $nextPage ?>"><?php echo $nextPage ?></a></li>
    <li class="page-item">
    <a class="page-link" href="?page=<?php echo $lastPage ?>" aria-label="Next">
    <span aria-hidden="true">Last</span>
    </a>
    </li>
    <?php } ?>
    </ul>
    </nav>
    <!-- //////////////////////////////////////////////////////////////// END PAGINATION NAVIGATION -->
    
    <br>
    <div class="short">
        <p style="padding:5px; font-size:16px;"><b>Pilots short list</b></p>
        <table class="table-striped">
            <th style="width: 200px;">Applied countries</th>
            <th>Number of activated pilots</th>
            <?php
                $sql = "SELECT id, activated, country, COUNT(country) FROM applied_pilots GROUP BY country HAVING COUNT(country) > 0 ORDER BY id ASC";
        	    $result = $db_con->query($sql);
                while($row = $result->fetch_assoc()) {
                    if ($result->num_rows > 0 && $row['activated'] != 0) {
                        echo '
                            <tr>
                                <td>' . $row['country'] . '</td>
                                <td style="text-align: center;">' . $row['COUNT(country)'] . '</td>
                            </tr>
                        ';
                    }
                }
            ?>
            <th>Total pilots</th>
            <?php
                $sql = "SELECT activated FROM applied_pilots WHERE activated = '1'";
                $result = $db_con->query($sql);
                $rowcount=mysqli_num_rows($result);
                echo '
                    <th style="text-align:center;">' . $rowcount . '</th>
                ';
            ?>
        </table>
    </div> <!-- END SHORT -->
    <div class="short">
        <p style="padding:5px; font-size:16px;"><b>Team Leaders short list</b></p>
        <table class="table-striped">
            <th style="width: 200px;">Applied countries</th>
            <th>Number of activated tls</th>
            <?php
                $sql = "SELECT id, activated, country, COUNT(country) FROM applied_tls GROUP BY country HAVING COUNT(country) > 0 ORDER BY id ASC";
        	    $result = $db_con->query($sql);
                while($row = $result->fetch_assoc()) {
                    if ($result->num_rows > 0 && $row['activated'] != 0) {
                        echo '
                            <tr>
                                <td>' . $row['country'] . '</td>
                                <td style="text-align: center;">' . $row['COUNT(country)'] . '</td>
                            </tr>
                        ';
                    }
                }
            ?>
            <th>Total team leaders</th>
            <?php
                $sql = "SELECT activated FROM applied_tls WHERE activated = '1'";
                $result = $db_con->query($sql);
                $rowcount=mysqli_num_rows($result);
                echo '
                    <th style="text-align:center;">' . $rowcount . '</th>
                ';
            ?>
        </table>
    </div> <!-- END SHORT -->
    
    <!--<div class="short"> <!-- START EXCEL -->
    <!--    <p style="padding:5px; font-size:16px;"><b>Pilots Excel file</b></p>
        <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=B6AD32C75BB5B42%218566&authkey=%21ABXaYJWYjEFO9XY&em=2&wdAllowInteractivity=False&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>
    </div> <!-- END EXCEL -->
    <!--<div class="short"> <!-- START EXCEL -->
    <!--    <p style="padding:5px; font-size:16px;"><b>Team Leaders Excel file</b></p>
        <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=B6AD32C75BB5B42%218567&authkey=%21ALPh_t3cfOqU9MA&em=2&wdAllowInteractivity=False&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>
    </div> <!-- END EXCEL -->
    <div class="spacer"></div>
    <hr style="margin-bottom: 40px;">
    
    
    
    <!-- //////////////////////////////////////////////////////////////// START TEAM LEADERS TABLE -->
    
    <!-- Modal -->
    <div id="messageTL"></div>
    
    <h2>Team leaders table</h2>
    <div class="menu"> <!-- START MENU -->
        <div class="search_div">
            <input class="mdb-select search" type="text" size="30" placeholder="Search by pilot name" onkeyup="showResultTL(this.value)">
        </div>
        <div class="counry_div">
            <!--<label for="countryTL">Choose country:</label>-->
            <select name="countryTL" id="cntTL" class="mdb-select md-form" disabled style="opacity: 0.5;">
                <?php
                    include("../../public_html/php_includes/country_options.php");
                ?>
            </select>
        </div>
        <div class="buttons_div">
            <button class="btn btn-default" type="button" onclick="listTL()" disabled>List selected</button>
            <button class="btn btn-default" type="button" onclick="window.location = 'index.php'" disabled>List all tls</button>
            <!--<button class="btn btn-default" type="button" onclick="listAllTL(this.value)" value="tl">List all tls</button>-->
            <button class="btn btn-danger" type="button" onclick="updateTL()">Update changes</button>
        </div>
    </div> <!-- END MENU -->
    <br><br>

    <table id="tableTL" class="table table-bordered table-striped table-responsive">
        <th>ID</th>
        <th>TL Name</th>
        <th>G</th>
        <th>Country</th>
        <th>NAC approval</th>
        <th>Status</th>
    <?php
        include_once("../../public_html/php_includes/db_con.php");
        
        /////////////////////////////////////////////////////////// START PAGINATION
        $rpp = 20;
        if(isset($_GET['pagetl']) && !empty($_GET['pagetl'])) {
            $currentPage = $_GET['pagetl'];
        } else {
            $currentPage = 1;
        }
        $startFrom = ($currentPage * $rpp) - $rpp;
        $total = "SELECT * FROM applied_tls";
        $result = mysqli_query($db_con, $total);
        $rows = mysqli_num_rows($result);
        $lastPage = ceil($rows/$rpp);
        $firstPage = 1;
        $nextPage = $currentPage + 1;
        $previousPage = $currentPage - 1;
        ///////////////////////////////////////////////////////////// END PAGINATION
        
        $country = $_POST['country'];
        $sql = "SELECT id, activated, Nac_order_number, Status, f_name, l_name, gender, country FROM applied_tls LIMIT $startFrom, $rpp"; // LIMIT $startFrom, $rpp Dodato zbog PAGINATION
    	$result = $db_con->query($sql);
    	$i = 0;
        while($row = $result->fetch_assoc()) {
            if ($result->num_rows > 0 && $row['activated'] != 0) {
                $i++;
                $row['gender'] == "male" ? $tlgender = "M" : $tlgender = "F";
            		echo '
                        <tr>
                            <td id="n'.$i.'">'.$row['id'].'</td>
                            <td>' . $row['f_name'] . ' ' . $row['l_name'] . '</td>
                            <td>' . $tlgender . '</td>
                            <td>' . $row['country'] . '</td>
                            <td>
                                <select name="nac" id="nodTL">
                                    <option value="NAC Aproval"'; if($row['Nac_order_number'] == "NAC Approval needed") echo "selected"; echo'>NAC Approval needed</option>
                                    <option value="1"'; if($row['Nac_order_number'] == "1") echo "selected"; echo'>1</option>
                                    <option value="2"'; if($row['Nac_order_number'] == "2") echo "selected"; echo'>2</option>
                                    <option value="3"'; if($row['Nac_order_number'] == "3") echo "selected"; echo'>3</option>
                                    <option value="4"'; if($row['Nac_order_number'] == "4") echo "selected"; echo'>4</option>
                                    <option value="5"'; if($row['Nac_order_number'] == "5") echo "selected"; echo'>5</option>
                                    <option value="6"'; if($row['Nac_order_number'] == "6") echo "selected"; echo'>6</option>
                                    <option value="7"'; if($row['Nac_order_number'] == "7") echo "selected"; echo'>7</option>
                                    <option value="8"'; if($row['Nac_order_number'] == "8") echo "selected"; echo'>8</option>
                                    <option value="9"'; if($row['Nac_order_number'] == "9") echo "selected"; echo'>9</option>
                                    <option value="10"'; if($row['Nac_order_number'] == "10") echo "selected"; echo'>10</option>
                                    <option value="11"'; if($row['Nac_order_number'] == "11") echo "selected"; echo'>11</option>
                                    <option value="12"'; if($row['Nac_order_number'] == "12") echo "selected"; echo'>12</option>
                                </select>
                            </td>
                            <td>
                                <select name="status" id="stat">
                                    <option value="list"'; if($row['Status'] == "Waiting list") echo "selected"; echo'>Waiting list</option>
                                    <option value="pay"'; if($row['Status'] == "Waiting payment") echo "selected"; echo'>Waiting payment</option>
                                    <option value="cnfrm"'; if($row['Status'] == "Confirmed") echo "selected"; echo'>Confirmed</option>
                                </select>
                            </td>  
                        </tr>
        		';
            }
        }
    ?>
    </table>

    <!-- Modal -->
    <div id="messageTL"></div>
    
    <!-- //////////////////////////////////////////////////////////////// START PAGINATION NAVIGATION -->
    <nav aria-label="Page navigation" class="nav_all" id="nav_allTL">
    <ul class="pagination">
    <?php if($currentPage != $firstPage) { ?>
    <li class="page-item">
    <a class="page-link" href="?pagetl=<?php echo $firstPage ?>" tabindex="-1" aria-label="Previous">
    <span aria-hidden="true">First</span>
    </a>
    </li>
    <?php } ?>
    <?php if($currentPage >= 2) { ?>
    <li class="page-item"><a class="page-link" href="?pagetl=<?php echo $previousPage ?>"><?php echo $previousPage ?></a></li>
    <?php } ?>
    <li class="page-item active"><a class="page-link" href="?pagetl=<?php echo $currentPage ?>"><?php echo $currentPage ?></a></li>
    <?php if($currentPage != $lastPage) { ?>
    <li class="page-item"><a class="page-link" href="?pagetl=<?php echo $nextPage ?>"><?php echo $nextPage ?></a></li>
    <li class="page-item">
    <a class="page-link" href="?pagetl=<?php echo $lastPage ?>" aria-label="Next">
    <span aria-hidden="true">Last</span>
    </a>
    </li>
    <?php } ?>
    </ul>
    </nav>
    <!-- //////////////////////////////////////////////////////////////// END PAGINATION NAVIGATION -->

    </div> <!-- END WRAPP -->
</div> <!-- END WRAPPER -->
</body>
</html>