<table id="table" class="table table-bordered table-striped table-responsive">
        <th>ID</th>
        <th>Pilot Name</th>
        <th>G</th>
        <th>Country</th>
        <th class="thExpand">Nation rank<!--<span id="arrow" style="color: green; cursor: pointer;" onclick="hideElement()">&nbsp;&#9654;</span>--></th>
        <th class="thExpand">Aloc number</th>
        <th class="hideElement">NAC order number</th>
        <th class="hideElement">Local Rules Criteria</th>
        <th class="hideElement">Status</th>
        <th class="hideElement">Team</th>
        <th>Email</th>
        <th>Team Leader</th>
        <th>Phone Number</th>
        <th>WhatsApp</th>
        <th>FAI ID</th>
        <th>Civl ID</th>
        <th>National Licence</th>
        <th>T-shirt</th>
        <th>Vegan</th>
        <th>Blood</th>
<?php
    include_once("../../public_html/php_includes/db_con.php");
    $country = $_POST['country'];
    //$sql = "SELECT id, activated, gender, Nac_order_number, LR_Criteria, Status, Team, f_name, l_name, country FROM applied_pilots WHERE country = '$country' ORDER BY Nac_order_number";
    $sql = "SELECT * FROM applied_pilots WHERE country = '$country' ORDER BY Nac_order_number";
	$result = $db_con->query($sql);
	$i = 0;
    while($row = $result->fetch_assoc()) {
        if ($result->num_rows > 0 && $row['activated'] != 0) {
            if($row['gender'] == "male") {
                $gender = "M";
            } else $gender = "F";
                $i++;
        	    $row['team_leader'] == "ytl" ? $tl = "YES" : $tl = "NO";
                $row['vegan'] == "yv" ? $vg = "Vegan" : $vg = "NO";
                $row['nation_rank'] == 0 ? $nr = "" : $nr = $row['nation_rank'];
                $row['aloc_number'] == 0 ? $an = "" : $an = $row['aloc_number'];
            		echo '
                        <tr>
                            <td id="n'.$i.'">'.$row['id'].'</td>
                            <td style="width: 1%; white-space: nowrap;">' . ucfirst(strtolower($row['f_name'])) . ' ' . ucfirst(strtolower($row['l_name'])) . '</td>
                            <td>' . $gender . '</td>
                            <td style="width: 1%; white-space: nowrap;">' . $row['country'] . '</td>
                            <td class="dbChange"><input type="number" class="inp" value="' . $nr . '" onkeyup="nr(this)"></input>
							<td class="dbChange"><input type="number" class="inp" value="' . $an . '" onkeyup="an(this)"></input>
                            <td class="hideElement">
                                <select name="nac" id="nod">
                                    <option value="NAC Aproval"'; if($row['Nac_order_number'] == "NAC Approval needed") echo "selected"; echo'>NAC Approval needed</option>
                                    <option value="1"'; if($row['Nac_order_number'] == "1") echo "selected"; echo'>1</option>
                                    <option value="2"'; if($row['Nac_order_number'] == "2") echo "selected"; echo'>2</option>
                                    <option value="3"'; if($row['Nac_order_number'] == "3") echo "selected"; echo'>3</option>
                                    <option value="4"'; if($row['Nac_order_number'] == "4") echo "selected"; echo'>4</option>
                                    <option value="5"'; if($row['Nac_order_number'] == "5") echo "selected"; echo'>5</option>
                                    <option value="6"'; if($row['Nac_order_number'] == "6") echo "selected"; echo'>6</option>
                                    <option value="7"'; if($row['Nac_order_number'] == "7") echo "selected"; echo'>7</option>
                                    <option value="8"'; if($row['Nac_order_number'] == "8") echo "selected"; echo'>8</option>
                                    <option value="9"'; if($row['Nac_order_number'] == "9") echo "selected"; echo'>9</option>
                                    <option value="10"'; if($row['Nac_order_number'] == "10") echo "selected"; echo'>10</option>
                                    <option value="11"'; if($row['Nac_order_number'] == "11") echo "selected"; echo'>11</option>
                                    <option value="12"'; if($row['Nac_order_number'] == "12") echo "selected"; echo'>12</option>
                                </select>
                            </td>
                            <td class="hideElement">
                                <select name="criteria" id="lrc">
                                    <option value="null"'; if($row['LR_Criteria'] == null) echo "selected"; echo'>Default</option>
                                    <option value="qual"'; if($row['LR_Criteria'] == "Qualified") echo "selected"; echo'>Qualified</option>
                                    <option value="miss"'; if($row['LR_Criteria'] == "WPRS Missing criteria") echo "selected"; echo'>WPRS Missing criteria</option>
                                </select>
                            </td>
                            <td class="hideElement">
                                <select name="status" id="stat">
                                    <option value="list"'; if($row['Status'] == "Waiting list") echo "selected"; echo'>Waiting list</option>
                                    <option value="pay"'; if($row['Status'] == "Waiting payment") echo "selected"; echo'>Waiting payment</option>
                                    <option value="cnfrm"'; if($row['Status'] == "Confirmed") echo "selected"; echo'>Confirmed</option>
                                </select>
                            </td>
                            <td class="hideElement">
                                <select name="team" id="team">
                                    <option value="null"'; if($row['Team'] == null) echo "selected"; echo'>Default</option>
                                    <option value="team"'; if($row['Team'] == "Team pilot") echo "selected"; echo'>Team pilot</option>
                                </select>
                            </td>
                            <td>' . $row['email'] . '</td>
                            <td>' . $tl . '</td>
                            <td>' . $row['cell'] . '</td>
                            <td>' . $row['wapp'] . '</td>
                            <td class="dbChange"><input class="inp" value="'.$row['faiid'].'" onkeyup="fid(this)"></input></td>
                            <td class="dbChange"><input class="inp" value="'.$row['civlid'].'" onkeyup="cid(this)"></input></td>
                            <td class="dbChange"><input class="inp" value="'.$row['natflid'].'" onkeyup="nfid(this)"></input></td>
                            <td>' . strtoupper($row['t_shirt']) . '</td>
                            <td>' . $vg . '</td>
                            <td>' . $row['blood'] . '</td>
                        </tr>
        		    ';
        }
    }
?>
</table>