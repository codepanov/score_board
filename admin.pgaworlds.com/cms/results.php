<?php

if(isset($_POST["score"])) {
    // declared array
    $total_single = array();
    $total_team = array();
    
    // db connection
    include_once("../../public_html/php_includes/db_con.php");
    
    // calculate result for every pilot per each series
    
    // get db data and cast to signed for php to use valid integers
    $sql = "SELECT id, Team, CAST(round_1 AS SIGNED), CAST(round_2 AS SIGNED), CAST(round_3 AS SIGNED), CAST(round_4 AS SIGNED), CAST(round_5 AS SIGNED), CAST(round_6 AS SIGNED), CAST(round_7 AS SIGNED), CAST(round_8 AS SIGNED), CAST(round_9 AS SIGNED), CAST(round_10 AS SIGNED), CAST(round_11 AS SIGNED), CAST(round_12 AS SIGNED), total FROM applied_pilots";
    
    $result = $db_con->query($sql);
    
    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            
            $id = $row['id'];
            
            // make an array of results in every pass
            $total_single = array($row['CAST(round_1 AS SIGNED)'], $row['CAST(round_2 AS SIGNED)'], $row['CAST(round_3 AS SIGNED)'], $row['CAST(round_4 AS SIGNED)'], $row['CAST(round_5 AS SIGNED)'], $row['CAST(round_6 AS SIGNED)'], $row['CAST(round_7 AS SIGNED)'], $row['CAST(round_8 AS SIGNED)'], $row['CAST(round_9 AS SIGNED)'], $row['CAST(round_10 AS SIGNED)'], $row['CAST(round_11 AS SIGNED)'], $row['CAST(round_12 AS SIGNED)']);
            
            // remove null array members
            $total_single = array_filter($total_single, function($v) { return !is_null($v); });
            // do not consider empty arrays (just for developing stage), as it is assumed that this code will run when all arrays will have at least one member
            if(!empty($total_single)) {
                
                //check if array has minimum 5 members
                if(count($total_single) >= 5) {
                    sort($total_single); // sort from lowest value
                    array_pop($total_single); // exclude last (highest value) member
                    
                    // sum values
                    $res = array_sum($total_single);
                    
                    // update db total field
                    $sql = "UPDATE applied_pilots SET total=$res WHERE id=$id";
                   
                    if (mysqli_query($db_con, $sql)) {
                        //echo "Database updated successfully!";
                    } else {
                        echo "Error updating record: " . mysqli_error($db_con);
                    }
                } else {
                    
                    // sum values
                    $res = array_sum($total_single);
                    
                    // update db total field
                    $sql = "UPDATE applied_pilots SET total=$res WHERE id=$id";
                    
                    if (mysqli_query($db_con, $sql)) {
                        //echo "Database updated successfully!";
                    } else {
                        echo "Error updating record: " . mysqli_error($db_con);
                    }
                }
                //ovde update kolone koju cu dodati u bazu, a iz koje ce se racunati timski rezultati
                $res = array_sum($total_single);
                $sql = "UPDATE applied_pilots SET total_team=$res WHERE id=$id";
                    
                if (mysqli_query($db_con, $sql)) {
                    //echo "Database updated successfully!";
                } else {
                    echo "Error updating record: " . mysqli_error($db_con);
                }
            }
    
        }
    }
    
    // calculate result for every team per each series
     
    // get db data and cast to signed for php to use valid integers
    // umesto total, uzimace se rezultati iz novokreirane kolone (total_team) iz reda 61
    $sql = "SELECT DISTINCT country, GROUP_CONCAT(CAST(total AS SIGNED)), GROUP_CONCAT(CAST(total_team AS SIGNED)) FROM applied_pilots WHERE Team = 'Team pilot' GROUP BY country";
    
    $result = $db_con->query($sql);
    
    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            
            // echo $row['country'] . "<br>"; exit;
            // echo $row['GROUP_CONCAT(CAST(total AS SIGNED))']  . "<br>";
            
            $country = $row['country'];
            
            // make an array of results in every pass | stari i novi nacin
            $total_team_old = explode("," ,$row['GROUP_CONCAT(CAST(total AS SIGNED))']); // array does not have null values
            $total_team = explode("," ,$row['GROUP_CONCAT(CAST(total_team AS SIGNED))']); // array does not have null values
            
            sort($total_team_old); // sort from lowest value
            sort($total_team); // sort from lowest value
            
            if(count($total_team) == 1) { // for 1 team member, add 3 x 500, for 2 add 2 x 500, for 3 add 500
                $res_old = array_sum($total_team_old) + 1500;
                $res = array_sum($total_team) + 1500;
                // update db total field
                $sql = "INSERT INTO team_results_total (country, result, result_old) VALUES ('$country', '$res', '$res_old')";
                
                if (mysqli_query($db_con, $sql)) {
                    //echo "Database updated successfully!";
                } else {
                    echo "Error updating record: " . mysqli_error($db_con);
                }
            } elseif(count($total_team) == 2) {
                $res_old = array_sum($total_team_old) + 1000;
                $res = array_sum($total_team) + 1000;
                // update db total field
                $sql = "INSERT INTO team_results_total (country, result, result_old) VALUES ('$country', '$res', '$res_old')";
    
                if (mysqli_query($db_con, $sql)) {
                    //echo "Database updated successfully!";
                } else {
                    echo "Error updating record: " . mysqli_error($db_con);
                }
            } elseif(count($total_team) == 3) {
                $res_old = array_sum($total_team_old) + 500;
                $res = array_sum($total_team) + 500;
                // update db total field
                $sql = "INSERT INTO team_results_total (country, result, result_old) VALUES ('$country', '$res', '$res_old')";
                
                if (mysqli_query($db_con, $sql)) {
                    //echo "Database updated successfully!";
                } else {
                    echo "Error updating record: " . mysqli_error($db_con);
                }
            } else {
                $res_old = array_map('array_sum', array_chunk($total_team_old, 4)) [0];
                $res = array_map('array_sum', array_chunk($total_team, 4)) [0];
                // update db total field
                $sql = "INSERT INTO team_results_total (country, result, result_old) VALUES ('$country', '$res', '$res_old')";
                
                if (mysqli_query($db_con, $sql)) {
                    //echo "Database updated successfully!";
                } else {
                    echo "Error updating record: " . mysqli_error($db_con);
                }
            }
        }
        echo "Database updated successfully!";
    }
}
