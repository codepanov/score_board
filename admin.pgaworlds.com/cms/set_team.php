<?php
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    if(isset($_POST["team"])){
        $array = json_decode(htmlspecialchars_decode($_POST["team"]));
        include_once("../../public_html/php_includes/db_con.php");
        
        $id = test_input($array[0]); // Pilot id
        $team = test_input($array[1]); // Team
        
        if($team == 'Not in team') $team = null; //Team
        
        $sql = "UPDATE applied_pilots SET Team='$team' WHERE id='$id'";
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    if(isset($_POST["order_number"])){
        
        include_once("../../public_html/php_includes/db_con.php");
        
        $id = test_input($_POST["pilot_id"]); // Pilot id
        $order_number = test_input($_POST["order_number"]); // Order number
        
        $sql = "UPDATE applied_pilots SET order_number=$order_number WHERE id=$id";
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    if(isset($_POST["score"])){
        
        include_once("../../public_html/php_includes/db_con.php");
        
        $score = test_input($_POST["score"]);
        $id = test_input($_POST["pilot_id"]);
        $round = test_input($_POST["ident"]);
        
        $sql = "UPDATE applied_pilots SET $round=$score WHERE id=$id";
        
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    /*
    $sql = "INSERT INTO applied_pilots 
                (Nac_order_number, LR_criteria, Status, Team)
                VALUES 
                    (1, 5, 8, 9),
                    (2, 10, 8, 3),
                    (3, 8, 3, 5),
                    (4, 10, 7, 6)
                ON DUPLICATE KEY UPDATE 
                    Nac_order_number = VALUES(Nac_order_number),
                    LR_criteria = VALUES(LR_criteria),
                    Status = VALUES(Status),
                    Team = VALUES(Team)";
    $query = mysqli_query($db_con, $sql);
    */
    