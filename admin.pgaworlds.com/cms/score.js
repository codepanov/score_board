function _(x){
	return document.getElementById(x);
}

function showPilots(res) {
    if(res.length == 0) {
        window.location = 'score.php';
    }
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            _("table").innerHTML = xhttp.responseText; // gadja trenutnu tabelu i u nju vraca odgovor
            _("nav_all").style.display="none"; // gasi pagination links
            _("message").style.display="none"; // gasi div za povratnu poruku ajaxa
        }
    }
    xhttp.open("GET","search_pilots.php?q="+res,true);
    xhttp.send();
}

function list() {
    let country = _("country").value;
    
    // AJAX call for table.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "country_table.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
		    _("table").innerHTML = xhttp.responseText;
            _("nav_all").style.display="none";
            _("message").style.display="none";
        }
    }
    xhttp.send("country="+country);
}

function update_db() {
    var tbl = _("table");
    var tRows = tbl.rows.length; // ovde dobijem table rows collection, pa sa 'lenght' koliko zapravo imam <th> i <tr> tojest horizontalnih redova
    for (i=1; i < tRows; i++) { // Preskacem <th> text, time sto loop pocinjem od 1
    var output = []; // inicijalizujem niz gde cu sve da pakujem
        for(j=0; j < 5; j++) { // loop ovde pocinje od 0, da bih ugrabio id takmicara iz baze i ovaj loop pici kroz polja u jednom redu. Brojac se zavrsava na 5 da ne uzima u obzir nista iza kolone "Team". Ako ide dalje, ima problem nextSibling sa iscitavanjem nekih polja i prijavljuje gresku.
            var select = tbl.rows[i].cells[j].firstChild.nextSibling; //Stavljam ovo u varijablu jer je predugacko, a to je ono sto pise u select polju
            if(!isNaN(tbl.rows[i].cells[j].innerHTML)) { //proverava da li je u pitanju broj i preskace ime pilota
                var pilot_id = parseInt(tbl.rows[i].cells[j].innerHTML); // parseInt, iz stringa u int
                output.push(pilot_id);
            }
            if(tbl.rows[i].cells[j].innerHTML.search("select") != -1) { //metoda .search() na izlazu daje -1 ako ne nadje string
                var aloc_data = select.options[select.selectedIndex].text;
                output.push(aloc_data);
            }
        }
        //console.log(output.toString());
        
        // AJAX call for crud.php to UPDATE database
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "set_team.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.onreadystatechange = function() {
	        if(xhttp.readyState == 4 && xhttp.status == 200) {
	            _("message").style.display = "block";
			    _("message").innerHTML = xhttp.responseText;
			    setTimeout(function(){_("message").style.display = "none"}, 3000);
			    //console.log('Proslo! :)');
	        }
        }
        var team = JSON.stringify(output); //Da bih poslao niz kroz POST metodu, moram da ga prethodno pretvorim u string
        xhttp.send("team="+team);
    }
}

// Updates Database by pressing ENTER (keyCode 13)
function round(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML); // id pilota
            var score = e.value; // vrednost koja se unese u polje
            var cell_index = e.parentNode.cellIndex; // indeks celije u jednom redu, treba da bude isti broj koji nosi id kolone u kojoj se celija nalazi
            var column_name = _("table").rows[0].cells.namedItem("column_"+cell_index).innerHTML; // ime kolone koje mora biti isto kao ime kolone u bazi
            console.log(score);
            
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "set_team.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    			 //   console.log('Proslo! :)');
    	        }
            }
            xhttp.send("score="+score+"&pilot_id="+pilot_id+"&ident="+column_name);
            e.blur();
        } else {
            e.blur();
        }
    }
}

function on(e) {
    if(event.keyCode === 13) {
        var c = confirm("Update Data?");
        if(c === true) {
            
            var pilot_id = parseInt(e.parentNode.parentNode.firstElementChild.innerHTML);
            var order_number = e.value;
            
            // AJAX call for crud.php to UPDATE database
            var xhttp = new XMLHttpRequest();
            xhttp.open("POST", "set_team.php", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function() {
    	        if(xhttp.readyState == 4 && xhttp.status == 200) {
    	            _("message").style.display = "block";
    			    _("message").innerHTML = xhttp.responseText;
    			    setTimeout(function(){_("message").style.display = "none"}, 3000);
    	        }
            }
            xhttp.send("order_number="+order_number+"&pilot_id="+pilot_id+"&ident="+"on");
            e.blur();
        } else {
            e.blur();
        }
    }
}

function update_score_board() {
    // AJAX call for table.php to draw new table with selected country
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "results.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
            _("message").style.display = "block";
		    _("message").innerHTML = xhttp.responseText;
		    setTimeout(function(){_("message").style.display = "none"}, 3000);
		    //console.log('Proslo! :)');
        }
    }
    let score = "score";
    xhttp.send("score="+score);
}

function team_score(param) {
    
    var cnfrm = confirm("Update " + param.value + "?");
    if(cnfrm === true) {
        var round = param.value;
        
        // AJAX call for table.php to draw new table with selected country
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "results_team.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.onreadystatechange = function() {
            if(xhttp.readyState == 4 && xhttp.status == 200) {
                _("message").style.display = "block";
    		    _("message").innerHTML = xhttp.responseText;
    		    setTimeout(function(){_("message").style.display = "none"}, 3000);
    		    //console.log('Proslo! :)');
            }
        }
        xhttp.send("round="+round);
    }
}