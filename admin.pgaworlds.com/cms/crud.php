<?php
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    
    if(isset($_POST["enc"])){
        $array = json_decode(htmlspecialchars_decode($_POST["enc"]));
        include_once("../../public_html/php_includes/db_con.php");
        
        $id = test_input($array[0]);
        $non = test_input($array[1]); //Nac_approval
        $lr_c = test_input($array[2]); //LR_Criteria
        $sts = test_input($array[3]); //Status
        $tm = test_input($array[4]); //Team
        
        if($lr_c == 'Default') $lr_c = null; //LR_Criteria
        if($tm == 'Default') $tm = null; //Team
        
        $sql = "UPDATE applied_pilots SET Nac_order_number='$non', LR_Criteria='$lr_c', Status='$sts', Team='$tm' WHERE id='$id'";
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    if(isset($_POST["tl_update"])){
        $array = json_decode(htmlspecialchars_decode($_POST["tl_update"]));
        include_once("../../public_html/php_includes/db_con.php");
        
        $id = test_input($array[0]);
        $nacappr = test_input($array[1]); //Nac_approval
        $status = test_input($array[2]); //Status
        
        $sql = "UPDATE applied_tls SET Nac_order_number='$nacappr', Status='$status' WHERE id='$id'";
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    if(isset($_POST["new_value"])){
        include_once("../../public_html/php_includes/db_con.php");
        $num = test_input($_POST["new_value"]);
        $id = test_input($_POST["pilot_id"]);
        if(test_input($_POST["ident"]) === "fid"){
            $sql = "UPDATE applied_pilots SET faiid='$num' WHERE id='$id'";
        } else if(test_input($_POST["ident"]) === "cid"){
            $sql = "UPDATE applied_pilots SET civlid='$num' WHERE id='$id'";
        } else if(test_input($_POST["ident"]) === "nfid"){
            $sql = "UPDATE applied_pilots SET natflid='$num' WHERE id='$id'";
        } else if(test_input($_POST["ident"]) === "nr"){
            $sql = "UPDATE applied_pilots SET nation_rank='$num' WHERE id='$id'";
        } else if(test_input($_POST["ident"]) === "an"){
            $sql = "UPDATE applied_pilots SET aloc_number='$num' WHERE id='$id'";
        }
        $query = mysqli_query($db_con, $sql);
        if (mysqli_query($db_con, $sql)) {
            echo "Database updated successfully!";
        } else {
            echo "Error updating record: " . mysqli_error($db_con);
        }
    }
    
    /*
    $sql = "INSERT INTO applied_pilots 
                (Nac_order_number, LR_criteria, Status, Team)
                VALUES 
                    (1, 5, 8, 9),
                    (2, 10, 8, 3),
                    (3, 8, 3, 5),
                    (4, 10, 7, 6)
                ON DUPLICATE KEY UPDATE 
                    Nac_order_number = VALUES(Nac_order_number),
                    LR_criteria = VALUES(LR_criteria),
                    Status = VALUES(Status),
                    Team = VALUES(Team)";
    $query = mysqli_query($db_con, $sql);
    */
    