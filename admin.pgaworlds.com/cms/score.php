<?php
    // ACCESS Control
    include_once("../php_includes/check_login_status.php");
    if($user_ok == true) {
        echo "Welcome ".ucfirst($_SESSION['username']);
        echo '
        <form action="../logout.php">
            <button type="submit" value="submit">Logout</button>
        </form>
    ';
    } else {
        header("location: ../index.html");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alocaton</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="score.js"></script>
</head>
<body>
<hr>
<h2>Scoring table</h2>
<div class="wrapper">
    <div class="menu"> <!-- START MENU -->
        <div class="search_div">
            <input class="mdb-select search" type="text" size="30" placeholder="Search by pilot name" onkeyup="showPilots(this.value)">
        </div>
        <div class="counry_div">
            <select name="country" id="country" class="mdb-select md-form">
                <?php
                    include_once("../../public_html/php_includes/country_options.php");
                ?>
            </select>
        </div>
        <div class="buttons_div">
            <button class="btn btn-primary" type="button" onclick="list()">List selected</button>
            <button class="btn btn-secondary" type="button" onclick="window.location = 'score.php'">Refresh table</button>
            <button class="btn btn-danger" type="button" onclick="update_db()">Update team</button>
            <button class="btn btn-info" type="button" onclick="update_score_board()">Indv SCORE</button>
            <button type="button" value="round_1" onclick="team_score(this)">r1</button>
            <button type="button" value="round_2" onclick="team_score(this)">r2</button>
            <button type="button" value="round_3" onclick="team_score(this)">r3</button>
            <button type="button" value="round_4" onclick="team_score(this)">r4</button>
            <button type="button" value="round_5" onclick="team_score(this)">r5</button>
            <button type="button" value="round_6" onclick="team_score(this)">r6</button>
            <button type="button" value="round_7" onclick="team_score(this)">r7</button>
            <button type="button" value="round_8" onclick="team_score(this)">r8</button>
            <button type="button" value="round_9" onclick="team_score(this)">r9</button>
            <button type="button" value="round_10" onclick="team_score(this)">r10</button>
            <button type="button" value="round_11" onclick="team_score(this)">r11</button>
            <button type="button" value="round_12" onclick="team_score(this)">r12</button>
        </div>
    </div> <!-- END MENU -->
    <br><div id="message" class="text-center"></div><br>
    <div class="wrapp">
    <table id="table" class="table table-bordered table-striped table-responsive">
        <th>ID</th>
        <th>Pilot Name</th>
        <th>G</th>
        <th>Country</th>
		<th class="hideElement">Team pilot</th>
        <th id="column_5" class="thExpand">Comp Num<!--<span id="arrow" style="color: green; cursor: pointer;" onclick="hideElement()">&nbsp;&#9654;</span>--></th>
        <th id="column_6" class="thExpand">round_1</th> <!-- Setting from num 6 as it is this cell index number in a row -->
        <th id="column_7" class="thExpand">round_2</th>
        <th id="column_8" class="thExpand">round_3</th>
        <th id="column_9" class="thExpand">round_4</th>
        <th id="column_10" class="thExpand">round_5</th>
        <th id="column_11" class="thExpand">round_6</th>
        <th id="column_12" class="thExpand">round_7</th>
        <th id="column_13" class="thExpand">round_8</th>
        <th id="column_14" class="thExpand">round_9</th>
        <th id="column_15" class="thExpand">round_10</th>
        <th id="column_16" class="thExpand">round_11</th>
        <th id="column_17" class="thExpand">round_12</th>
        <th id="column_18" class="thExpand">Total</th>
    <?php
        include_once("../../public_html/php_includes/db_con.php");
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// START PAGINATION
        $rpp = 10;
        if(isset($_GET['page']) && !empty($_GET['page'])) {
            $currentPage = $_GET['page'];
        } else {
            $currentPage = 1;
        }
        $startFrom = ($currentPage * $rpp) - $rpp;
        $total = "SELECT * FROM applied_pilots WHERE order_number"; ///////////////// DODAO WHERE order_number
        $result = mysqli_query($db_con, $total);
        $rows = mysqli_num_rows($result);
        $lastPage = ceil($rows/$rpp);
        $firstPage = 1;
        $nextPage = $currentPage + 1;
        $previousPage = $currentPage - 1;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// END PAGINATION
        
        $country = $_POST['country'];
        $sql = "SELECT * FROM applied_pilots ORDER BY -order_number DESC LIMIT $startFrom, $rpp";
    	$result = $db_con->query($sql);
    	$i = 0;
        while($row = $result->fetch_assoc()) {
            if ($result->num_rows > 0 && $row['activated'] != 0 && !empty($row['order_number'])) { ////////////////////////// DODAO && !empty($row['order_number'])
                if($row['gender'] == "male") {
                    $gender = "M";
                } else $gender = "F";
                $i++;
                
                $row['order_number'] == 0 ? $on = "" : $on = $row['order_number'];
                //$row['round1'] == 0 ? $round1 = "" : $round1 = $row['round1'];
				echo '
					<tr>
						<td id="n'.$i.'">'.$row['id'].'</td>
						<td>' . $row['f_name'] . ' ' . $row['l_name'] . '</td>
						<td>' . $gender . '</td>
						<td style="width: 1%; white-space: nowrap;">' . $row['country'] . '</td>
						<td>
							<select name="team" id="team">
								<option value="null"'; if($row['Team'] == null) echo "selected"; echo'>Not in team</option>
								<option value="team"'; if($row['Team'] == "Team pilot") echo "selected"; echo'>Team pilot</option>
							</select>
						</td>
				        <!-- Order number -->		
						<td class="dbChange"><input type="number" class="inp" value="' . $on . '" onkeyup="on(this)"></input></td>
		
                        <!-- Rounds -->		
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_1'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_2'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_3'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_4'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_5'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_6'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_7'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_8'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_9'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_10'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_11'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_12'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['total'] . '" onkeyup="round(this)"></td>
					</tr>
				';
            }
        }
    ?>
		</table>
		
		<!-- //////////////////////////////////////////////////////////////////////////////////////////////// START PAGINATION NAVIGATION -->
		<nav aria-label="Page navigation" class="nav_all" id="nav_all">
		<ul class="pagination">
		<?php if($currentPage != $firstPage) { ?>
		<li class="page-item">
		<a class="page-link" href="?page=<?php echo $firstPage ?>" tabindex="-1" aria-label="Previous">
		<span aria-hidden="true">First</span>
		</a>
		</li>
		<?php } ?>
		<?php if($currentPage >= 2) { ?>
		<li class="page-item"><a class="page-link" href="?page=<?php echo $previousPage ?>"><?php echo $previousPage ?></a></li>
		<?php } ?>
		<li class="page-item active"><a class="page-link" href="?page=<?php echo $currentPage ?>"><?php echo $currentPage ?></a></li>
		<?php if($currentPage != $lastPage) { ?>
		<li class="page-item"><a class="page-link" href="?page=<?php echo $nextPage ?>"><?php echo $nextPage ?></a></li>
		<li class="page-item">
		<a class="page-link" href="?page=<?php echo $lastPage ?>" aria-label="Next">
		<span aria-hidden="true">Last</span>
		</a>
		</li>
		<?php } ?>
		</ul>
		</nav>
		<!-- ////////////////////////////////////////////////////////////////////////////////////////////////// END PAGINATION NAVIGATION -->
    </div> <!-- END WRAPP -->
</div> <!-- END WRAPPER -->
</body>
</html>