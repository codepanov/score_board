<table id="table" class="table table-bordered table-striped table-responsive">
        <th>ID</th>
        <th>Pilot Name</th>
        <th>G</th>
        <th>Country</th>
		<th class="hideElement">Team pilot</th>
        <th id="column_5" class="thExpand">Comp Num<!--<span id="arrow" style="color: green; cursor: pointer;" onclick="hideElement()">&nbsp;&#9654;</span>--></th>
        <th id="column_6" class="thExpand">round_1</th> <!-- Setting from num 6 as it is this cell index number in a row -->
        <th id="column_7" class="thExpand">round_2</th>
        <th id="column_8" class="thExpand">round_3</th>
        <th id="column_9" class="thExpand">round_4</th>
        <th id="column_10" class="thExpand">round_5</th>
        <th id="column_11" class="thExpand">round_6</th>
        <th id="column_12" class="thExpand">round_7</th>
        <th id="column_13" class="thExpand">round_8</th>
        <th id="column_14" class="thExpand">round_9</th>
        <th id="column_15" class="thExpand">round_10</th>
        <th id="column_16" class="thExpand">round_11</th>
        <th id="column_17" class="thExpand">round_12</th>
        <th id="column_18" class="thExpand">Total</th>
    <?php
        $q=$_GET["q"];
        $w=$_GET["w"];
        
        include_once("../../public_html/php_includes/db_con.php");
        
        if(!$w){
            $sql = "SELECT * FROM applied_pilots WHERE (`f_name` LIKE '%".$q."%') OR (`l_name` LIKE '%".$q."%') ORDER BY -order_number DESC";
        } else {
            $sql = "SELECT * FROM applied_tls WHERE (`f_name` LIKE '%".$w."%') OR (`l_name` LIKE '%".$w."%') ORDER BY -order_number DESC";
        }
    	$result = $db_con->query($sql);
    	$i = 0;
        while($row = $result->fetch_assoc()) {
            if ($result->num_rows > 0 && $row['activated'] != 0) {
                if($row['gender'] == "male") {
                    $gender = "M";
                } else $gender = "F";
                $i++;
                
                $row['order_number'] == 0 ? $on = "" : $on = $row['order_number'];
                //$row['round1'] == 0 ? $round1 = "" : $round1 = $row['round1'];
				echo '
					<tr>
						<td id="n'.$i.'">'.$row['id'].'</td>
						<td>' . $row['f_name'] . ' ' . $row['l_name'] . '</td>
						<td>' . $gender . '</td>
						<td style="width: 1%; white-space: nowrap;">' . $row['country'] . '</td>
						<td>
							<select name="team" id="team">
								<option value="null"'; if($row['Team'] == null) echo "selected"; echo'>Not in team</option>
								<option value="team"'; if($row['Team'] == "Team pilot") echo "selected"; echo'>Team pilot</option>
							</select>
						</td>
						<td class="dbChange"><input type="number" class="inp" value="' . $on . '" onkeyup="on(this)"></input></td>
		
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_1'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_2'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_3'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_4'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_5'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_6'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_7'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_8'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_9'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_10'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_11'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['round_12'] . '" onkeyup="round(this)"></td>
						<td class="dbChange"><input type="number" class="inp" value="' . $row['total'] . '" onkeyup="round(this)"></td>
					</tr>
				';
            }
        }
    ?>
		</table>