<?php
session_start();
// If user is logged in, header them away
if(isset($_SESSION["username"])){
	header("location: message.php?msg=You must logout first!");
    exit();
}

// Ajax calls this for NAME CHECK
if(isset($_POST["usernamecheck"])){
	include_once("php_includes/db_con.php");
	$username = preg_replace('#[^a-z0-9]#i', '', $_POST['usernamecheck']);
	$sql = "SELECT id FROM admins WHERE username='$username' LIMIT 1";
    $query = mysqli_query($db_con, $sql); 
    $uname_check = mysqli_num_rows($query);
    if (strlen($username) < 3 || strlen($username) > 20) {
	    echo '<strong style="color:#F00;">3 - 20 characters please</strong>';
	    exit();
    }
	if (is_numeric($username[0])) {
	    echo '<strong style="color:#F00;">Usernames must begin with a letter</strong>';
	    exit();
    }
    if ($uname_check < 1) {
	    echo '<strong style="color:#009900;">' . $username . ' username is OK to register</strong>';
	    exit();
    } else {
	    echo '<strong style="color:#F00;">' . $username . ' is taken</strong>';
	    exit();
    }
}

// Ajax calls this for REGISTRATION
if(isset($_POST["u"])){
	// CONNECT TO THE DATABASE
	include_once("php_includes/db_con.php");
	// GATHER THE POSTED DATA INTO LOCAL VARIABLES
	$u = preg_replace('#[^a-z0-9]#i', '', $_POST['u']);
    $p = $_POST['p'];
    // GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
	// DUPLICATE DATA CHECK FOR USERNAME
	$sql = "SELECT id FROM admins WHERE username='$u' LIMIT 1";
    $query = mysqli_query($db_con, $sql); 
	$u_check = mysqli_num_rows($query);
	// FORM DATA ERROR HANDLING
	if($u == "" || $p == "") {
		echo "The form submission is missing values.";
        exit();
	} else if ($u_check > 0) { 
        echo "The username you entered is alreay taken";
        exit();
	} else if (strlen($u) < 3 || strlen($u) > 20) {
        echo "Username must be between 3 and 20 characters";
        exit(); 
    } else if (is_numeric($u[0])) {
        echo 'Username cannot begin with a number';
        exit();
    } else { // END FORM DATA ERROR HANDLING
	    // Begin Insertion of data into the database
        // Hash the password and apply your own mysterious unique salt
        /* Prvo resenje za pass hash
		@$cryptpass = crypt($p);
		include_once ("php_includes/randStrGen.php");
        $p_hash = randStrGen(20)."$cryptpass".randStrGen(20);
        */
        $p_hash = md5($p);
        //$p_hash = password_hash($p, PASSWORD_DEFAULT); Ovako bih ja...
		// Add user info into the database table for the main site table
		$sql = "INSERT INTO admins (username, password, signup, lastlogin, ip)       
		        VALUES('$u', '$p_hash', now(), now(), '$ip')";
		$query = mysqli_query($db_con, $sql); 
		$uid = mysqli_insert_id($db_con);
		// Email me an activation link
		$to = "codepanov@gmail.com";							 
		$from = "accounts@pgaworlds.com";
		$subject = 'Admin Account Activation';
        $message = $u . ' is applied for admin account.<br>Click the link below to activate his account when ready:<br>
        <a href="https://admin.pgaworlds.com/activation.php?id=' . $uid . '&u=' . $u . '">Click here to activate users account now</a>';
		$headers = "From: $from\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
		@mail($to, $subject, $message, $headers);
		echo "signup_success";
		exit();
	}
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="js/ajax.js"></script>
    <script src="js/redirect.js"></script>
    <script src="js/main.js"></script>
    <title>Document</title>
</head>
<body>
    <form name="signupform" id="signupform" onsubmit="return false;">
        <div class="container fit">
            <h1>Register</h1>
            <p>Please fill in this form to create an admin account.</p>
            <hr>
        
            <label for="username"><b>Username</b></label>
            <input placeholder="Enter Username" id="username" type="text" onblur="checkusername()" onkeyup="restrict('username')" maxlength="20" required>
            <span id="unamestatus" style="display: block;"></span>
        
            <label for="psw"><b>Password</b></label>
            <input placeholder="Enter Password" id="pass1" type="password" onfocus="emptyElement('status')" maxlength="100" required>
        
            <label for="psw-repeat"><b>Repeat Password</b></label>
            <input placeholder="Repeat Password" id="pass2" type="password" onfocus="emptyElement('status')" maxlength="100" required>
            <hr>
            <button type="button" class="registerbtn"  id="signupbtn" onclick="signup()">Register</button>
            <button type="button" class="registerbtn" onclick="home();">Cancel</button>
            <span id="status" style="display: block;"></span>
        </div>
    </form>
</body>
</html>