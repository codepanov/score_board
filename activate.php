<?php
if (isset($_GET['id']) && isset($_GET['e'])) {
	// Connect to database and sanitize incoming $_GET variables
    include_once("php_includes/db_con.php");
    $id = preg_replace('#[^0-9]#i', '', $_GET['id']); 
	$e = mysqli_real_escape_string($db_con, $_GET['e']);
	// Evaluate the lengths of the incoming $_GET variable
	if($id == "" || strlen($e) < 5){
		// Log this issue into a text file and email details to yourself
		header("location: message.php?msg=activation_string_length_issues");
    	exit();
	}
	// Check their credentials against the database
	$sql = "SELECT * FROM applied_pilots WHERE id='$id' AND email='$e' AND activated='0' LIMIT 1";
    $query = mysqli_query($db_con, $sql);
	$numrows = mysqli_num_rows($query);
	// Evaluate for a match in the system (0 = no match, 1 = match)
	if($numrows == 0){
		$sql = "SELECT * FROM applied_tls WHERE id='$id' AND email='$e' AND activated='0' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
		$numrows = mysqli_num_rows($query);
		// Log this potential hack attempt to text file and email details to yourself
		if($numrows == 0) {
			header("location: message.php?msg=Your credentials are not matching anything in our system");
			exit();
		}
		// Match was found in TEAM LEADERS, you can activate them
		$sql = "UPDATE applied_tls SET activated='1' WHERE id='$id' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
		// Double check to see if activated in fact now = 1
		$sql = "SELECT * FROM applied_tls WHERE id='$id' AND activated='1' LIMIT 1";
		$query = mysqli_query($db_con, $sql);
		$numrows = mysqli_num_rows($query);
		// Evaluate the double check
		if($numrows == 0){
			// Log this issue of no switch of activation field to 1
			header("location: message.php?msg=activation_failure");
			exit();
		} else if($numrows == 1) {
			// Great everything went fine with activation!
			// Send email to activated person
			$to = $e;							 
			$from = "accounts@pgawordls.com";
			$subject = "Request for PGAWC 2019, approved!";
			$message = "Your request for PGAWC 2019 was approved.";
			$headers = "From: $from\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\n";
			mail($to, $subject, $message, $headers);
			header("location: message.php?msg=activation_success");
			exit();
		}
	}
	// Match was found in PILOTS, you can activate them
	$sql = "UPDATE applied_pilots SET activated='1' WHERE id='$id' LIMIT 1";
    $query = mysqli_query($db_con, $sql);
	// Double check to see if activated in fact now = 1
	$sql = "SELECT * FROM applied_pilots WHERE id='$id' AND activated='1' LIMIT 1";
    $query = mysqli_query($db_con, $sql);
	$numrows = mysqli_num_rows($query);
	// Evaluate the double check
    if($numrows == 0){
		// Log this issue of no switch of activation field to 1
        header("location: message.php?msg=activation_failure");
    	exit();
    } else if($numrows == 1) {
		// Great everything went fine with activation!
		// Send email to activated person
		$to = $e;							 
		$from = "accounts@pgawordls.com";
		$subject = "Request for PGAWC 2019, approved!";
		$message = "Your request for PGAWC 2019 was approved.";
		$headers = "From: $from\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\n";
		mail($to, $subject, $message, $headers);
        header("location: message.php?msg=activation_success");
    	exit();
    }
} else {
	// Log this issue of missing initial $_GET variables
	header("location: message.php?msg=missing_GET_variables");
    exit(); 
}
