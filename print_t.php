<style>
    .image {
        width: 30%;
        margin: 0 auto;
    }
</style>
<div class="image"><img src="uploads/pgaw_logo.jpg" alt="pgaw_logo" width="100%"></div>
<link href="https://bootswatch.com/4/flatly/bootstrap.min.css" rel="stylesheet">
<h3>Team results</h3>
<br>	
<?php
	$db_con = mysqli_connect("localhost", "pgaworld_root", "Te0d0raD0ra", "pgaworld_acc");
    if(mysqli_connect_errno()) {
        echo mysqli_connect_error();
        exit();
    }
    $sql = "SELECT DISTINCT country, GROUP_CONCAT(CAST(result AS SIGNED)), SUM(CAST(result AS SIGNED)) FROM team_results_per_series GROUP BY country ORDER by -SUM(CAST(result AS SIGNED)) DESC";
	$result = $db_con->query($sql);
	if ($result->num_rows > 0) {
		echo "
		<table class='table table-hover'>
			<tr class='table-active'>
				<th>Position</th>
				<th>Country</th>
				<th>round 1</th>
				<th>round 2</th>
				<th>round 3</th>
				<th>round 4</th>
				<th>round 5</th>
				<th>round 6</th>
				<th>round 7</th>
				<th>round 8</th>
				<th>round 9</th>
				<th>round 10</th>
				<th>round 11</th>
				<th>round 12</th>
				<th>Score</th>
			</tr>
			";
			//$pos = 0;
			$score_array = array();
			// output data of each row
			while($row = $result->fetch_assoc()) {
				//$pos++;
				
				$cntcode = $row['country'];
			switch ($cntcode) {
				case 'United States':
					$flag = 'us';
					break;
				case 'Colombia':
					$flag = 'co';
					break;
				case 'Turkey':
					$flag = 'tr';
					break;
				case 'Canada':
					$flag = 'ca';
					break;
				case 'Serbia':
					$flag = 'rs';
					break;
				case 'Russian Federation':
					$flag = 'ru';
					break;
				case 'Iran':
					$flag = 'ir';
					break;
				case 'Mongolia':
					$flag = 'mn';
					break;
				case 'China':
					$flag = 'cn';
					break;
				case 'Slovenia':
					$flag = 'si';
					break;
				case 'Thailand':
					$flag = 'th';
					break;
				case 'Japan':
					$flag = 'jp';
					break;
				case 'France':
					$flag = 'fr';
					break;
				case 'Korea':
					$flag = 'kr';
					break;
				case 'Ecuador':
					$flag = 'ec';
					break;
				case 'Romania':
					$flag = 'ro';
					break;
				case 'Argentina':
					$flag = 'ar';
					break;
				case 'Poland':
					$flag = 'pl';
					break;
				case 'Nepal':
					$flag = 'np';
					break;
				case 'Hungary':
					$flag = 'hu';
					break;
				case 'Czech Republic':
					$flag = 'cz';
					break;
				case 'Kazakhstan':
					$flag = 'kz';
					break;
				case 'Netherlands':
					$flag = 'nl';
					break;
				case 'Latvia':
					$flag = 'lv';
					break;
				case 'Macedonia':
					$flag = 'mk';
					break;
				case 'Spain':
					$flag = 'es';
					break;
				case 'United Kingdom':
					$flag = 'gb';
					break;
				case 'Lithuania':
					$flag = 'lt';
					break;
				case 'Indonesia':
					$flag = 'id';
					break;
				case 'Albania':
					$flag = 'al';
					break;
				case 'Bulgaria':
					$flag = 'bg';
					break;
				case 'Germany':
					$flag = 'de';
					break;
				case 'Kosovo':
					$flag = 'xk';
					break;
				case 'Saudi Arabia':
					$flag = 'sa';
					break;
				default:
					$flag = '';
			}
			
			if($row['country'] == "Kosovo") {
				$country = "Kosovo*";
			} else {
				$country = $row['country'];
			}
			
			$results = explode("," ,$row['GROUP_CONCAT(CAST(result AS SIGNED))']);
			$round_1 = $results[0]==null ? $results[0] : str_pad($results[0], 4, 0, STR_PAD_LEFT);
			$round_2 = $results[1]==null ? $results[1] : str_pad($results[1], 4, 0, STR_PAD_LEFT);
			$round_3 = $results[2]==null ? $results[2] : str_pad($results[2], 4, 0, STR_PAD_LEFT);
			$round_4 = $results[3]==null ? $results[3] : str_pad($results[3], 4, 0, STR_PAD_LEFT);
			$round_5 = $results[4]==null ? $results[4] : str_pad($results[4], 4, 0, STR_PAD_LEFT);
			$round_6 = $results[5]==null ? $results[5] : str_pad($results[5], 4, 0, STR_PAD_LEFT);
			$round_7 = $results[6]==null ? $results[6] : str_pad($results[6], 4, 0, STR_PAD_LEFT);
			$round_8 = $results[7]==null ? $results[7] : str_pad($results[7], 4, 0, STR_PAD_LEFT);
			$round_9 = $results[8]==null ? $results[8] : str_pad($results[8], 4, 0, STR_PAD_LEFT);
			$round_10 = $results[9]==null ? $results[9] : str_pad($results[9], 4, 0, STR_PAD_LEFT);
			$round_11 = $results[10]==null ? $results[10] : str_pad($results[10], 4, 0, STR_PAD_LEFT);
			$round_12 = $results[11]==null ? $results[11] : str_pad($results[11], 4, 0, STR_PAD_LEFT);
			
			$score = str_pad($row['SUM(CAST(result AS SIGNED))'], 4, 0, STR_PAD_LEFT);
			array_push($score_array, $score);
			if(count($score_array) == 1) {
				$pos = count($score_array);
			} elseif(count($score_array) > 1) {
				if($score_array[count($score_array)-1] == $score_array[count($score_array)-2]) {
					$pos = array_search($score_array[count($score_array)-1], $score_array)+1;
				} else {
					$pos = count($score_array);
				}
			}
			echo "
			<tr>
				<td>" . $pos . "</td>
				<td>" . $country . "</td>
                <!--<td style='text-align: center;'><img class='flag' src='https://lipis.github.io/flag-icon-css/flags/1x1/{$flag}.svg' alt='Flag' style='width: 30px; border-radius: 15px;'></td>-->
				<td>" . $round_1 . "</td>
				<td>" . $round_2 . "</td>
				<td>" . $round_3 . "</td>
				<td>" . $round_4 . "</td>
				<td>" . $round_5 . "</td>
				<td>" . $round_6 . "</td>
				<td>" . $round_7 . "</td>
				<td>" . $round_8 . "</td>
				<td>" . $round_9 . "</td>
				<td>" . $round_10 . "</td>
				<td>" . $round_11 . "</td>
				<td>" . $round_12 . "</td>
				<td>" . $score . "</td>
			</tr>
			";
		}
		echo "
		</table>
		";
	} else {
		echo "0 results";
	}
	$db_con->close();
?>