-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 16, 2019 at 02:47 PM
-- Server version: 5.7.27-log-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pgaworld_acc`
--
CREATE DATABASE IF NOT EXISTS `pgaworld_acc` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pgaworld_acc`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `signup` datetime NOT NULL,
  `lastlogin` datetime NOT NULL,
  `activated` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `signup`, `lastlogin`, `activated`, `ip`) VALUES
(1, 'panov', '447ef4dd2f6357feaab83c13d8a93213', '2019-05-08 16:13:03', '2019-10-16 14:45:09', '1', '109.245.111.96'),
(3, 'Svetlana', '0dc1d41f9c83f314594aec7afa52893f', '2019-05-09 13:05:16', '2019-08-29 11:14:56', '1', '94.189.158.11'),
(4, 'GoranDjurkovic', '3554ca17f79e3dc208e05dd91e460727', '2019-06-11 13:25:02', '2019-06-11 21:24:31', '1', '188.2.217.200'),
(5, 'ovukazeljko', '26c4a5ede06dcc6d4e9167b7ae1b049b', '2019-06-15 10:43:43', '2019-09-08 23:02:01', '1', '82.117.211.154'),
(6, 'ninko', '3e1084428630d1c7fabbe7993ba69be7', '2019-09-07 16:13:29', '2019-09-26 22:45:28', '1', '93.87.132.29');

-- --------------------------------------------------------

--
-- Table structure for table `applied_pilots`
--

CREATE TABLE `applied_pilots` (
  `id` int(11) NOT NULL,
  `Nac_order_number` enum('NAC approval needed','1','2','3','4','5','6','7','8','9','10','11','12') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NAC approval needed',
  `LR_Criteria` enum('','Qualified','WPRS Missing criteria') COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` enum('Waiting list','Waiting payment','Confirmed','Canceled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Waiting list',
  `Team` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signedup` datetime NOT NULL,
  `activated` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pilot_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_leader` enum('ytl','ntl') COLLATE utf8_unicode_ci NOT NULL,
  `aloc_number` int(11) DEFAULT NULL,
  `nation_rank` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pg_man` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pg_mod` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pg_col` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faiid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `civlid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `natflid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insuco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insuno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL,
  `t_shirt` enum('xxs','xs','s','m','l','xl','xxl') COLLATE utf8_unicode_ci NOT NULL,
  `vegan` enum('yv','nv') COLLATE utf8_unicode_ci NOT NULL,
  `blood` enum('0m','0p','am','ap','bm','bp','abm','abp','dn') COLLATE utf8_unicode_ci NOT NULL,
  `order_number` int(11) DEFAULT NULL,
  `round_1` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_2` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_3` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_4` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_5` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_6` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_7` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_8` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_9` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_10` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_11` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `round_12` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `total` int(4) UNSIGNED ZEROFILL DEFAULT NULL,
  `total_team` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `applied_pilots`
--

INSERT INTO `applied_pilots` (`id`, `Nac_order_number`, `LR_Criteria`, `Status`, `Team`, `signedup`, `activated`, `pilot_image`, `f_name`, `l_name`, `email`, `team_leader`, `aloc_number`, `nation_rank`, `country`, `address`, `passport`, `cell`, `wapp`, `pg_man`, `pg_mod`, `pg_col`, `faiid`, `civlid`, `natflid`, `insuco`, `insuno`, `gender`, `date_birth`, `t_shirt`, `vegan`, `blood`, `order_number`, `round_1`, `round_2`, `round_3`, `round_4`, `round_5`, `round_6`, `round_7`, `round_8`, `round_9`, `round_10`, `round_11`, `round_12`, `total`, `total_team`) VALUES
(1, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-18 20:26:44', '1', 'pedro_tirado.jpg', 'Pedro', 'Tirado', 'pjtirado1@gmail.com', 'ytl', NULL, NULL, 'United States', '132 N Fillmore Avenue, Sterling, VA 20164, USA', '567396926', '17039733003', '573213269782', 'BGD', 'Adam', 'blue,purple,green,fucsia', 'H70702', '47900', 'USHPA70702', 'DogTagExtremebyTrawickInternational', '999508522', 'male', '1959-07-16', 'xl', 'nv', '0p', 21, 406, 500, 500, 500, 500, 392, 035, 010, 500, NULL, NULL, NULL, 2843, 2843),
(2, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-19 01:47:30', '1', 'felipe_arboleda.jpg', 'Felipe', 'Arboleda', 'felipe.arboleda85@hotmail.com', 'ntl', NULL, NULL, 'Colombia', 'Calle 59 AA 68 50  bello Antioquia Colombia ', 'AU178566', '0573122507346', '0573122507346', 'Sky Paragliders', 'Fides5', 'Verde blanco negro ', '126228', '23146', 'COL-875-PG', 'EPS', 'Novasalud', 'male', '1984-12-29', 'm', 'yv', '0p', 65, 001, 003, 012, 008, 132, 001, 008, 006, 104, NULL, NULL, NULL, 0143, 0143),
(3, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-19 10:08:26', '1', 'ali_zaimoglu.jpg', 'Ali', 'Zamoglu', 'alicakir2544@gmail.com', 'ytl', NULL, NULL, 'Turkey', '23, 5', '2936100', '05382936100', '', 'Sky', 'Aya', 'Reed white', '223012', '35648', '2166', '', '', 'male', '1983-09-05', 'm', 'nv', 'am', 122, 500, 500, 044, 008, 288, 000, 500, 500, 062, NULL, NULL, NULL, 1902, 1902),
(4, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-19 10:12:58', '1', 'selin_altun.jpg', 'Selin', 'Altun', 'Cerkez_selin@outlook.com', 'ntl', NULL, NULL, 'Turkey', '23, 5', '43996115266', '05349762207', '', 'Skywalk', 'Mescal4', 'Orange', '272382', '51637', '7198', '', '', 'female', '1995-02-01', 'xs', 'nv', 'bp', 23, 160, 500, 236, 396, 500, 043, 046, 250, 500, NULL, NULL, NULL, 2131, 2131),
(5, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-19 19:32:21', '1', 'daniel_vallejo.jpg', 'Daniel', 'Vallejo', 'Dgvallej@hotmail.com', 'ntl', NULL, NULL, 'Canada', '511 Rosemead ave Kelowna, BC, V1Y5Z9 ', 'AO582080', '12898384463', '12898384463', 'BGD', 'Adam', 'Green', '258779', '50360', 'HPAC6130', 'TUGO', 'FGM4926587', 'male', '1992-06-17', 's', 'nv', '0p', 8, 154, 016, 003, 009, 356, 500, 020, 016, 255, NULL, NULL, NULL, 0829, 0829),
(6, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-19 19:37:28', '1', 'jimmy_giroux.jpg', 'Jimmy', 'Giroux', 'Jimgiroux@hotmail.com', 'ytl', NULL, NULL, 'Canada', 'Canada', 'HP841838', '14189572582', '14189572582', 'Ozone', 'Andy', 'Red white gray', '93790', '49228', '5380', 'Tugo', 'Fgm1146073', 'male', '1973-11-30', 'xl', 'nv', 'abp', 36, 051, 500, 500, 068, 156, 500, 004, 201, 420, NULL, NULL, NULL, 1900, 1900),
(7, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-20 07:52:07', '1', 'ivan_pavlov.jpg', 'Ivan', 'Pavlov', 'pavlovivan24@gmail.com', 'ntl', NULL, NULL, 'Serbia', 'Glavna 7 Kanjiza', '0', '381645564234', '381645564237', 'Icaro', 'Pica', 'Blue', '62200', '23865', 'O-308', 'Ddor', '123', 'male', '1995-08-24', 'm', 'nv', 'dn', 40, 185, 001, 005, 005, 045, 002, 005, 005, 003, NULL, NULL, NULL, 0071, 0071),
(8, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-04-21 20:17:00', '1', 'andrey_orlov.jpg', 'Andrey', 'Orlov', 'Qvest82@mail.ru', 'ntl', NULL, NULL, 'Russian Federation', ' UL SADOVAYA, D 38, KV 11,  S ZAVYALOVO, RESP RESPUBLIKA UDMURTIYA, 427000, RUSSIAN FEDERATION', '022353', '79120144980', '79120144980', 'DavinciGliders', 'RHYTHM', 'Red , white', '2781', '29709', '', '', '', 'male', '1982-10-28', 'm', 'nv', 'ap', 6, 012, 006, 218, 004, 199, 002, 001, 044, 001, NULL, NULL, NULL, 0269, 0269),
(9, 'NAC approval needed', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-04-21 22:35:19', '1', 'mohsen_mansouri.jpg', 'Mohsen', 'Mansouri', 'acronaline@gmail.com', 'ytl', NULL, NULL, 'Iran', 'tehran iran', 'Y30194850', '989124674494', '989398347266', 'Gradient', 'Bright5', 'Gold', '', '24136', '', '', '', 'male', '1984-08-28', 'm', 'nv', 'abp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'NAC approval needed', 'Qualified', 'Waiting list', NULL, '2019-04-22 00:34:15', '1', 'hossein_abbasgholizadeh.jpg', 'Hossein', 'Abbasgholizadeh', 'hossin.para@yahoo.com', 'ntl', NULL, NULL, 'Iran', 'gorgan,street emamreza', '41717956', '00989356389612', '00989356389612', 'niviuk', 'PG model', 'green', '132903', '271886', '', '', '', 'male', '1986-12-28', 'xs', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'NAC approval needed', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-04-22 03:23:41', '1', 'mohammadamin_hazini.jpg', 'MohammadAmin', 'Hazini', 'nerofly.ir@gmail.com', 'ntl', NULL, NULL, 'Iran', 'No99Fourth LalehShohada AvenueGorgan ', '45460263', '989112700130', '989112700130', '', '', '', '132904', '66855', '132904', '', '', 'male', '1988-05-20', 'l', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-05 13:23:58', '1', 'nathalia_pinzon.jpg', 'Nathalia', 'Pinzon', 'parapentecolombia@hotmail.com', 'ntl', NULL, NULL, 'Colombia', '', 'AS158745', '573204545467', '573204545467', 'BGD', 'ADAM', '', '', '27564', 'COL-066-PG', '', '', 'female', '1987-09-04', 'xs', 'nv', 'bp', 101, 103, 164, 006, 285, 004, 002, 075, 010, 001, NULL, NULL, NULL, 0365, 0365),
(13, '8', 'Qualified', 'Waiting list', '', '2019-05-05 14:27:57', '1', 'diego_lopez.jpg', 'Diego', 'Lopez', 'diegofly28@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Calle 64 94a11', 'AN421575', '573192044175', '573192044175', 'Advance', 'Alpha5', 'Blue ', '133244', '41770', '748PG', 'Sura', '3349747', 'male', '1985-05-28', 'm', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '1', 'Qualified', 'Canceled', NULL, '2019-05-05 18:19:50', '0', 'batdavaa_alzakhgui.jpg', 'Batdavaa', 'Alzakhgui', 'A91902001@gmail.com', 'ntl', NULL, NULL, 'Mongolia', 'Chingeltei street of ulaanbaatar', 'E-2374699', '97691902001', '97691902001', 'Bgd', 'Adam', 'Topaz', '115535', '60214', 'Mgl01-001', 'MIkdaatgal', '0003476453276', 'male', '1977-05-23', 'm', 'nv', 'am', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 02:06:22', '1', 'yong_wu.jpg', 'Yong', 'WU', '364888857@qq.com', 'ntl', NULL, NULL, 'China', '', 'PE0708833', '8613739404968', '', 'Ozone', 'Atom3', 'Blue Purple', '83156', '41734', '4227', '', '', 'male', '1980-12-22', 's', 'nv', '0p', 100, 002, 002, 001, 002, 001, 001, 001, 500, 003, NULL, NULL, NULL, 0013, 0013),
(16, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 02:09:32', '1', 'haiping_chen.jpg', 'Haiping', 'CHEN', 'chenhaiping75525@126.com', 'ntl', NULL, NULL, 'China', '', 'PE1431748', '8613871637102', '', 'SKY', 'AYA', '', '93600', '16416', '1372', '', '', 'male', '1975-05-25', 'xxl', 'nv', 'bp', 83, 056, 005, 006, 004, 001, 003, 012, 003, 002, NULL, NULL, NULL, 0036, 0036),
(17, '6', 'WPRS Missing criteria', 'Waiting list', '', '2019-05-06 02:12:34', '1', 'simin_li.jpg', 'Simin', 'LI', '415766076@qq.com', 'ntl', NULL, NULL, 'China', '', 'PE1431750', '8618628219404', '', 'BGD', 'Adam', 'OrangeRed', '115534', '58673', '4747', '', '', 'female', '1994-04-04', 's', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 02:15:50', '1', 'gang_xiong.jpg', 'Gang', 'XIONG', 'pilot_xg@aliyun.com', 'ntl', NULL, NULL, 'China', '', 'PE1431746', '8613320539923', '', 'BGD', 'Adam', 'Red', '93602', '46732', '884', '', '', 'male', '1969-09-08', 'm', 'nv', 'abp', 17, 002, 013, 002, 003, 007, 182, 004, 005, 006, NULL, NULL, NULL, 0042, 0042),
(19, '7', 'WPRS Missing criteria', 'Waiting list', '', '2019-05-06 02:21:49', '1', 'jing_guo.jpg', 'Jing', 'GUO', '1546103543@qq.com', 'ntl', NULL, NULL, 'China', '', 'inprocess', '8615029244141', '', '', '', '', '133081', '59278', '5927', '', '', 'female', '1993-04-30', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 03:51:09', '1', 'hongji_wang.jpg', 'Hongji', 'WANG', '996687812@qq.com', 'ntl', NULL, NULL, 'China', '', 'P01639914', '8613456799639', '', '', '', '', '83157', '38358', '3598', '', '', 'male', '1988-09-28', 'l', 'nv', '0p', 64, 005, 004, 053, 005, 500, 002, 004, 000, 001, NULL, NULL, NULL, 0074, 0074),
(21, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 08:34:05', '1', 'goran_djurkovic.jpg', 'Goran', 'Djurkovic', 'paragliding.vrsac@gmail.com', 'ntl', NULL, NULL, 'Serbia', '', '012725838', '0637120765', '', '', '', '', '', '8285', '', '', '', 'male', '1982-01-17', 'm', 'nv', 'abp', 13, 120, 500, 045, 001, 177, 160, 003, 031, 001, NULL, NULL, NULL, 0538, 0538),
(22, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 09:05:41', '1', 'dragan_popov.jpg', 'Dragan', 'Popov', 'gandra@live.com', 'ntl', NULL, NULL, 'Serbia', 'Vase Pelagica 19, 26300 Vrsac Serbia', '013694716', '381654341434', '381654341434', 'Gradient', 'Bright5', 'Yellow,red', 'O-208', '8438', '0109', '', '', 'male', '1983-06-07', 'xl', 'nv', 'bp', 112, 003, 008, 003, 006, 007, 005, 500, 142, 005, NULL, NULL, NULL, 0179, 0179),
(23, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 09:51:35', '1', 'dejan_valek.jpg', 'Dejan', 'Valek', 'dvalek83@gmail.com', 'ntl', NULL, NULL, 'Serbia', 'Beneova 1a, 21211 Kisa', '21211', '381642469786', '381642469786', 'Icaro', 'Pica', 'Blue', 'O-404', '22622', '', '', '', 'male', '1983-10-28', 'm', 'nv', '0p', 98, 001, 005, 001, 009, 004, 004, 005, 008, 001, NULL, NULL, NULL, 0029, 0029),
(24, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 09:56:45', '1', 'slobodan_maletic.jpg', 'Slobodan', 'Maletic', 'slobodan.maletic@paraglajding.org', 'ntl', NULL, NULL, 'Serbia', 'Bore Stankovica 25,11271 Surcin, Beograd', '008273630', '381600832466', '381600832466', 'Swing', 'ARCUSRSlite', 'White,blue', 'O-051', '20557', '0063/1437', '', '', 'male', '1967-10-17', 'm', 'nv', 'bp', 81, 002, 100, 014, 007, 002, 070, 005, 003, 055, NULL, NULL, NULL, 0158, 0158),
(25, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 15:49:09', '1', 'matjaz_feraric.jpg', 'Matjaz', 'Feraric', 'matjaz.feraric@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Preski vrh 22, 2390 Ravne na Koroskem', 'PB0677560', '38641423724', '38641423724', '', '', '', '110001', '8388', '0323/3647', 'TRIGLAV', 'PR42300754338', 'male', '1961-10-08', 'm', 'yv', 'abp', 105, 002, 003, 002, 001, 002, 004, 002, 224, 003, NULL, NULL, NULL, 0019, 0019),
(26, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-06 16:49:13', '1', 'daniel_monsalve.jpg', 'Daniel', 'Monsalve', 'danieolo.fly@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Kilometro8 via medellin San Pedro,051057,Bello', 'AU323418', '57146430398', '573146430398', 'GIN', 'Bolero4', 'Amarillo,verde ', '126229', '28499', 'COL-270-PG', 'Sura', '1040322534', 'male', '1990-07-04', 'l', 'nv', 'ap', 18, 023, 001, 008, 004, 002, 000, 005, 038, 008, NULL, NULL, NULL, 0051, 0051),
(27, '6', 'Qualified', 'Canceled', '', '2019-05-07 08:57:05', '1', 'anastasia_samokhina.jpg', 'Anastasia', 'Samokhina', 'paramaniaspb@gmail.com', 'ntl', NULL, NULL, 'Russian Federation', '14D 206, Pulkovskoe shosse, StPetersburg, Russia', '724708502', '79117221182', '79117221182', 'BGD', 'Epic', 'orange, yellow,red, white, blue, green', '4486', '54528', '', '', '', 'female', '1984-01-27', 'm', 'nv', 'am', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-07 09:23:52', '1', 'lei_ma.jpg', 'Lei', 'MA', '773729647@qq.com', 'ntl', NULL, NULL, 'China', '', 'PE1431747', '8618749341919', '', 'BGD', 'Adam', 'Red', '115533', '38362', '3229', '', '', 'male', '1992-12-08', 'l', 'nv', 'bp', 44, 073, 021, 027, 007, 002, 002, 005, 005, 002, NULL, NULL, NULL, 0071, 0071),
(29, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-08 12:02:49', '1', 'nunnapat_phuchong.jpg', 'Nunnapat', 'Phuchong', 'skyparty_bebie@hotmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA5599618', '66899669131', '66899669131', 'GIN', 'BOLERO6', 'YELLOW, BLUE, WHITE', '79823', '18548', 'THA0164', '', '', 'female', '1987-11-23', 'm', 'nv', 'bp', 61, 054, 002, 132, 004, 410, 429, 003, 392, 039, NULL, NULL, NULL, 1036, 1036),
(30, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-08 12:14:21', '1', 'chantika_chaisanuk.jpg', 'Chantika', 'Chaisanuk', 'rasat.thailand3@gmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA8162382', '66984379358', '66984379358', 'BGD', 'ADAM', 'BLUE, PURPLE, RED', '81154', '28366', 'THA0752', '', '', 'female', '1997-10-03', 'm', 'nv', 'bp', 97, 028, 036, 002, 009, 500, 066, 014, 005, 500, NULL, NULL, NULL, 0660, 0660),
(31, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-08 12:32:02', '1', 'jirasak_witeetham.jpg', 'Jirasak', 'Witeetham', 'jirasak4399@gmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA9585388', '66861447396', '66861447396', 'GIN', 'BOLERO6', 'RED, BLUE, WHITE', '81188', '24003', 'THA0575', '', '', 'male', '1984-06-15', 'l', 'nv', 'bp', 120, 001, 001, 001, 002, 004, 002, 007, 082, 002, NULL, NULL, NULL, 0020, 0020),
(32, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-09 18:14:33', '1', 'jorge_hernandez.jpg', 'Jorge', 'Hernandez', 'javi3418@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Cra 22 5130 Bogot ', 'AR071192', '573212000306', '3212000306', 'BGD', 'Adam', 'Topaz', '', '24023', 'Col231PG', 'DGFM', '8191520', 'male', '1970-07-20', 'm', 'nv', '0p', 113, 015, 014, 005, 007, 005, 091, 077, 005, 005, NULL, NULL, NULL, 0133, 0133),
(33, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-10 10:43:10', '1', 'mongkut_preecha.jpg', 'MONGKUT', 'PREECHA', 'mongkut.p@hotmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA6452592', '66910198972', '66910198972', 'GIN', 'BOLERO6', 'YELLOW, BLUE, WHITE', '81191', '18544', 'THA0333', '', '', 'male', '1973-11-17', 'l', 'nv', '0p', 12, 004, 500, 019, 008, 020, 002, 003, 500, 022, NULL, NULL, NULL, 0578, 0578),
(34, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-10 11:28:44', '1', 'tanapat_luangiam.jpg', 'TANAPAT', 'LUANGIAM', 'tnp.mic8@gmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA6982843', '66929988626', '66929988626', 'GIN', 'BOLERO6', 'RED, BLUE, WHITE', '79826', '28384', 'THA0765', '', '', 'male', '1997-04-08', 'm', 'nv', 'ap', 39, 011, 010, 001, 004, 003, 500, 003, 004, 475, NULL, NULL, NULL, 0511, 0511),
(35, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-10 12:00:05', '1', 'nithat_yangjui.jpg', 'NITHAT', 'YANGJUI', 'mark_17240@hotmail.com', 'ntl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AB1309612', '66879840399', '66879840399', 'GIN', 'BOLERO6', 'YELLOW, BLUE, WHITE', '81144', '31467', 'THA0801', '', '', 'male', '1998-02-17', 'l', 'nv', '0p', 111, 006, 001, 056, 500, 096, 500, 069, 000, 005, NULL, NULL, NULL, 0733, 0733),
(36, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-10 12:05:33', '1', 'sarayut_chinpongsatorn.jpg', 'SARAYUT', 'Chinpongsatorn', 'gingliderthailand@yahoo.com', 'ytl', NULL, NULL, 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'AA5039566', '66898116700', '66898116700', 'GIN', 'BOLERO6', 'BLUE, BLACK, WHITE', '92702', '18558', 'THA0158', '', '', 'male', '1967-03-03', 'l', 'nv', '0p', 80, 036, 500, 007, 003, 500, 002, 004, 215, 210, NULL, NULL, NULL, 0977, 0977),
(37, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-11 00:23:43', '1', 'cristian_cruz.jpg', 'Cristian', 'Cruz', 'Parapentebogotavuelos@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Calle 2 9f 81 tocancipa colombia', 'AN822573', '573156319692', '573156319692', 'OZONE', 'Buzzz4', 'Green', 'Col046pg', '22641', 'Col046pg', 'Colasistencia', '240292', 'male', '1992-02-24', 'm', 'nv', 'am', 121, 005, 004, 021, 022, 004, 001, 033, 500, 099, NULL, NULL, NULL, 0189, 0189),
(38, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-13 13:22:01', '1', 'yoshiki_oka.jpg', 'Yoshiki', 'OKA', 'international@falhawk.co.jp', 'ytl', NULL, NULL, 'Japan', '1 53 12 Goutokuji,Seyagayaku,Tokyo 1540021', 'TR6281236', '819046289021', '', 'OZONE', 'MOJO5', 'RED', '20913', '4619', 'XC1321', '', '', 'male', '1947-03-15', 'm', 'nv', '0p', 1, 009, 245, 454, 057, 123, 128, 003, 197, 051, NULL, NULL, NULL, 0813, 0813),
(39, '9', 'Qualified', 'Confirmed', '', '2019-05-13 18:18:30', '1', 'john_gomez.jpg', 'JOHN', 'Gomez', 'paranautico_01@yahoo.com', 'ntl', NULL, NULL, 'Colombia', 'Calle 12a 71 20', 'AM598020', '573142696682', '573142696682', 'BGD', 'Adam', 'Ruby', '274206', '52863', 'COL567-PG', '', '', 'male', '1985-05-02', 'm', 'nv', 'bp', 127, 034, 037, 487, 021, 061, 274, 066, 001, 061, NULL, NULL, NULL, 0555, 0555),
(40, 'NAC approval needed', 'Qualified', 'Waiting list', NULL, '2019-05-13 20:43:30', '1', 'mahmoud_shirazinia.jpg', 'Mahmoud', 'Shirazinia', 'parssky@hotmail.com', 'ntl', NULL, NULL, 'Iran', '', 'A48694683', '00989121139351', '00989121139351', 'Niviuk', 'Koyot3', 'Red', '', '21528', '', 'Razi', '19166', 'male', '1993-11-29', 'm', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-15 03:23:03', '1', 'makoto_kawamura.jpg', 'MAKOTO', 'KAWAMURA', 'jl7raw@gmail.com', 'ntl', NULL, NULL, 'Japan', '', 'TR6009194', '819027934056', '', 'AirDesign', 'Eazy2superlight', 'Blue,White', '181712', '10424', '', '', '', 'male', '1961-11-29', 'l', 'nv', '0p', 54, 010, 380, 476, 230, 500, 100, 005, 500, 500, NULL, NULL, NULL, 2201, 2201),
(42, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-17 11:03:12', '1', 'sergei_bazhanov.jpg', 'Sergei', 'Bazhanov', 'serega_life@bk.ru', 'ntl', NULL, NULL, 'Russian Federation', 'Kaliningrad, Belanova 29, 76 ,  ', '755856199', '79118630618', '79118630618', 'skywalkMescal4', 'Mescal4', 'bronze', '3387', '37894', '3387', '', '', 'male', '1987-03-17', 'm', 'nv', 'bp', 78, 204, 003, 005, 003, 315, 092, 011, 105, 004, NULL, NULL, NULL, 0427, 0427),
(43, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-17 17:07:42', '1', 'camilo_gomez.jpg', 'Camilo', 'Gomez', 'milosomosfly@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'San Flix kilometro8, Bello, 051057', 'AO963697', '573105164740', '573105164740', 'Skyparagliders', 'Fides4', 'Verde, negro ', '126227', '26675', 'COL-329-PG', 'Nuevaeps', '1040324476', 'male', '1993-12-20', 'l', 'nv', 'am', 45, 500, 183, 008, 003, 035, 008, 003, 007, 005, NULL, NULL, NULL, 0252, 0252),
(44, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-18 11:04:56', '1', 'tamara_kostic.jpg', 'Tamara', 'Kostic', 'tamara.kostic89@gmail.com', 'ntl', NULL, NULL, 'Serbia', 'Kopaonicka 26', '12345678', '381606669173', '', '', '', '', '', '24430', '', '', '', 'female', '1989-06-20', 's', 'nv', 'bp', 62, 003, 005, 015, 124, 234, 384, 178, 448, 018, NULL, NULL, NULL, 0961, 0961),
(45, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-18 19:39:52', '1', 'konstantin_pavlushko.jpg', 'Konstantin', 'Pavlushko', 'pavl.k@mail.ru', 'ntl', NULL, NULL, 'Russian Federation', '', '754818350', '79211081464', '79118508207', 'SEA', 'AirFlow', 'Blue', '01284', '12268', '', '', '', 'male', '1982-10-19', 'm', 'nv', 'bp', 59, 154, 500, 005, 001, 008, 005, 008, 003, 001, NULL, NULL, NULL, 0185, 0185),
(46, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-19 10:31:42', '1', 'alexandr_shalnov.jpg', 'Alexandr', 'Shalnov', 'ShalnovAE@gmail.com', 'ntl', NULL, NULL, 'Russian Federation', 'Entuziastov str b27 rv70, Cheboksary, Russia', '719221649', '79674702070', '79674702070', 'Gin', 'Bolero6', 'Tiger', '', '20358', '2486', 'later', 'later', 'male', '1974-07-14', 'xxl', 'nv', 'ap', 34, 002, 016, 002, 004, 007, 008, 001, 009, 054, NULL, NULL, NULL, 0049, 0049),
(47, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 00:00:52', '1', 'pascal_piazzalunga.jpg', 'PASCAL', 'PIAZZALUNGA', 'fbi74@orange.fr', 'ntl', NULL, NULL, 'France', '925 route de chaparon 74201 LATHUILE', '14AL39410', '0033609765040', '', 'SKYWALK', 'MESCAL', '', '108489', '56130', '0018765W', 'FFVLAXACORPORATESOLUTION', 'XFR0087990AV18AA', 'male', '1963-04-10', 'm', 'nv', '0p', 75, 007, 011, 009, 004, 141, 163, 039, 017, 001, NULL, NULL, NULL, 0229, 0229),
(48, 'NAC approval needed', '', 'Waiting list', '', '2019-05-20 05:18:00', '1', 'insuk_kang.jpg', 'INSUK', 'KANG', 'acciya@hanmai.net', 'ntl', NULL, NULL, 'Korea', '122, Hwalgongjanggil, Mungyeongeup, Mungyeongsi', 'M13832550', '821024795700', '821024795700', 'BGD', 'ADAM', 'Green', '81825', '4795', 'KOR-O-0007', 'AXA', '', 'female', '1971-03-20', 'xs', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 09:03:55', '1', 'sperber_samuel.jpg', 'SPERBER', 'SAMUEL', 'A2Lparapente@gmail.com', 'ntl', NULL, NULL, 'France', '21 allee des Champs, section Morel', '17IA28880', '0789800980', '33789800980', 'Advance', 'Alpha6', 'vert rouge blanc', '105024', '56129', '0016616K', 'AXA', 'XFR0087990AV18A', 'male', '1965-02-12', 'l', 'nv', '0p', 3, 004, 014, 003, 016, 005, 500, 012, 007, 500, NULL, NULL, NULL, 0561, 0561),
(50, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 10:47:43', '1', 'annesophie_lemesle.jpg', 'AnneSophie', 'Lemesle', 'as.lemesle@gmail.com', 'ntl', NULL, NULL, 'France', '4375 route d Annecy, 74210 Doussard, France', '14DP73873', '33649985934', '36303137646', 'BGD', 'Adam', 'Blue pink orange', '127960', '46684', '1604357R', 'Axacorporatesolutions', 'XFR0087990AV18AA', 'female', '1984-12-05', 'xs', 'nv', '0m', 56, 103, 272, 099, 500, 005, 172, 009, 058, 495, NULL, NULL, NULL, 1213, 1213),
(51, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 15:08:27', '1', 'jihun_yoo.jpg', 'jihun', 'YOO', 'azsx148@daum.net', 'ntl', NULL, NULL, 'Korea', '9, Wolgotjungangro 14beongil, Siheungsi, Gyeonggido, Republic of Korea', 'M45929542', '821093102724', '821093102724', 'Gradient', 'Bright5', 'red blue', '176656', '46109', 'KOR4859', 'AXA', '20AU60101-18449', 'male', '2001-03-12', 's', 'nv', '0p', 5, 003, 004, 500, 001, 500, 500, 002, 500, 003, NULL, NULL, NULL, 1513, 1513),
(52, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 17:07:22', '1', 'thierriaz_johann.jpg', 'thierriaz', 'johann', 'contact@aerofiz.com', 'ntl', NULL, NULL, 'France', '339 rue des granges 74190 passy', '14DC94754', '0033674027987', '', 'supair', 'leaf', 'red blue white', '119074', '1760', '051075TJ', 'AXACORPORATESOLUTIONS', '0017017W', 'male', '1975-10-05', 'l', 'nv', '0p', 93, 008, 000, 020, 004, 003, 013, 009, 135, 017, NULL, NULL, NULL, 0074, 0074),
(53, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-20 21:36:52', '1', 'devos_catherine.jpg', 'Devos', 'Catherine', 'kdevos@hommell.com', 'ntl', NULL, NULL, 'France', '51 rue Molitor, 75016 Paris', '14DH12749', '33603031493', '33603031493', 'Advance', 'PI2', 'Blanc Orange Bleu', '130841', '65915', '141263DC', 'SaamVerspirenGroup', 'XFR0087990AV18AA', 'female', '1963-12-14', 'l', 'nv', 'ap', 31, 094, 005, 001, 017, 263, 013, 080, 332, 003, NULL, NULL, NULL, 0476, 0476),
(54, 'NAC approval needed', '', 'Waiting list', '', '2019-05-21 02:46:07', '1', 'mitsuharu_koga.jpg', 'Mitsuharu', 'KOGA', 'koga@ks-sky.com', 'ntl', NULL, NULL, 'Japan', '2642 2 KamikawadaMachi NumataShi Gunma Japan, 3780026', 'TS0290078', '819049284449', '', '777', 'Dlihgt', 'White, green, red', 'O-0416', '9919', '68361', '', '', 'male', '1963-04-09', 'l', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '4', 'Qualified', 'Waiting list', '', '2019-05-21 05:48:50', '1', 'changmin_lee.jpg', 'CHANGMIN', 'LEE', 'para-no1@naver.com', 'ntl', NULL, NULL, 'Korea', '108, Jungni 1gil, Yeongwoleup, Yeongwolgun, Gangwondo, Republic of Korea', 'M04397946', '821043571800', '821043571800', 'GINGLIDERS', 'YETI4', 'WIHTE', '205506', '53565', 'KOR6122', 'AXA', '20AU60101-20029', 'male', '1984-08-16', 'xl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-21 06:15:36', '1', 'seongmin_lee.jpg', 'SEONGMIN', 'LEE', 'Ywpara5627@hanmail.net', 'ntl', NULL, NULL, 'Korea', 'Gangwondo Yeongwolgun YeongwolHyanggyogil 4914', 'M47930232', '821046575627', '821046575627', 'GINGLIDERS', 'Bolero6', 'YELLOW', '205504', '55339', 'KOR4831', 'AXA', '20AU60101-20028', 'male', '1986-04-16', 'l', 'nv', 'dn', 58, 002, 006, 001, 006, 016, 000, 006, 500, 001, NULL, NULL, NULL, 0038, 0038),
(57, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-21 14:53:18', '1', 'kihyeon_kim.jpg', 'kihyeon', 'kim', 'hawkkkh@naver.com', 'ntl', NULL, NULL, 'Korea', '1018, Mangwolgil 225beongil, Hajeommyeon, Ganghwagun, Incheon, Korea ', 'M21056331', '821062093155', '', 'BGD', 'ADAM', 'red', '', '1960', '', 'AXA', '', 'male', '1965-08-07', 'l', 'nv', '0p', 77, 015, 009, 006, 006, 182, 020, 003, 130, 070, NULL, NULL, NULL, 0259, 0259),
(58, '7', 'Qualified', 'Waiting list', '', '2019-05-21 23:55:43', '1', 'raphael_denninger.jpg', 'Raphael', 'Denninger', 'raphael.denninger@outlook.fr', 'ntl', NULL, NULL, 'France', '8 rue vautrin 88000 EPINAL', '19CA95870', '0673308830', '', 'BGD', 'Adam', 'Blue, Green, Purple', '127961', '63259', '1320505F', 'AXACORPORATESOLUTIONS', 'XFR0087990AV18AA', 'male', '2001-09-23', 's', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '1', 'Qualified', 'Canceled', NULL, '2019-05-22 03:40:30', '1', 'santiago_perez.jpg', 'SANTIAGO', 'PEREZ', 'sandavid2309@hotmail.com', 'ntl', NULL, NULL, 'Ecuador', 'N52 San Jose E14127  Quito  Ecuador codigo postal 170514', '1716793128', '593995650638', '593995650638', 'Niviuk', 'Koyot', 'Verde blanco', '17345', '12625', '1040', 'Colonial', '2309', 'male', '1992-09-23', 'm', 'nv', 'am', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '9', 'Qualified', 'Confirmed', '', '2019-05-22 18:30:12', '1', 'renato_herega.jpg', 'Renato', 'Herega', 'rxslo2@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Gorsinica 81, 2272 Gorisnica', 'PB12796588SVN', '38640166070', '', 'TripleSeven', 'DECK', 'green, purple', '64053', '49240', '01546/11133', '', '', 'male', '1985-06-19', 'm', 'nv', '0p', 128, 006, 155, 001, 003, 004, 007, 004, 003, 005, NULL, NULL, NULL, 0033, 0033),
(61, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-23 08:54:27', '1', 'maja_preloznik.jpg', 'Maja', 'Preloznik', 'maja.navodnik@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Salek 62g, 3320 Velenje', 'PB0818567', '38631861542', '', 'BGD', 'Adam', 'green, yelow', '110021', '45097', '0502-3865', 'Generali', '', 'female', '1970-08-29', 's', 'nv', 'dn', 70, 143, 088, 090, 151, 500, 500, 039, 070, 244, NULL, NULL, NULL, 1325, 1325),
(62, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-23 09:00:59', '1', 'rok_preloznik.jpg', 'Rok', 'Preloznik', 'rok.preloznik@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Salek 62g, 3320 Velenje', 'PB0723046', '38631621340', '', 'BGD', 'Adam', 'green, yelow', '260465', '8457', '0500-3863', 'Generali', '', 'male', '1965-04-26', 'l', 'nv', 'dn', 89, 002, 018, 087, 008, 008, 273, 007, 030, 006, NULL, NULL, NULL, 0166, 0166),
(63, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-24 10:35:22', '1', 'marius_stoian.jpg', 'Marius', 'Stoian', 'marius2stoian@yahoo.com', 'ntl', NULL, NULL, 'Romania', '', '057165430', '40743095304', '40743095304', 'Skywalk', 'Mescal5', 'Green, Blue, White', '121', '50687', '1330', 'AXAVersicherungAG', '20AU60101-19284', 'male', '1989-05-05', 'm', 'nv', 'dn', 2, 011, 025, 008, 500, 500, 005, 012, 219, 037, NULL, NULL, NULL, 0817, 0817),
(64, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-24 13:38:32', '1', 'mihaidan_stefu.jpg', 'MihaiDan', 'Stefu', 'stefu.mihai@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'Unirii 11, 555200, Avrig', '055686381', '40753789419', '40753789419', 'Skywalk', 'Mescal4', 'Purple, orange, white', '', '34176', '52', 'AXA', '20AU60101-19292', 'male', '1988-10-28', 'm', 'nv', 'dn', 30, 124, 500, 001, 001, 159, 120, 008, 009, 003, NULL, NULL, NULL, 0425, 0425),
(65, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-26 11:43:38', '1', 'sergei_suhoruchenko.jpg', 'Sergei', 'Suhoruchenko', 'su82@rambler.ru', 'ntl', NULL, NULL, 'Russian Federation', '', '713414070', '79114902454', '79114902454', 'Team5', 'Greenb', '', '0048', '18714', '', '', '', 'male', '1982-11-29', 's', 'nv', 'ap', 96, 006, 004, 002, 004, 007, 000, 003, 000, 007, NULL, NULL, NULL, 0026, 0026),
(66, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-26 16:28:21', '1', 'georgiana_birgoz.jpg', 'Georgiana', 'Birgoz', 'georgianabir@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'str Atelierului, bl6, sc B, ap 6, city Simeria, region Hunedoara', '057035115', '40727569596', '', 'BGD', 'Adam', 'Iceland ', '', '55765', '153', 'AXA', '20AU60101-19904', 'female', '1990-05-31', 'xs', 'nv', '0p', 55, 003, 001, 065, 013, 095, 005, 005, 065, 003, NULL, NULL, NULL, 0160, 0160),
(67, '9', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-26 16:44:22', '1', 'valer_blumhaghel.jpg', 'Valer', 'Blumhaghel', 'lelyblu@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'str Mihail Kogalniceanu, nr 22, oras Simeria, jud Hunedoara', '088223156', '40724527586', '', 'Gradient', 'Bright4', 'blue and white', '', '36732', '', 'AXA', '20AU6010118889', 'male', '1976-02-02', 'm', 'nv', 'dn', 74, 231, 065, 500, 023, 009, 018, 020, 003, 128, NULL, NULL, NULL, 0497, 0497),
(68, '6', 'Qualified', 'Waiting list', '', '2019-05-27 21:29:31', '1', 'eliot_nochez.jpg', 'ELIOT', 'NOCHEZ', 'nochezelio@gmail.com', 'ntl', NULL, NULL, 'France', '110 CHEMIN DES CHENEVIERS', '16CZ213125', '0670486976', '', 'Advance', 'Alpha6', 'purple and green ', '240993NE', '22423', '0601103X', 'VespirenGroup', '', 'male', '1993-09-24', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-28 01:33:08', '1', 'taesoo_lee.jpg', 'TAESOO', 'LEE', 'tribol@hanmail.net', 'ntl', NULL, NULL, 'Korea', '', 'M30082431', '8201047889300', '', 'SKYAYA', 'AYA', 'BLUE RED', '', '49037', 'KOR6058', 'AXA', '', 'male', '1971-07-17', 'l', 'nv', '0p', 33, 013, 002, 094, 005, 004, 006, 004, 001, 092, NULL, NULL, NULL, 0127, 0127),
(70, '8', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-28 10:04:15', '1', 'dusko_gorenc.jpg', 'DUSKO', 'GORENC', 'duskogorenc@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'GORENJE GRADISCE 13 8350 DOLENJSKE TOPLICE', 'PB0562835', '38631379628', '', 'GIN', 'ATLAS', 'GREEN', '64023', '19208', '0002/3256', 'AXA', '20AU60101-14318', 'male', '1958-12-12', 'xl', 'nv', 'am', 116, 025, 154, 345, 078, 057, 069, 500, 011, 006, NULL, NULL, NULL, 0745, 0745),
(71, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-28 15:28:10', '1', 'eunyoung_jho.jpg', 'EUNYOUNG', 'CHO', 'assaeun1@gmail.com', 'ntl', NULL, NULL, 'Korea', '1101003, 11, Bangbaecheonro 18gil, Seochogu, Seoul, Republic of ', 'M06537923', '821085756560', '821085756560', 'Gradient', 'Bright5', 'WIHTE GREEN', '269553', '9821', 'KOR6676', 'AXA', '20AU60101-20197', 'female', '1994-07-11', 'xs', 'nv', 'dn', 110, 016, 002, 189, 001, 000, 052, 004, 006, 005, NULL, NULL, NULL, 0086, 0086),
(72, '1', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-05-29 01:52:19', '1', 'gabriel_diamantini.jpg', 'Gabriel', 'Diamantini', 'gabi@skysports.com.ar', 'ntl', NULL, NULL, 'Argentina', 'Zapiola 4642 cp1429 CABA ARGENTINA', 'AAC983743', '541141405000', '541141405000', '', '', '', '', '58142', '747', '', '', 'male', '1974-05-15', 'l', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, '2', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-05-29 02:41:51', '1', 'pablo_bozzo.jpg', 'Pablo', 'Bozzo', 'bozzop@yahoo.com.ar', 'ytl', NULL, NULL, 'Argentina', 'Los Platanos 36  Nordelta', '17856362', '5491144448996', '5491144448996', 'ozone', '', '', '', '42945', '', '', '', 'male', '1966-05-13', 'l', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-29 11:42:13', '1', 'takeshi_furuta.jpg', 'Takeshi', 'Furuta', 't49359789@gmail.com', 'ntl', NULL, NULL, 'Japan', '', 'TR3453524', '819049359789', '', 'BGD', 'ADAM', 'Purple', '', '22715', '', '', '', 'male', '1970-02-27', 'xl', 'nv', 'bp', 29, 500, 391, 010, 500, 500, 010, 121, 110, 169, NULL, NULL, NULL, 1811, 1811),
(75, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 15:27:47', '1', 'spela_krnc.jpg', 'Spela', 'Krnc', 'spela.krnc1@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Segonje 14, 8275 Skocjan', '004788470', '0038631425130', '', 'Tripleseven', 'Pawn', 'red', '64052', '56984', '01594/11387', '', '', 'female', '2001-02-12', 's', 'nv', 'dn', 124, 008, 015, 500, 500, 500, 077, 416, 032, 077, NULL, NULL, NULL, 1625, 1625),
(76, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 15:56:00', '1', 'pawel_grzybowski.jpg', 'Pawel', 'Grzybowski', 'info@parastyle.pl', 'ntl', NULL, NULL, 'Poland', 'Generala Stanislawa Maczka 4, 37700 Przemysl', 'ES9013247', '48790767353', '48790767353', 'AirDesign', 'EAZY2', '', '30834', '25906', 'POL-2013', 'ERGOHESTIA', '13782A5222019', 'male', '1982-01-30', 'l', 'nv', 'bm', 7, 181, 123, 181, 011, 179, 500, 003, 000, 002, NULL, NULL, NULL, 0680, 0680),
(77, '5', 'Qualified', 'Canceled', '', '2019-05-30 16:31:37', '1', 'tomasz_chodyra.jpg', 'Tomasz', 'Chodyra', 'tchodyra@gmail.com', 'ntl', NULL, NULL, 'Poland', 'Batorego, 4', 'AF1200293', '781333838', '48781333838', 'BGD', 'Adam', '', '65651', '20910', 'POL-117/13', 'TOMASZCHODYRA', '', 'male', '1985-10-07', 'xl', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 18:54:23', '1', 'slawomir_kucharski.jpg', 'Slawomir', 'Kucharski', 'Biuro@best-tandem.pl', 'ntl', NULL, NULL, 'Poland', 'Pilotw 8c 66 015 Zielona Gora', 'AUS038788', '48609981282', '48609981282', 'Gin', 'Bolero4', 'Blue , red', '', '56131', 'POL-172/16', 'Hestia', '14382A5222019', 'male', '1983-09-20', 'l', 'nv', '0p', 35, 002, 003, 500, 010, 004, 002, 003, 004, 060, NULL, NULL, NULL, 0088, 0088),
(79, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 20:37:34', '1', 'boguslaw_pelczar.jpg', 'Boguslaw', 'Pelczar', 'krogulec141@interia.pl', 'ytl', NULL, NULL, 'Poland', 'Polnocna 141 38422 Kroscienko Wyzne', 'EJ89797072', '48608452062', '', 'Dudek', 'Nemo4', 'red', '92037', '46596', 'POL-266/15', 'ERGOHESTIA', '11342A5222018', 'male', '1960-08-13', 'm', 'nv', 'am', 60, 019, 002, 500, 115, 017, 002, 005, 181, 078, NULL, NULL, NULL, 0419, 0419),
(80, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 22:09:25', '1', 'sunita_thapa.jpg', 'Sunita', 'Thapa', 'thpsunita@gmail.com', 'ytl', NULL, NULL, 'Nepal', '', '06381033', '48796439367', '48796439367', 'Bgd', 'Adam', 'Green', '', '36270', '', '', '', 'female', '1992-02-27', 's', 'nv', 'ap', 10, 023, 500, 367, 209, 293, 079, 039, 013, 033, NULL, NULL, NULL, 1056, 1056),
(81, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-30 22:12:31', '1', 'krystian_wajs.jpg', 'Krystian', 'Wajs', '3xcree@gmail.com', 'ntl', NULL, NULL, 'Poland', '', 'EF4419377', '48579650350', '', '', '', '', '', '13178', '', '', '', 'male', '1981-11-04', 'm', 'nv', 'dn', 79, 007, 058, 018, 061, 014, 066, 455, 500, 241, NULL, NULL, NULL, 0920, 0920),
(82, '4', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-05-30 23:30:23', '1', 'luciano_donamari.jpg', 'Luciano', 'Donamari', 'luciano_donamari@hotmail.com', 'ntl', NULL, NULL, 'Argentina', 'Pasaje Guarani 2452 CP2800 Zarate Buenos Aires', '01677605', '5493487520278', '5493487520278', 'Niviuk', 'Ikuma', 'Naranja,celeste, negro y blanco', '', '67671', '1237', '', '', 'male', '1977-03-24', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-31 00:09:04', '1', 'abel_varga.jpg', 'Abel', 'Varga', 'vargaabel98@gmail.com', 'ntl', NULL, NULL, 'Hungary', '', '763446PA', '06702246168', '', 'Phi', 'Sonata', 'Red', '15455', '55023', '', '', '', 'male', '1998-11-20', 'xl', 'nv', 'dn', 38, 003, 005, 012, 009, 114, 500, 005, 192, 001, NULL, NULL, NULL, 0341, 0341),
(84, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-31 03:57:42', '1', 'juliana_noscue.jpg', 'JULIANA', 'NOSCUE', 'Juliana.noscue@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Corregimiento de San Flix, Municipio de Bello Antioquia', 'AT732065', '573192431345', '573504348303', 'SOLPARAGLIDERS', 'PRYMUS5S', 'YELLOW, BLUE, RED', '109817', '46290', 'COL-774-PG', 'SaviaSalud', '1020488195', 'female', '1997-12-14', 'xxs', 'nv', 'bp', 84, 500, 500, 064, 093, 175, 500, 101, 039, 074, NULL, NULL, NULL, 1546, 1546),
(85, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-31 09:09:18', '1', 'sandor_kaszas.jpg', 'Sandor', 'Kaszas', 'casy-@freemail.hu', 'ytl', NULL, NULL, 'Hungary', 'Szilfakalja 6 4200 Hajduszoboszlo ', 'BJ8585783', '36303471249', '', 'BGD', 'MAGIC', 'LEO', 'HUN4032', '11384', 'HUN4032', 'Allianz', '654906', 'male', '1973-10-12', 'm', 'nv', '0p', 11, 005, 064, 123, 005, 009, 045, 199, 002, 088, NULL, NULL, NULL, 0341, 0341),
(86, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-05-31 12:03:56', '1', 'mariko_ito.jpg', 'MARIKO', 'ITO', 'mari110@aqua.ocn.ne.jp', 'ntl', NULL, NULL, 'Japan', '', 'TK1554638', '819091747078', '', 'advance', '', '', '', '35676', '', '', '', 'female', '1970-03-14', 'm', 'nv', 'ap', 73, 500, 002, 003, 006, 049, 500, 007, 285, 015, NULL, NULL, NULL, 0867, 0867),
(87, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 12:10:03', '1', 'bediha_sahinturk.jpg', 'bediha', 'sahinturk', 'bediha.sahinturk@hotmail.com', 'ntl', NULL, NULL, 'Turkey', 'fevzi cakmak caddesi no 79 buca izmir', '51463408292', '905307919735', '905307919735', 'skywalk', 'mescal4', 'turanj', '', '51632', '06849', '', '', 'female', '1984-12-02', 's', 'nv', 'am', 68, 126, 500, 046, 307, 144, 122, 050, 500, 500, NULL, NULL, NULL, 1795, 1795),
(88, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 13:03:08', '1', 'cemal_direkci.jpg', 'Cemal', 'Direkci', 'cemaldirekci@gmail.com', 'ntl', NULL, NULL, 'Turkey', '', 'U05985611', '905338758059', '905338758059', 'Bgd', 'Adam', 'Ree green yellow purple', '83503', '10092', '717', '', '', 'male', '1985-07-17', 'l', 'nv', 'ap', 103, 003, 005, 360, 004, 004, 100, 004, 023, 004, NULL, NULL, NULL, 0147, 0147),
(89, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 14:29:30', '1', 'serhad_karaduman.jpg', 'Serhad', 'Karaduman', 'serhad.karaduaman@gmail.com', 'ntl', NULL, NULL, 'Turkey', 'Cavusoglu Mahallesi Egitim Sokak Ayyildiz Apartmani No 12 Kat 1 Kartal Istanbul TURKEY ', 'Z20051212', '905348846666', '905348846666', 'SKY', 'GAIA', 'White Purple', '4895', '41379', '4895', 'ANADOLUANONMTRKSGORTARKET', '161558119', 'male', '1988-08-03', 'l', 'nv', '0p', 114, 011, 004, 380, 205, 001, 012, 328, 001, 500, NULL, NULL, NULL, 0942, 0942),
(90, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 15:07:27', '1', 'mustafa_uri.jpg', 'mustafa', 'uri', 'mustafa_uri@hotmail.com', 'ntl', NULL, NULL, 'Turkey', 'DHMI ADANA HAVA LIMANI', '2889366', '905356356262', '905356356262', 'skywalk', 'mescal4', 'blu whte', '', '9551', '254', '', '', 'male', '1972-04-08', 'l', 'nv', '0p', 87, 012, 002, 500, 004, 121, 008, 022, 267, 036, NULL, NULL, NULL, 0472, 0472),
(91, '8', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 17:01:08', '1', 'dagyeom_lee.jpg', 'Dagyeom', 'LEE', 'acrobeauty@naver.com', 'ntl', NULL, NULL, 'Korea', '3233, Wolchonro, Mohyeoneup, Cheoingu, Yonginsi, Gyeonggido, Republic of Korea', 'M14938513', '821071064900', '82171064900', 'Gradient', 'Bright5', 'wihte green', '269985', '19704', 'kor4080', 'axa', '20au60101-18835', 'female', '1990-11-20', 'xs', 'nv', 'dn', 95, 234, 006, 500, 005, 001, 005, 005, 500, 500, NULL, NULL, NULL, 1256, 1256),
(92, '10', 'Qualified', 'Waiting list', '', '2019-06-01 17:27:35', '1', 'ailyn_gonzalez.jpg', 'Ailyn', 'Gonzalez', 'ailyngonzalez53@gmail.com', 'ntl', NULL, NULL, 'Colombia', 'Carrera 32 13 231', 'AU244075', '573045465580', '573045465580', 'BGD', 'ADAM', 'Ruby', '131081', '54893', 'COL-1040-PG', 'Compensar', '1030590469', 'female', '1991-02-11', 'xs', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-01 19:18:10', '1', 'umut_akcil.jpg', 'UMUT', 'AKCIL', 'uakcil@gmail.com', 'ntl', NULL, NULL, 'Turkey', 'Fazil Orhan Aniler sok no16 Kermiya Mersin 10 Turkey', 'U06017290', '905488762033', '905488762033', 'BGD', 'ADAM', 'WHITE ORANGE YELLOW', '106556', '9552', '837', 'THK', '', 'male', '1983-02-16', 's', 'nv', 'ap', 49, 005, 380, 013, 004, 267, 054, 005, 001, 002, NULL, NULL, NULL, 0351, 0351),
(94, '9', 'Qualified', 'Waiting list', '', '2019-06-03 03:36:30', '1', 'hyunhee_kim.jpg', 'Hyunhee', 'Kim', '79praise@naver.com', 'ntl', NULL, NULL, 'Korea', 'Yugegil, Chogyemyeon, Hapcheongun', 'M46246814', '821031153777', '811031153777', 'Sky', 'Fides5', 'Kiwi', 'KOR3394', '39312', 'KOR3394', 'AXA', '20AU6010118544', 'female', '1979-07-01', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, '10', 'Qualified', 'Confirmed', '', '2019-06-03 05:27:19', '1', 'cho.jpg', 'Soyoung', 'Cho', 'sshyong3@gmail.com', 'ntl', NULL, NULL, 'Korea', '', 'm61588950', '821064056560', '', 'Gradient', 'Bright5', 'green, white', '', '55344', '', '', '', 'female', '1994-07-11', 's', 'nv', 'dn', 126, 048, 007, 001, 005, 014, 005, 008, 019, 008, NULL, NULL, NULL, 0067, 0067),
(96, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-03 07:23:12', '1', 'moonseob_lim.jpg', 'Moonseob', 'Lim', 'moonseob@hotmail.co.kr', 'ntl', NULL, NULL, 'Korea', '53, Sinchongil, Okcheonmyeon, Yangpyeonggun, Gyeonggido, Republic of Korea', 'm39280564', '821051163677', '821051163677', 'Davinci', 'Point', 'white', 'Kor1535', '2772', 'Kor1535', 'AXA', '20AU60101-18910', 'male', '1983-07-14', 'l', 'nv', '0p', 119, 003, 010, 003, 500, 003, 003, 003, 160, 022, NULL, NULL, NULL, 0207, 0207),
(97, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-03 11:27:51', '1', 'vlastimil_kricnar.jpg', 'Vlastimil', 'Kricnar', 'Vlastimil.kricnar@gmail.com', 'ntl', NULL, NULL, 'Czech Republic', 'K Rozvodne 99, 530 09, Pardubice ', '41799639', '420725947729', '420725947729', 'Axispara', 'Compact3', 'Red, blue, white', '', '30868', '', '', '', 'male', '1995-07-14', 'l', 'nv', 'dn', 28, 114, 002, 008, 000, 207, 003, 005, 032, 004, NULL, NULL, NULL, 0168, 0168),
(98, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-03 17:28:44', '1', 'thumb_h351e20190603_172815.jpg', 'Vladimir', 'Sova', 'vladimir.sova@gmail.com', 'ntl', NULL, NULL, 'Czech Republic', '', '43992228', '00420602155211', '', '', '', '', '', '13634', '', '', '', 'male', '1977-10-14', 'l', 'nv', 'dn', 118, 026, 007, 005, 054, 052, 002, 001, 005, 007, NULL, NULL, NULL, 0105, 0105),
(99, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-03 21:48:49', '1', 'matjaz_sluga.jpg', 'Matjaz', 'Sluga', 'sluga.matjaz@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Sevce 7, 3272 Rimske Toplice, Slovenija', 'PB0835573', '38641619909', '38641619909', 'Gradient', 'Bright5', 'Blue Red', '111019', '8389', '0334/3659', 'AXAVersicherungAG', '20AU60101-12739', 'male', '1974-01-07', 'l', 'nv', 'dn', 25, 105, 004, 002, 003, 005, 000, 001, 002, 001, NULL, NULL, NULL, 0018, 0018),
(100, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-04 02:51:39', '1', 'thumb_y$3$9P71003-111056(1).jpg', 'Igor', 'Korenskiy', 'korenskiy.igor@gmail.com', 'ntl', NULL, NULL, 'Kazakhstan', 'st Tauelsyzdyk 101,040000,Taldykorgan', '07464056', '77772222779', '77772222779', 'Gin', 'bolero6', 'orange,black, white', '', '48764', '', '', '', 'male', '1969-08-14', 'l', 'nv', '0m', 71, 163, 318, 033, 003, 356, 500, 015, 047, 014, NULL, NULL, NULL, 0949, 0949),
(101, '7', 'Qualified', 'Waiting list', '', '2019-06-04 05:10:23', '1', 'thumb_xhfm$photo.jpg', 'Igor', 'Virshki', 'igvir@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '040000, Taldykorgan', 'N09297342', '77772260919', '77772260919', 'niviukkoyot2', '', '', '', '21704', '', '', '', 'male', '1968-01-03', 'xxl', 'nv', 'bp', 117, 385, 012, 019, 500, 093, 006, 500, 222, 167, NULL, NULL, NULL, 1404, 1404),
(102, '8', 'Qualified', 'Waiting list', '', '2019-06-04 05:43:59', '1', 'thumb_y4dw1DAA309CD-240F-49B7-B82A-45A5E8C95288.jpeg', 'Almas', 'Akhmetov', 'almazyan87@gmail.com', 'ntl', NULL, NULL, 'Kazakhstan', 'Almaty, micro district Ainabulak 3, home 124,  050014', 'N09816360', '77057722239', '77057722239', 'GIN', 'Bolero6', 'Red, blue, white', '', '60985', '', '', '', 'male', '1987-06-14', 'l', 'nv', 'abp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-04 08:56:53', '1', 'viktor_kozlov.jpg', 'Viktor', 'Kozlov', 'vik_tk@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '040000 Kazakhstan Taldykorgan Chaikovskiy str 6 app 69', 'N09282742', '77772266020', '77772266020', 'Swing', 'Mistral', 'red blue white', '84835', '29009', '302', '', '', 'male', '1969-05-15', 'xl', 'nv', 'dn', 26, 500, 087, 024, 003, 165, 006, 004, 003, 500, NULL, NULL, NULL, 0792, 0792),
(104, '6', 'Qualified', 'Waiting list', '', '2019-06-04 10:45:50', '1', 'yuriy_virshki.jpg', 'Yuriy', 'Virshki', 'pro100virshki@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '040000, Taldykorgan', 'M09302823', '77051412157', '77051412157', '', '', '', '', '19255', '', '', '', 'male', '1959-10-02', 'xxl', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-04 13:23:59', '1', 'alexandr_smelyanets.jpg', 'Alexandr', 'Smelyanets', 'alex.sm76@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '', '11403108', '77773874495', '', '', '', '', '', '46873', '', '', '', 'male', '1976-05-25', 'xxl', 'nv', 'ap', 52, 132, 020, 238, 078, 151, 004, 030, 187, 424, NULL, NULL, NULL, 0840, 0840),
(106, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-04 13:25:01', '1', 'thumb_b1coppf.jpg', 'Marketa', 'Tomaskova', 'tomaskova.mar@gmail.com', 'ntl', NULL, NULL, 'Czech Republic', 'Sladeckova 64, Ostrava', '203859949', '420608647479', '420608647479', 'BGD', 'ADAM', 'blue, white, red', '0078', '8382', '380122', 'csobPojistovna', 'iZPV191053', 'female', '1983-09-15', 'm', 'yv', 'dn', 53, 397, 071, 500, 004, 109, 003, 003, 064, 005, NULL, NULL, NULL, 0656, 0656),
(107, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-04 22:48:57', '1', 'thumb_sbi$g20130901 (38).JPG', 'Aart', 'deHarder', 'aartdeharder@gmail.com', 'ntl', NULL, NULL, 'Netherlands', 'Hofrustlaan 3, 6721TA  Bennekom, The Netherlands', 'NN3D9LBH0', '31653140864', '', 'BGD', 'ADAM', 'Pink, Purple, yellow, green, blue', '624677', '8162', '6474', 'Riskdirect', '5369-210171592', 'male', '1964-05-10', 'xl', 'nv', 'dn', 16, 417, 390, 500, 500, 500, 500, 282, 500, 500, NULL, NULL, NULL, 3589, 3589),
(108, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 08:34:20', '1', 'thumb_jmkpxFAI 10 PGAWCH_Serbia_2019.jpg', 'Andris', 'Kuzmans', 'akuzmans@gmail.com', 'ntl', NULL, NULL, 'Latvia', 'Pernavas str 8, Jelgava, LV3004, Latvia', 'LV4265034', '37129420202', '37129420202', '', '', '', '81510', '47889', 'P529', '', '', 'male', '1973-11-16', 'm', 'yv', '0p', 57, 004, 001, 500, 003, 500, 043, 004, 115, 025, NULL, NULL, NULL, 0695, 0695),
(109, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 10:25:26', '1', 'krisjanis_sietins.jpg', 'Krisjanis', 'Sietins', 'krisjanis.sietins@gmail.com', 'ntl', NULL, NULL, 'Latvia', 'Rigas iela 21A, Baldone, LV2125 ', 'LV4532981', '37129339713', '37129339713', 'AIRDESIGN', 'Easy2', 'Lime, Orange ', '90878', '8837', 'I220', '', '', 'male', '1982-08-15', 'l', 'yv', '0p', 4, 004, 009, 025, 009, 004, 003, 154, 101, 002, NULL, NULL, NULL, 0157, 0157),
(110, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 12:00:26', '1', 'thumb_417bbyevgeniy_oreshkin_photo.jpg', 'YEVGENIY', 'ORESHKIN', 'jenyor@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', 'Orbita 1, hous 9, appartment 39, Almaty city, 050000', 'N12135773', '77772205452', '77772205452', 'GIN', 'Bolero6', 'orange', '', '3615', '', '', '', 'male', '1974-01-26', 'l', 'nv', 'ap', 106, 146, 500, 004, 013, 000, 089, 050, 007, 049, NULL, NULL, NULL, 0358, 0358);
INSERT INTO `applied_pilots` (`id`, `Nac_order_number`, `LR_Criteria`, `Status`, `Team`, `signedup`, `activated`, `pilot_image`, `f_name`, `l_name`, `email`, `team_leader`, `aloc_number`, `nation_rank`, `country`, `address`, `passport`, `cell`, `wapp`, `pg_man`, `pg_mod`, `pg_col`, `faiid`, `civlid`, `natflid`, `insuco`, `insuno`, `gender`, `date_birth`, `t_shirt`, `vegan`, `blood`, `order_number`, `round_1`, `round_2`, `round_3`, `round_4`, `round_5`, `round_6`, `round_7`, `round_8`, `round_9`, `round_10`, `round_11`, `round_12`, `total`, `total_team`) VALUES
(111, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 12:03:03', '1', 'perjessy_gyula.jpg', 'PERJESSY', 'GYULA', 'perjessyg@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'Aleea Sanatatii 2, Bl 15A 5,  Sf Gheorghe, 520064, Covasna', 'KV378336', '0040755107785', '', 'Gradient', 'Bright5', 'Red', '53', '23866', '1630/16.10.2017', 'AXAVersicherungAG', '20AU60101-17899', 'male', '1975-10-11', 'm', 'nv', '0p', 92, 007, 017, 001, 022, 093, 500, 023, 500, 149, NULL, NULL, NULL, 0812, 0812),
(112, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 13:58:59', '1', 'thumb_r6a2t11102602_794094187333674_1979635731926389036_n.jpg', 'Martin', 'Jovanoski', 'jovanoskimartin@gmail.com', 'ytl', NULL, NULL, 'Macedonia', '11ti Oktomvri 36a', '0703990440019', '38971740028', '', 'BGD', 'Adam', 'Green', '', '8384', '183/06', 'Vatdar', '0743864289411', 'male', '1990-03-07', 'l', 'nv', 'ap', 42, 002, 500, 003, 007, 007, 260, 003, 001, 001, NULL, NULL, NULL, 0284, 0284),
(113, '9', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 13:59:21', '1', 'jan_sirl.jpg', 'Jan', 'Sirl', 'jan.sirl@seznam.cz', 'ntl', NULL, NULL, 'Czech Republic', 'Secska 1874, 10000, Prague', '204901869', '420602265664', '420602265664', 'Axis', 'Comet', 'Red, Yellow', 'CZE-0771', '57006', 'PL480190', 'CSOB', '8070453310', 'male', '1988-11-27', 'm', 'yv', 'ap', 125, 198, 000, 057, 006, 025, 004, 002, 004, 006, NULL, NULL, NULL, 0104, 0104),
(114, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 21:16:39', '1', 'thumb_jeu$1RAUSIS.jpg', 'Edijs', 'Gaspuitis', 'florenda@inbox.lv', 'ytl', NULL, NULL, 'Latvia', 'skolas iela 3259', 'LV4493610', '37126161966', '3716161966', 'BGD', 'ADAM', 'BLUE', '106419', '47887', 'P563', '', '', 'male', '1983-11-08', 'm', 'nv', 'ap', 76, 008, 500, 012, 035, 045, 500, 010, 050, 005, NULL, NULL, NULL, 0665, 0665),
(115, '7', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 22:21:22', '1', 'thumb_blv13Olga Foto FAI.jpg', 'Olga', 'Karuna', 'olga.zalane@gmail.com', 'ntl', NULL, NULL, 'Latvia', 'Sterstu str 9, LV1004, Riga', 'LV5658353', '37129116201', '37129116201', 'Skywalk', 'Mescal 5', '', '118727', '8842', 'P331', '', '', 'female', '1983-07-02', 's', 'nv', 'ap', 32, 500, 190, 500, 082, 500, 416, 201, 500, 500, NULL, NULL, NULL, 2889, 2889),
(116, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 22:22:15', '1', 'thumb_un1$f20180213_194218.jpg', 'Martins', 'Strazdins', 'strazdins@mail.ru', 'ntl', NULL, NULL, 'Latvia', 'Pulkveza Brieza str 93 37 Sigulda LV2150 ', 'LV4850171', '37130351005', '37120351005', 'BGD', 'Base', 'Glacier', '118726', '57562', 'P606', '', '', 'male', '1969-12-10', 's', 'yv', 'ap', 109, 048, 002, 500, 014, 404, 016, 500, 218, 169, NULL, NULL, NULL, 1371, 1371),
(117, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-05 22:28:09', '1', 'thumb_nddp$Vilnis Foto FAI.jpg', 'Vilnis', 'Gailums', 'vilnisgailumss@gmail.com', 'ntl', NULL, NULL, 'Latvia', '', 'LV5867848', '37129207777', '37129207777', '', '', '', '81508', '18702', 'P383', '', '', 'male', '1967-12-17', 'xl', 'nv', 'ap', 94, 004, 006, 060, 010, 002, 003, 125, 007, 021, NULL, NULL, NULL, 0113, 0113),
(118, '6', 'Qualified', 'Waiting list', '', '2019-06-05 22:36:38', '1', 'thumb_t11c1Eriks Foto FAI.jpg', 'Eriks', 'Gailums', 'erik.gailums@inbox.lv', 'ntl', NULL, NULL, 'Latvia', '', 'LV6011500', '37128337741', '37128337741', '', '', '', '90885', '47883', 'P549', '', '', 'male', '2002-01-10', 'xl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'NAC approval needed', '', 'Waiting list', '', '2019-06-06 12:21:38', '1', 'thumb_8ka4yAlla foto.jpg', 'Alla', 'Shostak', 'a.zaya.85@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '    ', 'N11698264', '77768268585', '', 'Icaro', 'WildCatTE', 'White  pink', '134488', '48770', '', '', '', 'female', '1985-12-10', 'xs', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-07 11:58:44', '1', 'thumb_$$ybqVV.jpg', 'Vlastimil', 'Vachtl', 'vachtlv@seznam.cz', 'ntl', NULL, NULL, 'Czech Republic', 'Na Pruhonu 768, 33203 Stahlavy', '40255896', '420724618559', '420724618559', 'BGD', 'ADAM', 'White, Red, Blue', 'CZE-0568', '39671', 'PL420759', '', '', 'male', '1979-05-30', 'm', 'nv', 'ap', 72, 002, 008, 004, 005, 002, 011, 005, 017, 002, NULL, NULL, NULL, 0039, 0039),
(121, '2', 'Qualified', 'Canceled', NULL, '2019-06-07 13:29:32', '1', 'carlos_melendres.jpg', 'Carlos', 'Melendres', 'patalosh@hotmaill.es', 'ytl', NULL, NULL, 'Ecuador', 'San rafael', '0602938441', '593', '998307602', 'Niviuk', 'Hook', 'Blue', '0602938441', '42921', '10192', '', '', 'male', '1976-12-15', 'l', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, '2', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 15:56:48', '1', 'thumb_a$01nmario andalucia.jpg', 'Mario', 'Moreno', 'escuelaparapenteeolox@gmail.com', 'ntl', NULL, NULL, 'Spain', 'Cerro del Aguila, 41 E2 Torre del Mar Malaga Spain', '33527091S', '0034650685969', '0034650685969', 'Apco', 'Hybrid', 'White', '3347', '30308', 'LU2019.0042', 'MGCmutua', '236092-10P1992', 'male', '1975-05-04', 'l', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, '8', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 20:59:18', '1', 'thumb_m9t07AZAHARA 2 NO.jpg', 'VELASCO', 'AZAHARA', 'azahara@vuelolibre.net', 'ntl', NULL, NULL, 'Spain', 'CALLE BAHIA DE CADIZ, BLOQUE 9, ZIP CODE 41009 SEVILLA, SPAIN', 'PAI444831', '34667874617', '34667874617', 'NIVIUK', 'FGRAVITY', '', '', '16695', 'LA20190043LU20190012', '', '', 'female', '1990-03-01', 's', 'yv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, '6', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 21:03:26', '1', 'thumb_$$$p$Maite Moreno.jpg', 'MORENO', 'MAITE', 'maitonamb@gmail.com', 'ntl', NULL, NULL, 'Spain', 'CALLE DE LA PENDIENTE, 31 ZIP CODE 28814  DAGANZO DE ARRIBA,  MADRID', '77307270S', '34629783359', '34629783359', 'PENDINGTOCONFIRM', 'PTC', 'PTC', '17392', '1624', '5218', 'MICSEGUROS', '201900055', 'female', '1969-10-15', 'xs', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, '3', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 21:22:40', '1', 'magarac.jpg', 'MARTINEZ', 'DANIEL', 'olivair1@gmail.com', 'ytl', NULL, NULL, 'Spain', 'C TAVARA 30 ZIP CODE 23379 SEGURA DE LA SIERRA, JAEN, SPAIN', 'AAI051462', '34607301716', '34607301716', 'SKYWALK', 'ARAK', 'MUSTARD', '', '1580', 'LA.2019.0518-LU.2019.0095', 'MGC', 'LA20190518', 'male', '1970-07-13', 'xl', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, '1', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 22:28:46', '1', 'thumb_$v$50IMG-20190415-WA0049.jpg', 'Ramon', 'Morillas', 'ramon@draconexion.com', 'ntl', NULL, NULL, 'Spain', 'carretera de sierra nevada 93  CP 18190  cenes de la vega granada', 'AAE493080', '34666522527', '34666522527', 'Niviuk', 'Doberman2', 'blue', '1321', '1576', 'LA2019.0019-LU2010.0003', 'helvetia', 'LA20190019-LU20190003', 'male', '1967-06-22', 'm', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, '4', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-07 23:30:10', '1', 'thumb_we1n1emiko.jpg', 'Emiko', 'Morota', 'morota@gmail.com', 'ntl', NULL, NULL, 'Spain', 'Calle Veronica  8    Cenes de la vega         Granada   18190', 'TZ1168397', '34647889745', '34647889745', 'NIVIUK', 'ARTIK', 'RED  GREEN', '3371', '8979', 'X1564550K', 'MGC', 'X1564550k', 'female', '1966-06-01', 'xs', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, '5', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-08 01:54:51', '1', 'thumb_4hmnrJuan Carlos Zapata Mantas.jpg', 'JUANCARLOS', 'ZAPATAMANTAS', 'juancarloszm@gmail.com', 'ntl', NULL, NULL, 'Spain', 'CALLE GERANIO N 16  CP 23660 ALCAUDETE JAEN', '46963116Z', '34699903292', '34699903292', 'ADVANCE', 'EPSILON7', 'BLUE', '', '10892', 'LU.2019.0036', 'MGCMUTUAPOLIZA23609210P1992YHELVETIAPOLIZAS0R110002559', '236092-10P1992', 'male', '1977-04-04', 'l', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, '7', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-08 11:03:35', '1', 'rafaelmarcos.jpg', 'RAFAELMARCOS', 'SANCHOMATAS', 'teatreveshacerloconmigo@gmail.com', 'ntl', NULL, NULL, 'Spain', 'Calle Fuentezuelas, 29  CP 18300 LOJA Granada', '34025971R', '34650984697', '34650984697', 'Escape', 'Stream', 'naranja gris marron', '', '10885', 'LA.2019.0446-LU.2019.0081', 'MGC', '', 'male', '1972-04-25', 'l', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-08 17:02:20', '1', 'thumb_dj1mp20190608_210033.jpg', 'Maxim', 'Moldovanov', 'Max.sky.devil@mail.ru', 'ntl', NULL, NULL, 'Kazakhstan', '040000, Taldykorgan', 'N08497870', '77771609151', '77771609151', '', '', '', '', '29017', '', '', '', 'male', '1975-08-30', 'xxl', 'nv', '0p', 90, 009, 004, 003, 010, 012, 009, 004, 014, 003, NULL, NULL, NULL, 0054, 0054),
(131, '8', 'Qualified', 'Waiting payment', '', '2019-06-08 20:36:55', '1', 'thumb_$$mj1Veronika Culková profil.jpg', 'Veronika', 'Culkov', 'veronika.culkova@gmail.com', 'ntl', NULL, NULL, 'Czech Republic', 'Serikova 11, 32600 Plzen', '1', '724874902', '', 'BGD', 'Adam', 'czech flag', 'FAI-616', '43937', 'PL420', 'CSOB', '8070453310', 'female', '1982-09-20', 'l', 'yv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-09 11:25:35', '1', 'reijden.jpg', 'John', 'van der Reijden', 'johnvanderreijden@hotmail.com', 'ntl', NULL, NULL, 'Netherlands', 'Bergse Linker Rottekade 221b, 3056LG  Rotterdam', 'NXRDCKR9', '31654757845', '', 'ProDesign', 'Cuga', 'Red, White, Blue', '6478', '13432', '13432', 'DEamersfoortse', '131706373', 'male', '1964-09-23', 'xl', 'nv', 'am', 43, 012, 076, 500, 005, 015, 500, 100, 500, 167, NULL, NULL, NULL, 1375, 1375),
(133, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-09 22:03:13', '1', 'william_lawrence.jpg', 'William', 'Lawrence', 'williamlawrence10@btinternet.com', 'ntl', NULL, NULL, 'United Kingdom', '152 Park Lane, Hornchurch, Essex, RM111Eg', '533808564', '07930208600', '07930208600', 'UP', 'Ascent4', 'Orange, white, blue ', '2410', '46908', '2410', '', '', 'male', '1994-06-10', 'xs', 'nv', 'dn', 66, 004, 123, 059, 007, 169, 004, 106, 007, 085, NULL, NULL, NULL, 0395, 0395),
(134, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-10 15:43:37', '1', 'thumb_1jthojá2_Fotor.jpg', 'Ivana', 'Balakova', 'ivkyball@gmail.com', 'ntl', NULL, NULL, 'Czech Republic', 'Novohradska 70, Ceske Budejovice, 37008', '40097621', '420774857515', '420774857515', 'BGD', 'Adam', 'red, yellow, green', '100538', '51605', 'PL505092', 'CSOBPojistovna', '7110002580', 'female', '1986-02-24', 's', 'yv', 'dn', 107, 008, 007, 005, 007, 500, 015, 007, 107, 149, NULL, NULL, NULL, 0305, 0305),
(135, '10', 'Qualified', 'Waiting list', '', '2019-06-10 17:45:38', '1', 'daniel_mirza.jpg', 'mirza', 'daniel', 'hotcars_ro@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'strcarangului nr 16,341554', '056084608', '400721701785', '400721701785', 'skyparagliders', 'aya', 'red,blue', '157', '57619', '1558', 'axa', '20AU60101-19771', 'male', '1973-08-07', 'l', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-10 20:42:20', '1', 'thumb_htta$DSCN0588.JPG', 'Andrew', 'Webster', 'awebster168@btinternet.com', 'ntl', NULL, NULL, 'United Kingdom', '103 Southleigh Road, Leeds, LS115XG', '522511759', '447932532295', '', 'UP', 'Ascent4', 'Blue, Orange, White', '18856', '8180', 'BHPA/242', 'Dogtag', '710333', 'male', '1966-08-20', 'm', 'nv', 'dn', 85, 003, 005, 086, 038, 008, 074, 042, 069, 013, NULL, NULL, NULL, 0252, 0252),
(137, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-11 09:05:50', '1', 'thumb_x1lriPaso foto.jpg', 'Jurijus', 'Jakovlevas', 'skrydispara@gmail.com', 'ytl', NULL, NULL, 'Lithuania', 'Liepu 28C17', '24728348', '37065999051', '37065999051', 'BGD', 'Adam', 'Yellow,red', '21243', '2481', '0048', '', '', 'male', '1968-02-09', 'xl', 'nv', 'bp', 14, 002, 306, 500, 051, 010, 116, 009, 297, 003, NULL, NULL, NULL, 0794, 0794),
(138, '4', 'Qualified', 'Waiting list', '', '2019-06-11 12:33:50', '1', 'thumb_knj1skamil_xicht.jpg', 'Kamil', 'Konecny', 'kamil@pghnizdo.cz', 'ntl', NULL, NULL, 'Czech Republic', 'Lubno 173', '39336861', '420608811699', '420608811699', 'SkyParagliders', 'Aya', 'Red, Blue, White', 'CZE0077', '1478', 'PL380027', 'CSOB', 'iZPV19820', 'male', '1975-04-09', 'm', 'nv', 'abp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-06-11 20:22:32', '0', 'thumb_15602773282603011205830487518601.jpg', 'Eduard', 'Daci', 'eduard.daci.95@gmail.com', 'ntl', NULL, NULL, 'Kosovo', 'Rr Emrush Miftari Nr23 , 30000 Peje', 'P00504337', '38349180925', '38349180925', 'Ozone', 'Atom3', 'White', '145220', '44277', 'RKS-HPL-016', '', '', 'male', '1995-10-08', 'l', 'yv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-11 22:21:13', '1', 'thumb_1sdi1Radek foto_1.jpg', 'Radek', 'Vaclavik', 'vaclavikr@seznam.cz', 'ntl', NULL, NULL, 'Czech Republic', 'Lucni 548, 33033 Mesto Touskov', '42867096', '420774723543', '', 'SKYPARAGLIDERS', 'AYA', 'white, red, yellow', '66066', '1499', 'PL130034', 'CSOB', 'iZPV191172', 'male', '1966-07-25', 'xl', 'nv', 'dn', 91, 063, 000, 500, 004, 500, 011, 003, 185, 002, NULL, NULL, NULL, 0768, 0768),
(141, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-11 22:26:33', '1', 'thumb_$$$4ageorge1-750x577.jpg', 'George', 'Cotet', 'george_bv_ro@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'Str Carpatilor 49, Brasov , Romania ', '053118022', '40727407007', '', 'Gradient', 'Bright4', 'Red', '31', '19089', '31', 'AXA', '20AU60101-16146', 'male', '1968-06-22', 'l', 'nv', 'ap', 108, 003, 003, 500, 003, 500, 003, 000, 364, 013, NULL, NULL, NULL, 0889, 0889),
(142, '1', 'Qualified', 'Waiting list', '', '2019-06-11 22:33:12', '1', 'thumb_nu1r$64317967_385918665463970_6274291433784475648_n.jpg', 'Agafiu', 'FlorianStefan', 'agafiuf@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'str Iezer nr 3', '535816', '40730707827', '40730707827', 'SkyCountry', 'Muscat3', 'Blue, Grey', '13', '10707', '1211', 'AXXA', '20AU60101-17898', 'male', '1977-05-13', 'l', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, '6', 'Qualified', 'Waiting list', '', '2019-06-12 16:27:45', '1', 'thumb_l5v1320190515_155756.jpg', 'takacs', 'gabriela', 'teodorvladimirr@yahoo.com', 'ntl', NULL, NULL, 'Romania', 'tudor vladimirescu 74', 'hd212131', '400771119639', '400771119639', 'skyparagliders', 'aya', 'red,blue', '154', '55766', '1646', 'axa', '20au60101-19903', 'female', '1980-10-04', 's', 'nv', 'am', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-12 17:24:30', '1', 'anton_svoljsak.jpg', 'Anton', 'SVOLJSAK', 'Tone.Svoljsak@gmail.com', 'ntl', NULL, NULL, 'Slovenia', 'Hrib 16, 4205 PREDDVOR', 'PB0950738', '38640621313', '38640621313', 'TRIPLESEVENPARAGLIDERS', 'DECK', 'WHITE BLUE GREEN', '111016', '8834', '0667-D2-1480', 'AXA', '20AU60101-12740', 'male', '1959-03-13', 'l', 'nv', 'ap', 51, 193, 001, 500, 004, 111, 003, 009, 080, 500, NULL, NULL, NULL, 0901, 0901),
(146, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-06-13 15:44:25', '0', 'thumb_Avni Kuci.jpg', 'Avni', 'Kuci', 'avnikuci73@gmail.com', 'ytl', NULL, NULL, 'Kosovo', 'Str Maksut Kuci 115 Suhareka Kosovo', 'P00955108', '38349224005', '38349224005', 'BGD', 'Adam', 'Green red', '145221', '44285', 'RKS-HPL-009', 'NA', 'NA', 'male', '1973-11-23', 'xl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, '7', 'Qualified', 'Confirmed', '', '2019-06-13 16:21:53', '1', 'thumb_9141$Scan_20190613 (3)2.jpg', 'Bojan', 'Ufceski', 'bojanuvceski@yahoo.com', 'ntl', NULL, NULL, 'Macedonia', '', 'B0552801', '38976686484', '', 'Gradient', 'BRIGHT5', 'Green', '', '39102', '', '', '', 'male', '1984-11-05', 's', 'nv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, '8', 'Qualified', 'Canceled', '', '2019-06-13 16:29:06', '1', 'thumb_sb$1$20181201_074603-1-1.jpg', 'Aleksandra', 'Ufceska', 'cunkoskaaleksandra@yahoo.com', 'ntl', NULL, NULL, 'Macedonia', '', 'C0053557', '38976800440', '', 'Gradient', 'BRIGHT5', 'Green', '', '46991', '', '', '', 'female', '1986-12-16', 'l', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'NAC approval needed', 'Qualified', 'Waiting list', '', '2019-06-13 16:46:37', '1', 'rika_wijayanti.jpg', 'Rika', 'Wijayanti', 'rikaw41@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jl trunojoyo gg4 songgokerto batu 65312', 'C3015615', '6287759983065', '6287759983065', 'Gradient', 'Bright5', 'Green black yellow ', '79556', '31245', 'PG1041', 'Allianz', 'JKT01-043599000017', 'female', '1994-09-08', 'm', 'yv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 'NAC approval needed', 'Qualified', 'Waiting list', '', '2019-06-13 16:48:30', '1', 'thumb_1$oyv20190613_194221.jpg', 'Aris', 'Afriansyah', 'arispara@ymail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jln raya puncak kp pensiunan rt02 rw 01 desa tugu selatan kec cisarua kab bogor postcode 16750', 'C1681309', '6287770238542', '6287777023842', 'GRADIENT', 'BRIGHT5', 'Red white', '80746', '31196', 'PG1027', '', '', 'male', '1994-04-15', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-13 16:57:30', '1', 'thumb_tv34$20190613_202425.jpg', 'Irvan', 'Winarya', 'Irvan120692@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jln raya puncak kp pensiunan rt02 rw 01 desa tugu selatan kec cisarua kab bogor postcode 16750', 'B6923361', '62812928378', '62812928378', 'GRADIENT', 'BRIGHT5', 'Red yelow', '108560', '46485', 'PG0982', '', '', 'male', '1992-06-12', 'l', 'nv', '0p', 69, 003, 002, 001, 001, 003, 002, 006, 004, 001, NULL, NULL, NULL, 0017, 0017),
(152, '8', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-13 16:58:46', '1', 'guntari.jpg', 'GitarezkyYuanita', 'Guntari', 'gitarez12@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Ds malongpong No 7 maja majalengka 45461', 'B8786569', '6285321444914', '6285321444914', 'GIN', 'Bolero6', 'Orange ', '113422', '43591', 'PG1092', 'EquityLifeIndonesiaAAInternational', '1910030663', 'female', '1996-07-02', 's', 'nv', 'dn', 50, 013, 045, 001, 001, 500, 004, 005, 166, 061, NULL, NULL, NULL, 0296, 0296),
(153, '4', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-13 17:15:38', '1', 'thumb_1$7am20190613_221301.jpg', 'Hening', 'Paradigma', 'digma.terbang@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Perumahan Telkom Mulyaharja blok B no 23, Kelurahan Cibereum Kecamatan Bogor Selatan', 'B0475732', '6281284948696', '6281284948696', 'Gradient', 'Bright5', 'RedWhite', '', '16252', 'PG0297', 'BPJS', '3374132406860001', 'male', '1986-06-24', 's', 'yv', '0p', 24, 092, 006, 018, 006, 007, 006, 450, 003, 025, NULL, NULL, NULL, 0163, 0163),
(154, 'NAC approval needed', '', 'Confirmed', 'Team pilot', '2019-06-13 17:54:10', '1', 'fanol.jpg', 'Fanol', 'Shala', 'fanolshala21@gmail.com', 'ntl', NULL, NULL, 'Kosovo', '15000 Obiliq', 'P01273295', '38349365106', '38349365106', 'BGD', 'ADAM', 'blue ', '190616', '46995', 'RKS-HPL-030', 'KosovaeRe', '111679', 'male', '1981-08-25', 'l', 'nv', 'bp', 37, 001, 015, 500, 003, 015, 372, 002, 001, 001, NULL, NULL, NULL, 0410, 0410),
(155, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-13 21:38:45', '1', 'thumb_09y01IMG_4879.jpeg', 'Andy', 'Shaw', 'fly@greendragons.co.uk', 'ytl', NULL, NULL, 'United Kingdom', 'Warren Barn Cottage, Slines Oak Road, Woldingham, Surrey CR3 7HN', '561550683', '447860875567', '447860875567', 'UP', 'Ascent4', 'Orange and Blue', '135', '6179', '135', 'BHPA', '01162611322', 'male', '1966-05-25', 'xl', 'nv', 'ap', 19, 004, 012, 050, 001, 064, 325, 011, 006, 015, NULL, NULL, NULL, 0163, 0163),
(156, 'NAC approval needed', '', 'Confirmed', 'Team pilot', '2019-06-13 22:53:12', '1', 'thumb_on1xm20190613_225250.jpg', 'Hizdri', 'Gubetini', 'hizigubetini@gmail.com', 'ntl', NULL, NULL, 'Kosovo', '10000 Prishtina', 'P00878730', '0038349889226', '0038349889226', 'BGD', 'Adam', 'Blue', '145211', '10106', 'RKS-HPL-020', 'SCARDIAN', '0121808', 'male', '1977-04-08', 'l', 'yv', '0p', 9, 001, 007, 004, 008, 242, 015, 059, 007, 043, NULL, NULL, NULL, 0144, 0144),
(157, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-06-13 22:54:10', '0', 'thumb_image.jpg', 'Veton', 'Sylaj', 'Vetonsylaj@hotmail.com', 'ntl', NULL, NULL, 'Kosovo', 'Rr naser bytyqi pn 23000 suharek', 'P00820912', '0038344718104', '00491608041991', 'BGD', 'Adam', 'Green ', '266141', '57557', 'Rks-hpl-033', '', '', 'male', '1991-04-08', 'l', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, '6', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-14 00:11:43', '1', 'thumb_1th1fhec prof.jpg', 'Hector', 'Zoia', 'hectorzoia@yahoo.com.ar', 'ntl', NULL, NULL, 'Argentina', 'Segundo Sombra 480', '16941429', '5491144714292', '5491144714292', 'OZONE', '', '', '77512', '10370', 'AR070105A', 'OSDE', '', 'male', '1964-04-15', 'xl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 06:16:04', '1', 'thumb_b9cqodainius_liegus.JPG', 'Dainius', 'Liegus', 'dainius.liegus@gmail.com', 'ntl', NULL, NULL, 'Lithuania', 'Tuskulenu 3 108, 09215, Vilnius', '22804068', '37067244452', '37067244452', 'FlowParagliders', 'ACE', 'Red, yellow, white', '27258', '21204', '441', '', '', 'male', '1985-02-06', 'l', 'nv', '0p', 41, 053, 021, 500, 036, 500, 112, 190, 017, 137, NULL, NULL, NULL, 1066, 1066),
(160, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 07:41:12', '1', 'david_mercer.jpg', 'DAVID', 'MERCER', 'davidmercer32@btinternet.com', 'ntl', NULL, NULL, 'United Kingdom', '260 WALLIS PLACE, HART ST, MAIDSTONE', '520491625', '447970483419', '', 'UP', 'ASCENT3', 'BLUE WHITE GREEN', '81546', '43936', '31919', 'DOGTAG', '707608', 'male', '1961-10-06', 'm', 'nv', 'bp', 46, 500, 115, 342, 500, 500, 002, 002, 118, 005, NULL, NULL, NULL, 1584, 1584),
(161, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 09:20:52', '1', 'thumb_82h1w20190614_091901.jpg', 'Borjan', 'Jovanoski', 'borjanjovanoski@gmail.com', 'ntl', NULL, NULL, 'Macedonia', '11ti Oktomvri 36 a', 'C0044150', '38975836625', '38975836625', 'BGD', 'Adam', 'White', '19009', '15212', '225/1240', 'AXA', '20AU60101-20140', 'male', '1995-06-23', 'm', 'nv', 'am', 15, 007, 002, 014, 008, 006, 006, 006, 005, 005, NULL, NULL, NULL, 0045, 0045),
(162, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-06-14 12:23:00', '0', 'thumb_b481uNijazi Gashi.jpg', 'Nijazi', 'Gashi', 'nijazi_20@hotmail.com', 'ntl', NULL, NULL, 'Kosovo', 'Dubrava, Suhareka 23000 Kosovo', 'K00605147', '38349121086', '38349121086', 'BGD', 'Adam', 'Green red', '190571', '49238', 'RKS-HPL-026', 'ILLYRIA', '0193212', 'male', '1966-10-28', 'l', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 12:46:16', '1', 'thumb_jv31eac2863b3-92bd-4ea7-be49-cc368ca1aa6e.jpg', 'Siljan', 'Zani', 'silianzani@icloud.com', 'ntl', NULL, NULL, 'Albania', '', 'Bc2192535', '355697723322', '355697723322', '', '', '', '', '44280', '', '', '', 'male', '1993-03-20', 'm', 'nv', 'ap', 20, 007, 062, 053, 004, 021, 041, 044, 194, 500, NULL, NULL, NULL, 0426, 0426),
(164, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 12:54:59', '1', 'e4nmgimage.jpg', 'Arber', 'Xhaja', 'arber.xhaja@yahoo.com', 'ntl', NULL, NULL, 'Albania', 'Tirane', '025489446', '0697401111', '0697401111', 'Bgd', 'Adam', 'Blu,green,red', '114480', '54079', '034', '', '', 'male', '1984-02-13', 'xl', 'nv', 'bp', 67, 005, 167, 087, 004, 066, 500, 003, 232, 003, NULL, NULL, NULL, 0567, 0567),
(165, '4', 'Qualified', 'Waiting list', '', '2019-06-14 13:50:12', '1', 'thumb_u3v61IMG-20190516-WA0003.jpeg', 'artion', 'vreto', 'artionvreto2012@gmail.com', 'ntl', NULL, NULL, 'Albania', 'LAli Demi, Rr Zonja Curre, Nr27', 'B5054981', '355692521086', '355692521086', 'Skywalk', 'Mescal5', 'Orange, dark Blue', '', '29284', 'AL-002', 'SKYSPORTSALBANIA', '', 'male', '1979-12-09', 'xxl', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 13:59:22', '1', 'thumb_l25$$20190614_135826.jpg', 'Blerian', 'Brace', 'blerian.brace@gmail.com', 'ntl', NULL, NULL, 'Albania', 'Rr Muhamet Gjollesha Nd148, H4, Ap 3', 'BE9296005', '355692068684', '355692068684', 'Gradient', 'Bright5', 'Blue white red', '', '44290', 'AL-07', '', '', 'male', '1985-04-03', 's', 'nv', 'ap', 47, 202, 147, 013, 007, 218, 500, 004, 012, 011, NULL, NULL, NULL, 0614, 0614),
(167, '7', 'Qualified', 'Waiting list', '', '2019-06-14 14:11:36', '1', 'arber_haxhiaj.jpg', 'arber', 'haxhiaj', 'salonarber@gmail.com', 'ntl', NULL, NULL, 'Albania', 'tirana ', 'BG4295652', '355692652277', '355692652277', 'ozone', 'atom', 'white red', '108766', '50644', '024', '', '', 'male', '1988-02-08', 'xxl', 'nv', 'abp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, '7', 'WPRS Missing criteria', 'Waiting list', NULL, '2019-06-14 16:45:13', '1', 'who_is.jpg', 'Hernan', 'Queirolo', 'hqueirolo@insumaq.com.ar', 'ntl', NULL, NULL, 'Argentina', 'Uriarte 2465', '14156127', '5491153377005', '5491153377005', 'Macpara', 'Eden6', 'Yellow , black', '', '67647', '337', '', '', 'male', '1960-08-01', 'xxl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 16:49:12', '1', 'thumb_w$22mIMG_20180825_124227_820.jpg', 'Adam', 'Spirkoski', 'spirkosky@gmail.com', 'ntl', NULL, NULL, 'Macedonia', 'Blaga Ruzeto 30A Prilep 7500', 'C0171131', '38975721819', '38975721819', 'BGD', 'Adam', '', '239957', '55458', '242-1327', 'AXA', '20AU60101-20143', 'male', '1992-09-28', 'm', 'nv', 'abm', 99, 019, 117, 500, 012, 043, 500, 148, 020, 012, NULL, NULL, NULL, 0871, 0871),
(170, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-06-14 17:58:21', '0', 'thumb_aaaaaaaa.jpg', 'BURIM', 'JASHARI', 'burimjashariph@gmail.com', 'ntl', NULL, NULL, 'Kosovo', 'Str Hakif zejnullahu 95 Podujevo', 'P00707386', '38349811565', '38349811565', 'BGD', 'Adam', 'Iceland', '145639', '44286', 'RSHHPL014', 'Sigal', '1', 'male', '1984-12-06', 'xl', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, '5', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 20:04:38', '1', 'charles_grant.jpg', 'Charles', 'Grantham', 'Charlesgrantham@icloud.com', 'ntl', NULL, NULL, 'United Kingdom', 'Flat 10 , 7 Claremont Gardens , Surbiton ,  United Kingdom , KT6 4TN ', '513346061', '07719048138', '07719048137', 'Up', 'Ascent4', 'Blue  Orange ', '89739', '8205', '099', '', '', 'male', '1961-02-04', 'xl', 'nv', 'bp', 102, 323, 500, 500, 500, 316, 127, 168, 500, 500, NULL, NULL, NULL, 2934, 2934),
(172, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-14 21:35:31', '1', 'mileto.jpg', 'MILE', 'JOVANOSKI', 'deltamj@t-home.mk', 'ntl', NULL, NULL, 'Macedonia', 'UL 11 ti Oktomvri 36a  Prilep', 'C1081567', '38970612437', '38970612437', 'Gradient', 'Brighit4', 'Red jelov', '157', '157', '105/0579', 'AXAVerdicherungAG', '20AU60101-20141', 'male', '1960-08-25', 'l', 'nv', 'abp', 63, 065, 004, 082, 001, 025, 003, 003, 026, 010, NULL, NULL, NULL, 0137, 0137),
(173, '3', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-15 07:06:37', '1', 'thumb_$bnf3IMG-20190608-WA0010.jpg', 'Johny', 'Effendy', 'Joniefo90@gmail.com', 'ntl', NULL, NULL, 'Indonesia', '', 'B1020629', '6281332165697', '6281332165697', 'Gradien', 'Bright5', 'Gren yelow', '79555', '19033', 'Pg0565', '', '', 'male', '1990-12-07', 'xl', 'yv', '0p', 104, 002, 015, 001, 004, 095, 004, 002, 016, 003, NULL, NULL, NULL, 0047, 0047),
(174, '6', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-15 07:54:24', '1', 'jafro.jpg', 'Jafro', 'Megawanto', 'jafro.mega@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Arumdalu 159 Batu, Jawa Timur , Indonesia', 'B3701907', '6281234665570', '6281234665570', 'Gradient', 'Bright5', 'Green black gold', '102607', '31224', 'PG0966', 'Equitylife', '1910030823', 'male', '1996-03-18', 'l', 'nv', '0p', 88, 001, 000, 002, 006, 004, 004, 002, 004, 001, NULL, NULL, NULL, 0018, 0018),
(176, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-15 16:36:01', '1', 'islami.jpg', 'Xhonatan', 'Islami', 'Xhonatanislami@gmail.com', 'ytl', NULL, NULL, 'Albania', 'Xhanfize Keko street 577 ap 12', 'BR5801369', '355692284000', '355692284000', 'Ozone', 'Atom3', 'White White White White', '', '29294', '', '', '', 'male', '1996-12-17', 'xl', 'nv', 'dn', 86, 010, 004, 003, 003, 005, 177, 003, 126, 005, NULL, NULL, NULL, 0159, 0159),
(177, '8', 'Qualified', 'Waiting list', '', '2019-06-15 17:09:26', '1', 'thumb_p$a1uFoto.jpg', 'Murariu', 'Ovidiu', 'real_mcmvra@libero.it', 'ntl', NULL, NULL, 'Romania', '', '055656156', '4035628164', '40735628164', 'UTurn', 'Evolution', 'Green Black', '93', '47006', '', '', '', 'male', '1986-12-12', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-15 19:28:56', '1', 'thumb_$em$k20190615_192549.jpg', 'Schubert', 'Andreas', 'andreas@wasserkuppe.com', 'ntl', NULL, NULL, 'Germany', 'Sandfeld 29, 36163 Poppenhausen Wasserkuppe ', 'C5ZR8XWZ32D', '491726551646', '491726551646', 'PapillonParagliders', 'RaqoonSM', 'White ', '', '21130', 'DHV10266', 'DHV', '10266', 'male', '1970-11-23', 'xs', 'nv', 'bp', 27, 001, 003, 025, 005, 008, 002, 009, 005, 004, NULL, NULL, NULL, 0037, 0037),
(179, 'NAC approval needed', '', 'Waiting list', '', '2019-06-15 22:03:59', '1', 'thumb_17n11Cri2.jpg', 'ElenaCristina', 'Brosu', 'crisbros74@gmail.com', 'ntl', NULL, NULL, 'Romania', 'Str Albinelor nr 56A, 500470, Brasov', '055451685', '40747900652', '', 'BGD', 'Adam', 'Blue, red, orange', '124', '49969', '1358', '', '', 'female', '1974-02-06', 's', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, '2', 'WPRS Missing criteria', 'Waiting list', '', '2019-06-16 16:10:51', '1', 'who_is.jpg', 'Jens', 'Salomon', 'Jens@gleitschirm-direkt.de', 'ntl', NULL, NULL, 'Germany', 'Weinerstrasse 18,36039 Fulda', 'L5Z80FWY1', '491731540117', '', '', 'Horizon', 'White, blue', '63743', '64241', '', '', '', 'male', '1977-05-29', 'm', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, '7', 'WPRS Missing criteria', 'Waiting list', '', '2019-06-18 23:50:11', '1', 'who_is.jpg', 'Anastasiia', 'Veretennikova', 'nas-tasya@mail.ru', 'ntl', NULL, NULL, 'Russian Federation', 'Vilenskiy pereulok, 3, kv 20, 191014, Saint Petersburg, Russia', '756138403', '79213261605', '79213261605', 'SkyParagliders', 'ATIS4', 'Yellow, Black, Green', '', '63956', '', '', '', 'female', '1982-07-25', 'l', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, 'NAC approval needed', '', 'Waiting list', 'Team pilot', '2019-06-20 05:49:52', '1', 'thumb_028$2Kiko_Portret.jpg', 'Kristijan', 'Temelkoski', 'epilotkiko@gmail.com', 'ntl', NULL, NULL, 'Macedonia', 'Metodija Patcev 45A', 'C0035335', '38975508493', '38975508493', 'Gradient', 'Bright4', 'YellowRed', '', '57', '161-0953', '', '', 'male', '1983-04-04', 'l', 'nv', 'bp', 82, 228, 008, 136, 009, 500, 500, 003, 043, 005, NULL, NULL, NULL, 0932, 0932),
(185, 'NAC approval needed', '', 'Waiting list', '', '2019-06-20 11:22:41', '1', 'thumb_b$2$fDSC_1484.JPG', 'Ersoy', 'AKSOY', 'ersoyaksoy1@hotmail.com', 'ntl', NULL, NULL, 'Turkey', '100 yil MH 85265 street no20 adana Turkiye ', 'S20302897', '905321787824', '905326327697', 'BGD', 'ADAM', 'Pink yellow', '44497', '22895767264', '6038', '', '', 'male', '1964-11-24', 'm', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 'NAC approval needed', '', 'Waiting list', NULL, '2019-06-21 07:20:56', '1', 'who_is.jpg', 'Franklin', 'Carrera', 'Frankarrera_@hotmail.com', 'ntl', NULL, NULL, 'Ecuador', '', '1708182777', '0979338083', '0979338073', 'NIVIUK', 'HOOK3', 'LILA', '10083', '18366', '', '', '', 'male', '1964-08-04', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, '1', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-27 23:24:30', '1', 'thumb_f81c720190118_113236.jpg', 'Tzvetan', 'Tzolov', 'ozyline@gmail.com', 'ytl', NULL, NULL, 'Bulgaria', 'Banishora 15D str, Balsha village,1217, Bulgaria', '8024143122345', '00359888024143', '00359888024143', 'Gradient', 'Bright5', 'white', '15922', '4278', '00046', '', '', 'male', '1972-05-03', 'l', 'yv', 'am', 22, 002, 002, 116, 002, 003, 010, 006, 002, 133, NULL, NULL, NULL, 0143, 0143),
(188, '2', 'Qualified', 'Confirmed', 'Team pilot', '2019-06-28 14:01:27', '1', 'thumb_3$v$$Ralev.jpg', 'Dimitar', 'Ralev', 'ralev.fly@gmail.com', 'ntl', NULL, NULL, 'Bulgaria', 'Pernik 1215, Bulgaria', '640049021', '00359888220609', '', 'Gradient', 'Bright5', 'Blue', '15921', '8242', '', '', '', 'male', '1962-02-12', 'xl', 'yv', 'dn', 48, 500, 035, 007, 500, 500, 134, 004, 004, 002, NULL, NULL, NULL, 1186, 1186),
(189, '1', 'Qualified', 'Canceled', NULL, '2019-06-30 18:29:17', '1', 'nawaf.jpg', 'Nawaf', 'Alrehaili', 'zahef1988@hotmail.com', 'ytl', NULL, NULL, 'Saudi Arabia', '', 'T973742', '966553303599', '966553303599', 'BGD', 'Adam', 'Purle', 'PG2124', '55840', '', '', '', 'male', '1988-10-17', 'm', 'yv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, 'NAC approval needed', '', 'Waiting list', '', '2019-07-01 08:15:02', '1', 'who_is.jpg', 'Pangeran', 'Dirgantara', 'Dirgantara30p@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jlcutnyakdhien no75 sumedang, jawabarat,indonesia 45311', 'B4747706', '6287798784798', '6287798784798', 'GINGLIDERS', 'YETI4', 'WHITE', '', '51587', '', '', '', 'male', '2002-10-01', 'm', 'nv', '0p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, '1', '', 'Confirmed', 'Team pilot', '2019-07-01 15:43:58', '1', 'thumb_1ri$$Purnomo ALamsyah.jpg', 'Purnomo', 'Alamsyah', 'purnomoalamsyah@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jl Raya Puncak Naringgul RT 01 RW 17, Kec Cisarua, Kab Bogor, Jawa Barat', 'B0463950', '6282115937422', '6282115937422', 'Niviuk', 'Hook4', 'Yellow, marroon', '', '31233', 'PG0970', '', '', 'male', '1991-06-22', 's', 'nv', 'dn', 115, 500, 003, 021, 001, 004, 003, 053, 002, 005, NULL, NULL, NULL, 0092, 0092),
(192, '2', '', 'Confirmed', '', '2019-07-01 16:23:21', '1', 'thumb_016$1IMG_20190701_212104.jpg', 'Yuda', 'Maisaputra', 'Yudamaisaputra5@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Payakumbuh city sumatra barat Indonesia', 'B5137473', '6287799049738', '6287799049738', 'Nova', 'Ion', 'Red', '265542', '53194', 'PG-1649', 'BPJSketenagakerjaan', '1307030505990001', 'male', '1999-05-05', 'm', 'yv', 'bp', 131, 003, 500, 004, 039, 001, 005, 005, 003, 002, NULL, NULL, NULL, 0062, 0062),
(193, '7', '', 'Confirmed', 'Team pilot', '2019-07-01 16:59:16', '1', 'thumb_1o6ypMilawati Sirin.jpg', 'Milawati', 'Sirin', 'milasirin@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jl Raya Hankam No 288 Jatiwarna, Pondok Melati', 'X447033', '628111773636', '628111773636', 'Gingliders', 'Yeti4', 'Green, white', '79524', '14680', 'PG0015', 'Medilum', '4252703659-01', 'female', '1970-10-31', 'm', 'nv', '0p', 123, 019, 069, 153, 500, 001, 009, 014, 299, 003, NULL, NULL, NULL, 0567, 0567),
(194, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-07-21 09:08:04', '0', 'thumb_IMG20170201183410.jpg', 'LendupT', 'Sherpa', 'tsherpa573@gmail.com', 'ytl', NULL, NULL, 'India', 'Tadong, gairi gaon, Gangtok, East Sikkim ', 'T7663385', '918900276878', '918900276878', 'Gradient', 'Golden4', 'Red and black ', '135631', '52488', '279360', 'LifeInsuranceCorporationofIndia', '455267549', 'male', '1980-06-01', 'm', 'yv', 'bp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-08-02 04:12:59', '0', 'thumb_IMG-20190802-WA0002.jpg', 'Renita', 'Dewiva', 'renita.dewiva@gmail.com', 'ntl', NULL, NULL, 'Indonesia', 'Jln raya puncak kp pensiunan rt02 rw 01 desa tugu selatan kec cisarua kab bogor postcode 16750', 'B0840087', '6285263109142', '6285263109142', 'GRADIENT', 'BRIGHT5', 'Red yelow', '108710', '50908', 'PG1146', '', '', 'female', '1990-10-19', 's', 'nv', 'ap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 'NAC approval needed', NULL, 'Waiting list', NULL, '2019-08-19 15:48:18', '0', 'thumb_5A5D5689-F81F-4B36-9DC7-47C20592F78E.jpeg', 'Abdulla', 'Althawadi', 'Abdullaalthawadidmm@gmail.com', 'ntl', NULL, NULL, 'Bahrain', 'Flat ', '2090154', '97338888290', '00966546444467', 'Macbara', 'Musc4', ' White and yellow ', '237491', '123310', '123310', '', '', 'male', '1987-09-07', 'm', 'nv', 'dn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, '11', '', 'Confirmed', '', '2019-09-03 00:55:49', '1', 'resized_$hvwlBYUNGCHANG JEUN.jpg', 'BYUNGCHANG', 'JEUN', 'gamire@naver.com', 'ntl', NULL, NULL, 'Korea', '', 'M69757260', '821073302626', '821073302626', 'SKYParagliders', 'AYA', 'BLUE , RED, WHITE', '', '215', '', 'AXA', '', 'male', '1973-12-02', 'l', 'nv', 'bp', 129, 002, 005, 004, 015, 005, 000, 500, 065, 002, NULL, NULL, NULL, 0098, 0098),
(198, '11', 'Qualified', 'Confirmed', '', '2019-09-08 16:18:58', '1', 'fredy_loaiza.jpg', 'Fredy', 'Loaiza', 'katireloaiza@gmail.com', 'ytl', NULL, NULL, 'Colombia', 'Cr 5 N 3 57 La Calera Colombia', 'AR273190', '573002931950', '573002931950', 'BGD', 'Magic', 'Multi', '135500', '42032', 'Col616pg', 'Dogtag', '999584645', 'male', '1977-08-30', 'm', 'nv', '0p', 130, 500, 291, 500, 500, 500, 161, 500, 500, 018, NULL, NULL, NULL, 2970, 2970);

-- --------------------------------------------------------

--
-- Table structure for table `applied_tls`
--

CREATE TABLE `applied_tls` (
  `id` int(11) NOT NULL,
  `tl_number` int(11) DEFAULT NULL,
  `NAC_order_number` enum('NAC approval needed','1','2','3','4','5','6','7','8','9','10','11','12') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NAC approval needed',
  `status` enum('Waiting list','Waiting payment','Confirmed','Canceled') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Waiting list',
  `signedup` datetime NOT NULL,
  `activated` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `tl_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL,
  `t_shirt` enum('xxs','xs','s','m','l','xl','xxl') COLLATE utf8_unicode_ci NOT NULL,
  `vegan` enum('yv','nv') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `applied_tls`
--

INSERT INTO `applied_tls` (`id`, `tl_number`, `NAC_order_number`, `status`, `signedup`, `activated`, `tl_image`, `f_name`, `l_name`, `email`, `country`, `address`, `passport`, `cell`, `wapp`, `gender`, `date_birth`, `t_shirt`, `vegan`) VALUES
(1, NULL, '1', 'Confirmed', '2019-04-19 03:59:54', '1', 'fredy_loaiza.jpg', 'Fredy', 'Loaiza', 'katireloaiza@gmail.com', 'Colombia', 'Cra 5 N 3  57 La Calera, Cundinamarca, Colombia', 'AR273190', '573002931950', '573002931950', 'male', '1977-08-30', 'm', 'nv'),
(2, NULL, 'NAC approval needed', 'Waiting list', '2019-04-21 21:12:38', '1', 'mohammad_razeghi.jpg', 'Mohammad', 'Razeghi', 'madrazeghi@yahoo.com', 'Iran', '', 'E47442149', '989121139351', '989121139351', 'male', '1969-04-23', 'xl', 'nv'),
(3, NULL, '1', 'Confirmed', '2019-04-25 11:34:11', '1', 'amiable_joel.jpg', 'Amiable', 'Joel', 'joel_amiable@yahoo.fr', 'France', '11 rue de BEAUMARCHAIS  77610  Les CHAPELLES BOURBON  France', '13CP70036', '33682741928', '33682741928', 'male', '1957-09-04', 'l', 'nv'),
(4, NULL, '1', 'Confirmed', '2019-05-05 03:51:40', '1', 'jialun_hou.jpg', 'Jialun', 'HOU', 'wazza1023@163.com', 'China', 'A14,Tiantan Dongli Zhongqu,Dongcheng District,Beijing100061,China', 'PE1077451', '8613581961387', '8613581961387', 'male', '1986-10-23', 'm', 'nv'),
(5, NULL, 'NAC approval needed', 'Waiting list', '2019-05-05 04:20:56', '0', 'walter_vallejo.jpg', 'Walter', 'Vallejo', 'wallas967@yahoo.com', 'Colombia', 'Kra 70C No 118 44 BOGOTA COLOMBIA', 'AS107126', '573132136136', '573132136136', 'male', '1957-06-17', 'm', 'nv'),
(6, NULL, 'NAC approval needed', 'Waiting list', '2019-05-22 03:45:40', '1', 'raul_perez.jpg', 'RAUL', 'PEREZ', 'rasanpez@hotmail.com', 'Ecuador', 'N52 San Jose E14127  Quito  Ecuador codigo postal 170514', '1704201597', '593997606991', '593997606991', 'male', '1955-05-01', 'm', 'nv'),
(7, NULL, '1', 'Confirmed', '2019-05-23 00:05:17', '1', 'thumb_1z61$Srecko Jost.jpg', 'Srecko', 'Jost', 'srecko.jost@guest.arnes.si', 'Slovenia', 'Pod kostanji 24, 3000 Celje, Slovenija', '003051811', '38641432447', '38641432447', 'male', '1956-07-01', 'xl', 'nv'),
(8, NULL, 'NAC approval needed', 'Waiting list', '2019-05-30 00:36:11', '1', 'thumb_1th1fhec prof.jpg', 'Hector', 'Zoia', 'hectorzoia@yahoo.com.ar', 'Argentina', 'Segundo Sombra 480', '16941429', '5491144714292', '5491144714292', 'male', '1964-04-15', 'xl', 'nv'),
(9, NULL, 'NAC approval needed', 'Waiting list', '2019-05-30 23:50:50', '0', 'elizabeth_marquez.jpg', 'ELIZABETH', 'MARQUEZ', 'elizabeth.marquezd1@hotmail.com', 'Colombia', 'Cra 70C No 118 44 Bogota Colombia', 'AS107207', '573157801832', '573157801832', 'female', '1961-11-28', 'm', 'nv'),
(10, NULL, 'NAC approval needed', 'Waiting list', '2019-06-01 15:11:41', '0', 'ali_zaimoglu.jpg', 'Ali', 'ZAIMOGLU', 'alicakir2544@gmail.com', 'Turkey', 'Turk Hava Kurumu Erzincan Havacilik Egitim Merkezi Mudurlugu Merkz ERZNCAN', 'Z00548159', '905382936100', '905382936100', 'male', '1983-09-05', 'm', 'nv'),
(11, NULL, '1', 'Confirmed', '2019-06-03 11:09:20', '1', 'thumb_s1$miIMG_3495.jpg', 'Lucie', 'Lednik Timiopulu', 'timilu@jemm.cz', 'Czech Republic', 'Postovni 1361, 73911 Frydlant nad Ostravici, Czech Republic', '39647066', '420604928675', '447937743771', 'female', '1908-07-03', 'xs', 'yv'),
(12, NULL, '1', 'Confirmed', '2019-06-11 10:12:20', '1', 'thumb_1$$n1Gp. Capt. Veerayuth Didyasarin.jpg', 'VEERAYUTH', 'DIDYASARIN', 'veerayuth.rasat@gmail.com', 'Thailand', 'ROYAL AERONAUTIC SPORTS ASSOCIATION OF THAILAND, 171 AVIATION SCIENCE MUSEUM BUILDING, Phaholyothin Road, Donmuang, Bangkok 10210', 'OF1097361', '66997824614', '66818985099', 'male', '1955-05-01', 'xl', 'nv'),
(13, NULL, '1', 'Confirmed', '2019-06-11 21:58:45', '1', 'who_is.jpg', 'Popa', 'ValentinIoan', 'skywalkromania@yahoo.com', 'Romania', 'str Tipografilor nr18', '583993', '40728983210', '40728983210', 'male', '1969-11-20', 'xl', 'nv'),
(15, NULL, '1', 'Waiting list', '2019-06-13 21:41:07', '0', 'thumb_09y01IMG_4879.jpeg', 'Andy', 'Shaw', 'fly@greendragons.co.uk', 'United Kingdom', 'Warren Barn Cottage, Slines Oak Road, Woldingham, Surrey CR3 7HN', '561550683', '447860875567', '447860875567', 'male', '1966-05-25', 'xl', 'nv'),
(16, NULL, 'NAC approval needed', 'Waiting list', '2019-06-15 05:25:53', '1', 'thumb_0pg1yPhoto_arif.jpg', 'ArifEko', 'Wahyudi', 'arifekow@gmail.com', 'Indonesia', 'Karah Vb no 11, Jambangan, Surabaya City, East Java, Indonesia', 'X616126', '62816526151', '62816526151', 'male', '1977-12-22', 'xl', 'nv'),
(17, NULL, 'NAC approval needed', 'Waiting list', '2019-06-15 09:18:43', '1', 'thumb_lf$ylIMG-20190527-WA0088.jpeg', 'Asgaf', 'Umar', 'asgaf.23.ag@gmail.com', 'Indonesia', 'Jl Toli Toli I no 110 Kel Silae Kota Palu Postel code 94227', 'B2710759', '6281245287889', '87881981636', 'male', '1975-04-09', 'l', 'yv'),
(18, NULL, 'NAC approval needed', 'Waiting list', '2019-06-20 13:26:12', '0', 'thumb_DSC_1484.JPG', 'Jaroslav', 'Jindra', 'jaroslav-jindra@email.cz', 'Czech Republic', 'U Apolla 693,  75701 Valasske Mezirici, Czech republic', '41910274', '420775082168', '420775082168', 'male', '1967-04-28', 'xl', 'nv'),
(19, NULL, 'NAC approval needed', 'Waiting list', '2019-06-26 17:54:55', '1', 'thumb_1111pIMG20170812081711-01.jpeg', 'Thomas', 'Widyananto', 'thomaswidyananto@gmail.com', 'Indonesia', 'Wirogunan RT 02 RW 02 Kartasura, Sukoharjo, Jawa Tengah Indonesia', 'C1310075', '6281329444997', '6281329444997', 'male', '1977-01-24', 'l', 'nv'),
(20, NULL, '1', 'Confirmed', '2019-06-26 18:11:23', '1', 'thumb_vdtchEPAC 2016 NL_ed foto.jpg', 'EduardWilhelmus', 'Hilleveld', 'edhilleveld@gmail.com', 'Netherlands', 'Slotermeer 21', 'NR6F692F9', '31611900607', '31611900607', 'male', '1960-08-06', 'xxl', 'nv'),
(21, NULL, 'NAC approval needed', 'Waiting list', '2019-06-27 01:19:42', '1', 'who_is.jpg', 'Yustira', 'Ramadhani', 'playhighparagliding@gmail.com', 'Indonesia', 'Sulfat Erfina Residence C31,Malang Indonesia 65123', 'B6703517', '628179622141', '628179622141', 'male', '1976-09-02', 'xl', 'nv'),
(22, NULL, 'NAC approval needed', 'Waiting list', '2019-06-27 23:20:30', '0', 'thumb_20190118_113236.jpg', 'Tzvetan', 'Tzolov', 'ozyline@gmail.com', 'Bulgaria', 'Banishora 15D str, Balsha village,1217, Bulgaria', '8024143122345', '00359888024143', '00359888024143', 'male', '1972-05-03', 'l', 'yv'),
(23, NULL, 'NAC approval needed', 'Waiting list', '2019-08-05 10:16:18', '0', 'thumb_eLzar Sukendro_pas Photo.png', 'elzar', 'sukendro', 'ekendro@gmail.com', 'Indonesia', 'Jl Cucakrawa no 1, Bukit Duri, Tebet, Jakarta Selatan', 'CO837684', '628118110625', '628118110625', '', '0000-00-00', 'xxl', 'yv'),
(24, NULL, 'NAC approval needed', 'Waiting list', '2019-08-05 10:18:52', '0', 'thumb_eLzar Sukendro_pas Photo.png', 'eLzar', 'Sukendro', 'ekendro@yahoo.com', 'Indonesia', 'jl Cucakrawa no 1, Bukit Duri, Tebet, Jakarta Selatan, Indonesia', 'CO837684', '628118110625', '628118110625', 'male', '1973-04-25', 'xxl', 'yv'),
(25, NULL, 'NAC approval needed', 'Waiting list', '2019-08-14 03:27:49', '0', 'thumb_KOREA TEAM LEADER - Dongil Wang.jpg', 'DONGIL', 'WANG', 'eastone71@nate.com', 'Korea', '118dong 2004ho,113, Samyangro 19gil, Gangbukgu, Seoul, Republic of Korea', 'M15829715', '821054940906', '', 'male', '1971-10-24', 'xl', 'nv'),
(26, NULL, 'NAC approval needed', 'Waiting list', '2019-09-07 18:01:45', '1', 'elzar.jpg', 'ELzar', 'Sukendro', 'Ljar_es@yahoo.com', 'Indonesia', 'Jl Cucakrawa no 1, Bukit Duri, Tebet, 120840', 'C0837684', '628118110625', '628118110625', 'male', '1973-04-25', 'xl', 'nv'),
(27, NULL, 'NAC approval needed', 'Waiting list', '2019-09-08 09:14:14', '1', 'who_is.jpg', 'Toni', 'Preston', 'Tonipreston1968@yahoo.com', 'United Kingdom', '', '546380230', '447852589123', '447852589123', 'female', '1968-07-30', 's', '');

-- --------------------------------------------------------

--
-- Table structure for table `team_results_per_series`
--

CREATE TABLE `team_results_per_series` (
  `id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `round` varchar(255) NOT NULL,
  `result` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_results_per_series`
--

INSERT INTO `team_results_per_series` (`id`, `country`, `round`, `result`) VALUES
(1, 'Albania', 'round_1', 0224),
(2, 'Bulgaria', 'round_1', 1502),
(3, 'Canada', 'round_1', 1205),
(4, 'China', 'round_1', 0065),
(5, 'Colombia', 'round_1', 0044),
(6, 'Czech Republic', 'round_1', 0099),
(7, 'France', 'round_1', 0113),
(8, 'Germany', 'round_1', 1501),
(9, 'Hungary', 'round_1', 1008),
(10, 'Indonesia', 'round_1', 0019),
(11, 'Japan', 'round_1', 1019),
(12, 'Kazakhstan', 'round_1', 0450),
(13, 'Korea', 'round_1', 0021),
(14, 'Kosovo', 'round_1', 1002),
(15, 'Latvia', 'round_1', 0020),
(16, 'Lithuania', 'round_1', 1055),
(17, 'Macedonia', 'round_1', 0093),
(18, 'Nepal', 'round_1', 1523),
(19, 'Netherlands', 'round_1', 1429),
(20, 'Poland', 'round_1', 0209),
(21, 'Romania', 'round_1', 0024),
(22, 'Russian Federation', 'round_1', 0174),
(23, 'Serbia', 'round_1', 0009),
(24, 'Slovenia', 'round_1', 0037),
(25, 'Thailand', 'round_1', 0022),
(26, 'Turkey', 'round_1', 0031),
(27, 'United Kingdom', 'round_1', 0334),
(28, 'United States', 'round_1', 1906),
(29, 'Albania', 'round_2', 0380),
(30, 'Bulgaria', 'round_2', 1037),
(31, 'Canada', 'round_2', 1516),
(32, 'China', 'round_2', 0024),
(33, 'Colombia', 'round_2', 0022),
(34, 'Czech Republic', 'round_2', 0009),
(35, 'France', 'round_2', 0030),
(36, 'Germany', 'round_2', 1503),
(37, 'Hungary', 'round_2', 1069),
(38, 'Indonesia', 'round_2', 0011),
(39, 'Japan', 'round_2', 1018),
(40, 'Kazakhstan', 'round_2', 0429),
(41, 'Korea', 'round_2', 0014),
(42, 'Kosovo', 'round_2', 1022),
(43, 'Latvia', 'round_2', 0018),
(44, 'Lithuania', 'round_2', 1327),
(45, 'Macedonia', 'round_2', 0131),
(46, 'Nepal', 'round_2', 2000),
(47, 'Netherlands', 'round_2', 1466),
(48, 'Poland', 'round_2', 0186),
(49, 'Romania', 'round_2', 0046),
(50, 'Russian Federation', 'round_2', 0029),
(51, 'Serbia', 'round_2', 0019),
(52, 'Slovenia', 'round_2', 0023),
(53, 'Thailand', 'round_2', 0014),
(54, 'Turkey', 'round_2', 0391),
(55, 'United Kingdom', 'round_2', 0255),
(56, 'United States', 'round_2', 2000),
(57, 'Albania', 'round_3', 0156),
(58, 'Bulgaria', 'round_3', 1123),
(59, 'Canada', 'round_3', 1503),
(60, 'China', 'round_3', 0036),
(61, 'Colombia', 'round_3', 0027),
(62, 'Czech Republic', 'round_3', 0022),
(63, 'France', 'round_3', 0033),
(64, 'Germany', 'round_3', 1525),
(65, 'Hungary', 'round_3', 1135),
(66, 'Indonesia', 'round_3', 0005),
(67, 'Japan', 'round_3', 0943),
(68, 'Kazakhstan', 'round_3', 0064),
(69, 'Korea', 'round_3', 0104),
(70, 'Kosovo', 'round_3', 1504),
(71, 'Latvia', 'round_3', 0597),
(72, 'Lithuania', 'round_3', 2000),
(73, 'Macedonia', 'round_3', 0235),
(74, 'Nepal', 'round_3', 1867),
(75, 'Netherlands', 'round_3', 2000),
(76, 'Poland', 'round_3', 1199),
(77, 'Romania', 'round_3', 0075),
(78, 'Russian Federation', 'round_3', 0014),
(79, 'Serbia', 'round_3', 0023),
(80, 'Slovenia', 'round_3', 0181),
(81, 'Thailand', 'round_3', 0011),
(82, 'Turkey', 'round_3', 0339),
(83, 'United Kingdom', 'round_3', 0537),
(84, 'United States', 'round_3', 2000),
(85, 'Albania', 'round_4', 0018),
(86, 'Bulgaria', 'round_4', 1502),
(87, 'Canada', 'round_4', 1077),
(88, 'China', 'round_4', 0014),
(89, 'Colombia', 'round_4', 0022),
(90, 'Czech Republic', 'round_4', 0013),
(91, 'France', 'round_4', 0041),
(92, 'Germany', 'round_4', 1505),
(93, 'Hungary', 'round_4', 1014),
(94, 'Indonesia', 'round_4', 0007),
(95, 'Japan', 'round_4', 0793),
(96, 'Kazakhstan', 'round_4', 0029),
(97, 'Korea', 'round_4', 0012),
(98, 'Kosovo', 'round_4', 1011),
(99, 'Latvia', 'round_4', 0036),
(100, 'Lithuania', 'round_4', 1087),
(101, 'Macedonia', 'round_4', 0025),
(102, 'Nepal', 'round_4', 1709),
(103, 'Netherlands', 'round_4', 1505),
(104, 'Poland', 'round_4', 0197),
(105, 'Romania', 'round_4', 0039),
(106, 'Russian Federation', 'round_4', 0012),
(107, 'Serbia', 'round_4', 0019),
(108, 'Slovenia', 'round_4', 0016),
(109, 'Thailand', 'round_4', 0013),
(110, 'Turkey', 'round_4', 0020),
(111, 'United Kingdom', 'round_4', 0546),
(112, 'United States', 'round_4', 2000),
(113, 'Albania', 'round_5', 0310),
(114, 'Bulgaria', 'round_5', 1503),
(115, 'Canada', 'round_5', 1512),
(116, 'China', 'round_5', 0011),
(117, 'Colombia', 'round_5', 0015),
(118, 'Czech Republic', 'round_5', 0188),
(119, 'France', 'round_5', 0154),
(120, 'Germany', 'round_5', 1508),
(121, 'Hungary', 'round_5', 1123),
(122, 'Indonesia', 'round_5', 0012),
(123, 'Japan', 'round_5', 1172),
(124, 'Kazakhstan', 'round_5', 0328),
(125, 'Korea', 'round_5', 0008),
(126, 'Kosovo', 'round_5', 1257),
(127, 'Latvia', 'round_5', 0455),
(128, 'Lithuania', 'round_5', 1510),
(129, 'Macedonia', 'round_5', 0081),
(130, 'Nepal', 'round_5', 1793),
(131, 'Netherlands', 'round_5', 1515),
(132, 'Poland', 'round_5', 0214),
(133, 'Romania', 'round_5', 0356),
(134, 'Russian Federation', 'round_5', 0221),
(135, 'Serbia', 'round_5', 0058),
(136, 'Slovenia', 'round_5', 0072),
(137, 'Thailand', 'round_5', 0123),
(138, 'Turkey', 'round_5', 0270),
(139, 'United Kingdom', 'round_5', 0557),
(140, 'United States', 'round_5', 2000),
(141, 'Albania', 'round_6', 1218),
(142, 'Bulgaria', 'round_6', 1144),
(143, 'Canada', 'round_6', 2000),
(144, 'China', 'round_6', 0008),
(145, 'Colombia', 'round_6', 0004),
(146, 'Czech Republic', 'round_6', 0012),
(147, 'France', 'round_6', 0361),
(148, 'Germany', 'round_6', 1502),
(149, 'Hungary', 'round_6', 1545),
(150, 'Indonesia', 'round_6', 0013),
(151, 'Japan', 'round_6', 0738),
(152, 'Kazakhstan', 'round_6', 0108),
(153, 'Korea', 'round_6', 0014),
(154, 'Kosovo', 'round_6', 1387),
(155, 'Latvia', 'round_6', 0065),
(156, 'Lithuania', 'round_6', 1228),
(157, 'Macedonia', 'round_6', 0769),
(158, 'Nepal', 'round_6', 1579),
(159, 'Netherlands', 'round_6', 2000),
(160, 'Poland', 'round_6', 0570),
(161, 'Romania', 'round_6', 0031),
(162, 'Russian Federation', 'round_6', 0015),
(163, 'Serbia', 'round_6', 0081),
(164, 'Slovenia', 'round_6', 0076),
(165, 'Thailand', 'round_6', 0072),
(166, 'Turkey', 'round_6', 0063),
(167, 'United Kingdom', 'round_6', 0207),
(168, 'United States', 'round_6', 1892),
(169, 'Albania', 'round_7', 0054),
(170, 'Bulgaria', 'round_7', 1010),
(171, 'Canada', 'round_7', 1024),
(172, 'China', 'round_7', 0014),
(173, 'Colombia', 'round_7', 0049),
(174, 'Czech Republic', 'round_7', 0009),
(175, 'France', 'round_7', 0069),
(176, 'Germany', 'round_7', 1509),
(177, 'Hungary', 'round_7', 1204),
(178, 'Indonesia', 'round_7', 0015),
(179, 'Japan', 'round_7', 0136),
(180, 'Kazakhstan', 'round_7', 0053),
(181, 'Korea', 'round_7', 0012),
(182, 'Kosovo', 'round_7', 1061),
(183, 'Latvia', 'round_7', 0293),
(184, 'Lithuania', 'round_7', 1199),
(185, 'Macedonia', 'round_7', 0015),
(186, 'Nepal', 'round_7', 1539),
(187, 'Netherlands', 'round_7', 1382),
(188, 'Poland', 'round_7', 0466),
(189, 'Romania', 'round_7', 0025),
(190, 'Russian Federation', 'round_7', 0013),
(191, 'Serbia', 'round_7', 0018),
(192, 'Slovenia', 'round_7', 0019),
(193, 'Thailand', 'round_7', 0013),
(194, 'Turkey', 'round_7', 0077),
(195, 'United Kingdom', 'round_7', 0161),
(196, 'United States', 'round_7', 1535),
(197, 'Albania', 'round_8', 0564),
(198, 'Bulgaria', 'round_8', 1006),
(199, 'Canada', 'round_8', 1217),
(200, 'China', 'round_8', 0013),
(201, 'Colombia', 'round_8', 0028),
(202, 'Czech Republic', 'round_8', 0058),
(203, 'France', 'round_8', 0217),
(204, 'Germany', 'round_8', 1505),
(205, 'Hungary', 'round_8', 1194),
(206, 'Indonesia', 'round_8', 0013),
(207, 'Japan', 'round_8', 1092),
(208, 'Kazakhstan', 'round_8', 0071),
(209, 'Korea', 'round_8', 0297),
(210, 'Kosovo', 'round_8', 1008),
(211, 'Latvia', 'round_8', 0273),
(212, 'Lithuania', 'round_8', 1314),
(213, 'Macedonia', 'round_8', 0052),
(214, 'Nepal', 'round_8', 1513),
(215, 'Netherlands', 'round_8', 2000),
(216, 'Poland', 'round_8', 0685),
(217, 'Romania', 'round_8', 0296),
(218, 'Russian Federation', 'round_8', 0056),
(219, 'Serbia', 'round_8', 0047),
(220, 'Slovenia', 'round_8', 0075),
(221, 'Thailand', 'round_8', 0091),
(222, 'Turkey', 'round_8', 0275),
(223, 'United Kingdom', 'round_8', 0200),
(224, 'United States', 'round_8', 1510),
(225, 'Albania', 'round_9', 0519),
(226, 'Bulgaria', 'round_9', 1135),
(227, 'Canada', 'round_9', 1675),
(228, 'China', 'round_9', 0008),
(229, 'Colombia', 'round_9', 0019),
(230, 'Czech Republic', 'round_9', 0013),
(231, 'France', 'round_9', 0516),
(232, 'Germany', 'round_9', 1504),
(233, 'Hungary', 'round_9', 1089),
(234, 'Indonesia', 'round_9', 0008),
(235, 'Japan', 'round_9', 0735),
(236, 'Kazakhstan', 'round_9', 0490),
(237, 'Korea', 'round_9', 0031),
(238, 'Kosovo', 'round_9', 1044),
(239, 'Latvia', 'round_9', 0053),
(240, 'Lithuania', 'round_9', 1140),
(241, 'Macedonia', 'round_9', 0021),
(242, 'Nepal', 'round_9', 1533),
(243, 'Netherlands', 'round_9', 1667),
(244, 'Poland', 'round_9', 0381),
(245, 'Romania', 'round_9', 0056),
(246, 'Russian Federation', 'round_9', 0013),
(247, 'Serbia', 'round_9', 0010),
(248, 'Slovenia', 'round_9', 0016),
(249, 'Thailand', 'round_9', 0068),
(250, 'Turkey', 'round_9', 0104),
(251, 'United Kingdom', 'round_9', 0118),
(252, 'United States', 'round_9', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `team_results_total`
--

CREATE TABLE `team_results_total` (
  `id` int(3) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `result` int(4) UNSIGNED ZEROFILL NOT NULL,
  `result_old` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `team_results_total`
--

INSERT INTO `team_results_total` (`id`, `country`, `result`, `result_old`) VALUES
(1, 'Albania', 0224, 0224),
(2, 'Bulgaria', 1502, 1502),
(3, 'Canada', 1205, 1205),
(4, 'China', 0065, 0065),
(5, 'Colombia', 0044, 0044),
(6, 'Czech Republic', 0099, 0099),
(7, 'France', 0113, 0113),
(8, 'Germany', 1501, 1501),
(9, 'Hungary', 1008, 1008),
(10, 'Indonesia', 0019, 0019),
(11, 'Japan', 1019, 1019),
(12, 'Kazakhstan', 0450, 0450),
(13, 'Korea', 0021, 0021),
(14, 'Kosovo', 1002, 1002),
(15, 'Latvia', 0020, 0020),
(16, 'Lithuania', 1055, 1055),
(17, 'Macedonia', 0093, 0093),
(18, 'Nepal', 1523, 1523),
(19, 'Netherlands', 1429, 1429),
(20, 'Poland', 0209, 0209),
(21, 'Romania', 0024, 0024),
(22, 'Russian Federation', 0174, 0174),
(23, 'Serbia', 0009, 0009),
(24, 'Slovenia', 0037, 0037),
(25, 'Thailand', 0022, 0022),
(26, 'Turkey', 0031, 0031),
(27, 'United Kingdom', 0334, 0334),
(28, 'United States', 1906, 1906),
(29, 'Albania', 0604, 0604),
(30, 'Bulgaria', 1539, 1539),
(31, 'Canada', 1721, 1721),
(32, 'China', 0089, 0089),
(33, 'Colombia', 0066, 0066),
(34, 'Czech Republic', 0121, 0121),
(35, 'France', 0143, 0143),
(36, 'Germany', 1504, 1504),
(37, 'Hungary', 1077, 1077),
(38, 'Indonesia', 0081, 0081),
(39, 'Japan', 2037, 2037),
(40, 'Kazakhstan', 1233, 1233),
(41, 'Korea', 0043, 0043),
(42, 'Kosovo', 1024, 1024),
(43, 'Latvia', 0078, 0078),
(44, 'Lithuania', 1382, 1382),
(45, 'Macedonia', 0450, 0450),
(46, 'Nepal', 2023, 2023),
(47, 'Netherlands', 1895, 1895),
(48, 'Poland', 0395, 0395),
(49, 'Romania', 0070, 0070),
(50, 'Russian Federation', 0253, 0253),
(51, 'Serbia', 0127, 0127),
(52, 'Slovenia', 0157, 0157),
(53, 'Thailand', 0086, 0086),
(54, 'Turkey', 0422, 0422),
(55, 'United Kingdom', 0766, 0766),
(56, 'United States', 2406, 2406),
(57, 'Albania', 0749, 0749),
(58, 'Bulgaria', 1662, 1662),
(59, 'Canada', 2224, 2224),
(60, 'China', 0151, 0151),
(61, 'Colombia', 0112, 0112),
(62, 'Czech Republic', 0196, 0196),
(63, 'France', 0176, 0176),
(64, 'Germany', 1529, 1529),
(65, 'Hungary', 1212, 1212),
(66, 'Indonesia', 0086, 0086),
(67, 'Japan', 2980, 2980),
(68, 'Kazakhstan', 1531, 1531),
(69, 'Korea', 0164, 0164),
(70, 'Kosovo', 1528, 1528),
(71, 'Latvia', 1133, 1133),
(72, 'Lithuania', 2382, 2382),
(73, 'Macedonia', 1051, 1051),
(74, 'Nepal', 2390, 2390),
(75, 'Netherlands', 2895, 2895),
(76, 'Poland', 1594, 1594),
(77, 'Romania', 0644, 0644),
(78, 'Russian Federation', 0480, 0480),
(79, 'Serbia', 0160, 0160),
(80, 'Slovenia', 0546, 0546),
(81, 'Thailand', 0154, 0154),
(82, 'Turkey', 1675, 1675),
(83, 'United Kingdom', 1303, 1303),
(84, 'United States', 2906, 2906),
(85, 'Albania', 0778, 0778),
(86, 'Bulgaria', 2164, 2164),
(87, 'Canada', 2301, 2301),
(88, 'China', 0165, 0165),
(89, 'Colombia', 0153, 0153),
(90, 'Czech Republic', 0262, 0262),
(91, 'France', 0217, 0217),
(92, 'Germany', 1534, 1534),
(93, 'Hungary', 1226, 1226),
(94, 'Indonesia', 0098, 0098),
(95, 'Japan', 3773, 3773),
(96, 'Kazakhstan', 1625, 1625),
(97, 'Korea', 0373, 0373),
(98, 'Kosovo', 1539, 1539),
(99, 'Latvia', 1190, 1190),
(100, 'Lithuania', 2469, 2469),
(101, 'Macedonia', 1076, 1076),
(102, 'Nepal', 2599, 2599),
(103, 'Netherlands', 3400, 3400),
(104, 'Poland', 1791, 1791),
(105, 'Romania', 1182, 1182),
(106, 'Russian Federation', 0495, 0495),
(107, 'Serbia', 0306, 0306),
(108, 'Slovenia', 0709, 0709),
(109, 'Thailand', 0298, 0298),
(110, 'Turkey', 1892, 1892),
(111, 'United Kingdom', 1849, 1849),
(112, 'United States', 3406, 3406),
(113, 'Albania', 0631, 0631),
(114, 'Bulgaria', 2051, 2051),
(115, 'Canada', 1957, 1957),
(116, 'China', 0093, 0093),
(117, 'Colombia', 0104, 0104),
(118, 'Czech Republic', 0218, 0218),
(119, 'France', 0189, 0189),
(120, 'Germany', 1517, 1517),
(121, 'Hungary', 1112, 1112),
(122, 'Indonesia', 0065, 0065),
(123, 'Japan', 2991, 2991),
(124, 'Kazakhstan', 0849, 0849),
(125, 'Korea', 0077, 0077),
(126, 'Kosovo', 1054, 1054),
(127, 'Latvia', 0616, 0616),
(128, 'Lithuania', 1979, 1979),
(129, 'Macedonia', 0328, 0328),
(130, 'Nepal', 2392, 2392),
(131, 'Netherlands', 2915, 2915),
(132, 'Poland', 0763, 0763),
(133, 'Romania', 0742, 0742),
(134, 'Russian Federation', 0414, 0414),
(135, 'Serbia', 0111, 0111),
(136, 'Slovenia', 0366, 0366),
(137, 'Thailand', 0149, 0149),
(138, 'Turkey', 0665, 0665),
(139, 'United Kingdom', 1771, 1771),
(140, 'United States', 3406, 3406),
(141, 'Albania', 1067, 1067),
(142, 'Bulgaria', 2195, 2195),
(143, 'Canada', 2813, 2813),
(144, 'China', 0112, 0112),
(145, 'Colombia', 0121, 0121),
(146, 'Czech Republic', 0247, 0247),
(147, 'France', 0372, 0372),
(148, 'Germany', 1519, 1519),
(149, 'Hungary', 1271, 1271),
(150, 'Indonesia', 0078, 0078),
(151, 'Japan', 3729, 3729),
(152, 'Kazakhstan', 0957, 0957),
(153, 'Korea', 0123, 0123),
(154, 'Kosovo', 1441, 1441),
(155, 'Latvia', 1089, 1089),
(156, 'Lithuania', 2207, 2207),
(157, 'Macedonia', 1097, 1097),
(158, 'Nepal', 2471, 2471),
(159, 'Netherlands', 3915, 3915),
(160, 'Poland', 1009, 1009),
(161, 'Romania', 0978, 0978),
(162, 'Russian Federation', 0435, 0435),
(163, 'Serbia', 0192, 0192),
(164, 'Slovenia', 0459, 0459),
(165, 'Thailand', 0230, 0230),
(166, 'Turkey', 0839, 0839),
(167, 'United Kingdom', 1915, 1915),
(168, 'United States', 3798, 3798),
(169, 'Albania', 1121, 1121),
(170, 'Bulgaria', 2205, 2205),
(171, 'Canada', 2837, 2837),
(172, 'China', 0134, 0134),
(173, 'Colombia', 0233, 0233),
(174, 'Czech Republic', 0262, 0262),
(175, 'France', 0512, 0512),
(176, 'Germany', 1528, 1528),
(177, 'Hungary', 1399, 1399),
(178, 'Indonesia', 0122, 0122),
(179, 'Japan', 3865, 3865),
(180, 'Kazakhstan', 1045, 1045),
(181, 'Korea', 0139, 0139),
(182, 'Kosovo', 1502, 1502),
(183, 'Latvia', 1304, 1304),
(184, 'Lithuania', 2406, 2406),
(185, 'Macedonia', 1257, 1257),
(186, 'Nepal', 2510, 2510),
(187, 'Netherlands', 4297, 4297),
(188, 'Poland', 1086, 1086),
(189, 'Romania', 1034, 1034),
(190, 'Russian Federation', 0448, 0448),
(191, 'Serbia', 0215, 0215),
(192, 'Slovenia', 0478, 0478),
(193, 'Thailand', 0254, 0254),
(194, 'Turkey', 1198, 1198),
(195, 'United Kingdom', 2076, 2076),
(196, 'United States', 3833, 3833),
(197, 'Albania', 1553, 1553),
(198, 'Bulgaria', 2211, 2211),
(199, 'Canada', 3054, 3054),
(200, 'China', 0149, 0149),
(201, 'Colombia', 0300, 0300),
(202, 'Czech Republic', 0389, 0389),
(203, 'France', 0819, 0819),
(204, 'Germany', 1533, 1533),
(205, 'Hungary', 1593, 1593),
(206, 'Indonesia', 0164, 0164),
(207, 'Japan', 4957, 4957),
(208, 'Kazakhstan', 1254, 1254),
(209, 'Korea', 0338, 0338),
(210, 'Kosovo', 1510, 1510),
(211, 'Latvia', 1577, 1577),
(212, 'Lithuania', 2720, 2720),
(213, 'Macedonia', 1309, 1309),
(214, 'Nepal', 2523, 2523),
(215, 'Netherlands', 5297, 5297),
(216, 'Poland', 1726, 1726),
(217, 'Romania', 1611, 1611),
(218, 'Russian Federation', 0504, 0504),
(219, 'Serbia', 0373, 0373),
(220, 'Slovenia', 0594, 0594),
(221, 'Thailand', 0770, 0770),
(222, 'Turkey', 1490, 1490),
(223, 'United Kingdom', 2276, 2276),
(224, 'United States', 3843, 3843),
(225, 'Albania', 1766, 1766),
(226, 'Bulgaria', 2329, 2329),
(227, 'Canada', 3729, 3729),
(228, 'China', 0162, 0162),
(229, 'Colombia', 0516, 0516),
(230, 'Czech Republic', 0416, 0416),
(231, 'France', 1340, 1340),
(232, 'Germany', 1537, 1537),
(233, 'Hungary', 1682, 1682),
(234, 'Indonesia', 0174, 0174),
(235, 'Japan', 5692, 5692),
(236, 'Kazakhstan', 2044, 2044),
(237, 'Korea', 0458, 0458),
(238, 'Kosovo', 1554, 1554),
(239, 'Latvia', 1630, 1630),
(240, 'Lithuania', 2860, 2860),
(241, 'Macedonia', 1337, 1337),
(242, 'Nepal', 2556, 2556),
(243, 'Netherlands', 5964, 5964),
(244, 'Poland', 2107, 2107),
(245, 'Romania', 1894, 1894),
(246, 'Russian Federation', 0529, 0529),
(247, 'Serbia', 0437, 0437),
(248, 'Slovenia', 0948, 0948),
(249, 'Thailand', 1769, 1769),
(250, 'Turkey', 1912, 1912),
(251, 'United Kingdom', 2394, 2394),
(252, 'United States', 4343, 4343);

-- --------------------------------------------------------

--
-- Table structure for table `team_results_total_derived`
--

CREATE TABLE `team_results_total_derived` (
  `id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `round_1` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_2` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_3` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_4` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_5` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_6` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_7` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_8` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_9` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_10` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_11` int(4) UNSIGNED ZEROFILL NOT NULL,
  `round_12` int(4) UNSIGNED ZEROFILL NOT NULL,
  `total` int(4) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applied_pilots`
--
ALTER TABLE `applied_pilots`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `passport` (`passport`),
  ADD UNIQUE KEY `pilot_number` (`aloc_number`),
  ADD UNIQUE KEY `order_number` (`order_number`);

--
-- Indexes for table `applied_tls`
--
ALTER TABLE `applied_tls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_results_per_series`
--
ALTER TABLE `team_results_per_series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_results_total`
--
ALTER TABLE `team_results_total`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_results_total_derived`
--
ALTER TABLE `team_results_total_derived`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `applied_pilots`
--
ALTER TABLE `applied_pilots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `applied_tls`
--
ALTER TABLE `applied_tls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `team_results_per_series`
--
ALTER TABLE `team_results_per_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `team_results_total`
--
ALTER TABLE `team_results_total`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `team_results_total_derived`
--
ALTER TABLE `team_results_total_derived`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
